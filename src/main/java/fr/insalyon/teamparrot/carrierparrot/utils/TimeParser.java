/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.utils;

import java.time.LocalTime;

/**
 * A class used to parse variables to Time variables.
 */
public class TimeParser {

    /**
     * Return a LocalTime based on a "HH:MM" String.
     *
     * @param timeToParse "HH:MM' String.
     * @return The LocalTime corresponding.
     */
    public static LocalTime getAsLocalTime(String timeToParse) {
        if (null == timeToParse || timeToParse.isEmpty()) {
            return null;
        }

        String[] timeComponents;
        timeComponents = timeToParse.trim().split(":");
        final int N = timeComponents.length;

        return LocalTime.of(
                Integer.parseInt(timeComponents[0]),
                (N >= 2) ? Integer.parseInt(timeComponents[1]) : 0,
                (N >= 3) ? Integer.parseInt(timeComponents[2]) : 0
        );
    }

}
