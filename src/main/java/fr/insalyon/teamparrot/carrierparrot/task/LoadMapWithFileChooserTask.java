/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import javafx.stage.FileChooser;
import javafx.stage.Window;
import java.io.File;
import javax.annotation.Nullable;

/**
 * Handle the load of a from a File, send it to the XMLMapParser and trigger the MapLoadedEvent.
 */
public class LoadMapWithFileChooserTask extends InitializableTask {

    private File mapFile;
    private Window window;

    /**
     * LoadMapTask constructor.
     *
     * @param window JavaFX Window instance.
     */
    public LoadMapWithFileChooserTask(@Nullable Window window) {
        this.window = window;
    }

    /**
     * Show a file chooser to select the location of the map to load.
     */
    @Override
    protected boolean initialize() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open a map");
        this.mapFile = fileChooser.showOpenDialog(window);
        return null != this.mapFile;
    }

    @Override
    protected Void call() throws Exception {
        TaskExecutor.execute(new LoadMapTask(mapFile));
        return null;
    }
}
