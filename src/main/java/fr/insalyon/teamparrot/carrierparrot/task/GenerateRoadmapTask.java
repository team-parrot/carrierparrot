/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoadMapGeneratedEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.view.RoadmapExportedDialog;
import fr.insalyon.teamparrot.carrierparrot.view.RoadmapExporter;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import java.io.File;
import javax.annotation.Nullable;

/**
 * A simple task, exporting a round to a roadmap file.
 */
public class GenerateRoadmapTask extends InitializableTask {
    private Round round;
    private String roadmapLocation;
    private Window window;

    /**
     * GenerateRoadmapTask constructor. The file chooser dialog is showed in constructor execution context to stay in
     * UI thread.
     * @param round The round used for roadmap generation.
     * @param window The current JavaFX Window.
     */
    public GenerateRoadmapTask(Round round, @Nullable Window window) {
        this.round = round;
        this.window = window;
    }

    /**
     * Show a file chooser to select the location of the generated roadmap.
     */
    @Override
    protected boolean initialize() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose where to save the roadmap");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("PDF", "*.pdf"));
        File roadmapFile = fileChooser.showSaveDialog(window);
        if (null == roadmapFile) {
            return false;
        } else {
            this.roadmapLocation = roadmapFile.getAbsolutePath();
            return true;
        }
    }

    @Override
    public Void call() throws Exception {
        // Export the roadmap
        new RoadmapExporter(roadmapLocation, round).createDocument();
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        EventBus.post(new RoadMapGeneratedEvent());

        RoadmapExportedDialog dialog = new RoadmapExportedDialog(roadmapLocation);
        dialog.showAndHandle();
    }

    @Override
    protected void failed() {
        super.failed();

        EventBus.post(new NotificationEvent(
                "Impossible to open the PDF on your computer.",
                "You should check if you have a default PDF application.",
                NotificationEvent.NotificationEventType.WARNING
        ));
    }
}
