/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.controller.StateManager;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;

/**
 * A task which add a Delivery at a specific place if it is possible.
 */
public class AddDeliveryManuallyTask extends InitializableTask {

    private final StateManager stateManager;
    private Round round;
    private Delivery deliveryToAdd;
    private int position;

    public AddDeliveryManuallyTask(Round round, Delivery delivery, int position) {
        stateManager = StateManager.getInstance();
        this.round = round;
        this.deliveryToAdd = delivery;
        this.position = position;
    }

    @Override
    protected Void call() throws Exception {
        if (!round.addAt(this.deliveryToAdd, this.position)) {
            throw new Exception("It's impossible to add the delivery.");
        }
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        stateManager.toRoundSelectedState();
        EventBus.post(new NotificationEvent(
                "Delivery has been added.",
                "",
                NotificationEvent.NotificationEventType.INFO
        ));
    }

    @Override
    protected void failed() {
        super.failed();

        stateManager.toRoundSelectedState();
        Throwable e = getException();
        EventBus.post(new NotificationEvent(
                "Failed to add the delivery.",
                e.getMessage(),
                NotificationEvent.NotificationEventType.WARNING
        ));
//        CarrierParrotApp.LOGGER.error("Failed to add the delivery.", e);
    }
}
