/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.MessageEvent;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;

/**
 * Asynchronously removes a delivery from a round.
 */
public class RemoveDeliveryTask extends InitializableTask {

    private Round round;
    private Delivery delivery;

    public RemoveDeliveryTask(Round round, Delivery delivery) {
        this.round = round;
        this.delivery = delivery;
    }

    @Override
    public Void call() throws Exception {
        int originalRoundSize = round.getPaths().size();
        if (originalRoundSize > 2) {
            round.removeDelivery(delivery);
        } else {
            EventBus.post(new NotificationEvent(
                    "Failed to remove the delivery.",
                    "Cannot remove the last delivery of a round.",
                    NotificationEvent.NotificationEventType.WARNING
            ));
        }
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        EventBus.post(new MessageEvent("Delivery successfully removed"));
    }

    @Override
    protected void failed() {
        super.failed();

        EventBus.post(new NotificationEvent(
                "Failed to remove the delivery.",
                getMessage(),
                NotificationEvent.NotificationEventType.WARNING
        ));
        CarrierParrotApp.LOGGER.error(getException());
    }
}
