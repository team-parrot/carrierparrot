/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.event.DeliveryRequestLoadedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import java.io.File;

/**
 * Handle the loading of delivery request, open a file chooser, and finally parse the file chosen.
 */
public class LoadDeliveryRequestTask extends InitializableTask {
    private final Map map;
    private File deliveryRequestFile;
    private DeliveryRequest deliveryRequest;

    /**
     * LoadDeliveryRequestTask constructor, construct a FileChooser in UI Thread to get map file.
     *
     * @param map                 The map object.
     * @param deliveryRequestFile The file containing the delivery request.
     */
    public LoadDeliveryRequestTask(Map map, File deliveryRequestFile) {
        this.map = map;
        this.deliveryRequestFile = deliveryRequestFile;
        this.deliveryRequest = null;
    }

    @Override
    protected Void call() throws Exception {
        // Parse the delivery request file
        this.deliveryRequest = XMLDeliveryRequestParser.parse(deliveryRequestFile.getAbsolutePath(), this.map);
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        EventBus.post(new DeliveryRequestLoadedEvent(this.deliveryRequest));

        // Trigger the computation of the round
        TaskExecutor.execute(new ComputeRoundTask(
                this.map,
                this.deliveryRequest
        ));
    }

    @Override
    protected void failed() {
        super.failed();

        Throwable e = getException();
        EventBus.post(new NotificationEvent(
                "Failed to load the delivery request.",
                e.getMessage(),
                NotificationEvent.NotificationEventType.WARNING
        ));
        CarrierParrotApp.LOGGER.error("Failed to load the delivery request.", e);
    }
}
