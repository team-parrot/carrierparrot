/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.controller.StateManager;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundComputedEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.RoundFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;

/**
 * This task call the RoundFactory, using provided map, and generate the best round possible for a delivery request.
 */
public class ComputeRoundTask extends InitializableTask {
    private StateManager stateManager;

    private final Map map;
    private final DeliveryRequest deliveryRequest;
    private Round round;
    private long duration;

    ComputeRoundTask(Map map, DeliveryRequest deliveryRequest) {
        stateManager = StateManager.getInstance();

        this.map = map;
        this.deliveryRequest = deliveryRequest;
        this.duration = 0;
    }

    @Override
    protected void running() {
        super.scheduled();

        duration = System.currentTimeMillis();
    }

    @Override
    protected Void call() throws Exception {
        // Compute the round
        this.round = RoundFactory.get(this.map, this.deliveryRequest);
        duration = System.currentTimeMillis() - duration;
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        stateManager.toRoundSelectedState();

        EventBus.post(new RoundComputedEvent(this.round, duration / 1000.));
    }

    @Override
    protected void cancelled() {
        super.cancelled();
//        TODO: EventBus.post(new LoadDeliveryRequestCancelledEvent());
    }

    @Override
    protected void failed() {
        super.failed();

//    } catch (DeliveryRequestFailedToLoadException e) {
//        return false;
//    }

        Throwable e = getException();
        EventBus.post(new NotificationEvent(
                "Failed to compute the round.",
                e.getMessage(),
                NotificationEvent.NotificationEventType.WARNING
        ));
        CarrierParrotApp.LOGGER.error("Failed to compute the round.", e);
    }
}
