/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.concurrent.Worker;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A @TaskExecutor executes task asynchronously, but sequentially.
 */
public class TaskExecutor {

    private static TaskExecutor instance = null;

    public static TaskExecutor getInstance() {
        if (null == instance) {
            instance = new TaskExecutor();
        }
        return instance;
    }

    private ExecutorService executor;

    /**
     * Constructor: Create a TaskExecutor with a new single thread executor.
     */
    private TaskExecutor() {
        this.executor = Executors.newSingleThreadExecutor(r -> {
            Thread t = new Thread(r);
            t.setDaemon(true); // allows app to exit if task are running
            return t;
        });
    }

    /**
     * Execute a task asynchronously. If a task is already running, add the task to a queue.
     *
     * @param task : the task to execute.
     * @return the State of the task as a property.
     */
    private ReadOnlyObjectProperty<Worker.State> _execute(InitializableTask task) {
        if (task.initialize()) {
            executor.submit(task);
        }
        return task.stateProperty();
    }

    /**
     * Execute a task asynchronously. If a task is already running, add the task to a queue.
     *
     * @param task : the task to execute.
     * @return the State of the task as a property.
     */
    public static ReadOnlyObjectProperty<Worker.State> execute(InitializableTask task) {
        return getInstance()._execute(task);
    }
}
