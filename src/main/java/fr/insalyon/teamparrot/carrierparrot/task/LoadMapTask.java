/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.controller.StateManager;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.MapLoadedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.parser.XMLMapParser;
import java.io.File;

/**
 * Handle the load of a from a File, send it to the XMLMapParser and trigger the MapLoadedEvent.
 */
public class LoadMapTask extends InitializableTask {

    private StateManager stateManager;

    private File mapFile;
    private Map map;

    /**
     * @param mapFile the file to load.
     */
    public LoadMapTask(File mapFile) {
        stateManager = StateManager.getInstance();

        this.mapFile = mapFile;
    }

    @Override
    protected Void call() throws Exception {
        // Parse the map file
        this.map = XMLMapParser.parse(mapFile.getAbsolutePath());
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        stateManager.toNoRoundSelectedState();

        EventBus.post(new MapLoadedEvent(this.map));
    }

    @Override
    protected void failed() {
        super.failed();

        Throwable e = getException();
        EventBus.post(new NotificationEvent(
                "Failed to load the map.",
                e.getMessage(),
                NotificationEvent.NotificationEventType.WARNING
        ));
        CarrierParrotApp.LOGGER.error("Failed to load the map.", e);
    }
}
