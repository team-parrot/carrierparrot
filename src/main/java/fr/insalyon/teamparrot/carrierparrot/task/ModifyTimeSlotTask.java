/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import java.time.LocalTime;

/**
 * Handle the modification of the time slot of a delivery.
 */
public class ModifyTimeSlotTask extends InitializableTask {

    private Round round;
    private final Delivery delivery;
    private TimeSlot timeSlot;

    public ModifyTimeSlotTask(Round round, Delivery delivery, TimeSlot timeSlot) {
        this.round = round;
        this.delivery = delivery;
        this.timeSlot = timeSlot;
    }

    @Override
    protected Void call() throws Exception {
        LocalTime startTime = timeSlot.getStartTime();
        LocalTime endTime = timeSlot.getEndTime();

        round.changeDeliveryTimeSlots(delivery, startTime, endTime);

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        CarrierParrotApp.LOGGER.debug("Time slot modified successfully.");

        EventBus.post(new NotificationEvent("Time slot modified successfully.",
                "",
                NotificationEvent.NotificationEventType.INFO));
    }

    @Override
    protected void failed() {
        super.failed();

        EventBus.post(new NotificationEvent(
                "Failed to modify the delivery time slot.",
                getException().getMessage(),
                NotificationEvent.NotificationEventType.WARNING
        ));
    }
}
