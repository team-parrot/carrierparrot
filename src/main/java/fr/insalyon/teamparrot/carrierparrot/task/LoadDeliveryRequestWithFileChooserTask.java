/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import java.io.File;
import javax.annotation.Nullable;

/**
 * Handle the loading of delivery request, open a file chooser, and finally parse the file chosen.
 */
public class LoadDeliveryRequestWithFileChooserTask extends InitializableTask {
    private final Map map;
    private File deliveryRequestFile;
    private Window window;

    /**
     * LoadDeliveryRequestTask constructor, construct a FileChooser in UI Thread to get map file.
     *
     * @param map    The map object.
     * @param window The JavaFX Window instance.
     */
    public LoadDeliveryRequestWithFileChooserTask(Map map, @Nullable Window window) {
        this.map = map;
        this.window = window;
    }

    /**
     * Show a file chooser to select the location of the generated roadmap.
     */
    @Override
    protected boolean initialize() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open a delivery request");
        this.deliveryRequestFile = fileChooser.showOpenDialog(window);
        return null != this.deliveryRequestFile;
    }

    @Override
    protected Void call() throws Exception {
        TaskExecutor.execute(new LoadDeliveryRequestTask(map, deliveryRequestFile));
        return null;
    }
}
