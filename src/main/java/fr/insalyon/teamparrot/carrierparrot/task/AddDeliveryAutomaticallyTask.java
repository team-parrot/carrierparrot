/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.controller.StateManager;
import fr.insalyon.teamparrot.carrierparrot.event.CloseAddingModeEvent;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;

/**
 * This task add the provided delivery to the current round.
 */
public class AddDeliveryAutomaticallyTask extends InitializableTask {
    private StateManager stateManager;

    private Round round;
    private Delivery deliveryToAdd;

    public AddDeliveryAutomaticallyTask(Round round, Delivery delivery) {
        stateManager = StateManager.getInstance();

        this.round = round;
        this.deliveryToAdd = delivery;
    }

    @Override
    public Void call() throws Exception {
        round.addAutomatically(deliveryToAdd);
        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        stateManager.toRoundSelectedState();

        EventBus.post(new NotificationEvent("Delivery added to the round",
                "The delivery have been added to the round successfully",
                NotificationEvent.NotificationEventType.INFO));
        EventBus.post(new CloseAddingModeEvent());
    }

    @Override
    protected void failed() {
        super.failed();

        EventBus.post(new NotificationEvent("Impossible to add a delivery with the specified time slot.",
                "It is impossible to add a delivery with this time slot constraints. Please change the time " +
                        "slots and try again.",
                NotificationEvent.NotificationEventType.WARNING));
    }
}
