/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.command;

import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.task.AddDeliveryAutomaticallyTask;
import fr.insalyon.teamparrot.carrierparrot.task.AddDeliveryManuallyTask;
import fr.insalyon.teamparrot.carrierparrot.task.InitializableTask;
import fr.insalyon.teamparrot.carrierparrot.task.RemoveDeliveryTask;

/**
 * A command which permit to do / undo / redo the adding of a delivery.
 */
public class AddDeliveryCommand extends Command {

    private Round round;
    private Delivery delivery;
    private int position;

    /**
     * Construct the command which add @delivery in @round at position @position.
     * If @position is negative, it's automatically determined to be optimal.
     *
     * @param round    : the round in which add the delivery.
     * @param delivery : the delivery to add.
     * @param position : the position where to insert the delivery.
     */
    public AddDeliveryCommand(Round round, Delivery delivery, int position) {
        this.round = round;
        this.delivery = delivery;
        this.position = position;
    }

    @Override
    boolean initialize() {
        // Nothing to do
        return true;
    }

    @Override
    public InitializableTask getExecuteTask() {
        if (position < 0) {
            return new AddDeliveryAutomaticallyTask(round, delivery);
        } else {
            return new AddDeliveryManuallyTask(round, delivery, position);
        }
    }

    @Override
    public InitializableTask getRollbackTask() {
        // Search the delivery in the round
        for (Path path : round) {
            if (path.getStart() == delivery) {
                return new RemoveDeliveryTask(round, delivery);
            }
        }
        return null;
    }
}
