/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.command;

import fr.insalyon.teamparrot.carrierparrot.task.InitializableTask;
import fr.insalyon.teamparrot.carrierparrot.task.TaskExecutor;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.concurrent.Worker;

/**
 * This is the base class for commands, which are undo / redo-able.
 */
abstract class Command {
    private InitializableTask executeTask;
    private InitializableTask rollbackTask;

    public Command() {
        this.executeTask = null;
        this.rollbackTask = null;
    }

    /**
     * Initialize the command. This method is called once.
     *
     * @return whether the initialization succeeded or not.
     */
    abstract boolean initialize();

    /**
     * Execute the command.
     * <p>
     * This method start the command returned by @getExecuteTask(), but doesn't wait
     * for the task to finish. So, the returned value doesn't acknowledge if the execution
     * succeeded, but whether the task started.
     *
     * @return the execution started or not.
     */
    ReadOnlyObjectProperty<Worker.State> execute() {
        if (didTaskFail(this.rollbackTask)) {
            return new ReadOnlyObjectWrapper<>(Worker.State.CANCELLED);
        }
        if (null == (this.executeTask = getExecuteTask())) {
            return new ReadOnlyObjectWrapper<>(Worker.State.CANCELLED);
        }
        return TaskExecutor.execute(this.executeTask);
    }

    /**
     * Rollback the command.
     * <p>
     * This method start the rollback command returned by @getRollbackTask(), but doesn't wait
     * for the task to finish. So, the returned value doesn't acknowledge if the rollback
     * succeeded, but whether the task started.
     *
     * @return the rollback started or not.
     */
    ReadOnlyObjectProperty<Worker.State> rollback() {
        if (didTaskFail(this.executeTask)) {
            return new ReadOnlyObjectWrapper<>(Worker.State.CANCELLED);
        }
        if (null == (this.rollbackTask = getRollbackTask())) {
            return new ReadOnlyObjectWrapper<>(Worker.State.CANCELLED);
        }
        return TaskExecutor.execute(this.rollbackTask);
    }

    private static boolean didTaskFail(InitializableTask task) {
        return null != task && task.getState() != Worker.State.SUCCEEDED;
    }

    /**
     * Returns the task which will be launched by @execute().
     *
     * @return the execution task.
     */
    abstract InitializableTask getExecuteTask();

    /**
     * Returns the task which will be launched by @rollback().
     *
     * @return the rollback task.
     */
    abstract InitializableTask getRollbackTask();
}
