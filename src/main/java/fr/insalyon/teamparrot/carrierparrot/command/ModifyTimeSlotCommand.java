/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.command;

import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import fr.insalyon.teamparrot.carrierparrot.task.InitializableTask;
import fr.insalyon.teamparrot.carrierparrot.task.ModifyTimeSlotTask;
import fr.insalyon.teamparrot.carrierparrot.view.ModifyTimeSlotDialog;

/**
 * A command which permit to do / undo / redo the modification the time slots of a delivery.
 */
public class ModifyTimeSlotCommand extends Command {

    private Round round;
    private final Delivery delivery;

    private TimeSlot originalTimeSlot;
    private TimeSlot newTimeSlot;

    public ModifyTimeSlotCommand(Round round, Delivery delivery) {
        this.round = round;
        this.delivery = delivery;

        this.originalTimeSlot = null;
        this.newTimeSlot = null;
    }

    @Override
    public boolean initialize() {
        // Ask the new time slot
        if ((newTimeSlot = new ModifyTimeSlotDialog(delivery).getTimeSlot()) == null) {
            return false;
        }

        // Save the current time slot
        this.originalTimeSlot = new TimeSlot(delivery.getTimeSlot());

        return true;
    }

    @Override
    public InitializableTask getExecuteTask() {
        return new ModifyTimeSlotTask(round, delivery, newTimeSlot);
    }

    @Override
    public InitializableTask getRollbackTask() {
        return new ModifyTimeSlotTask(round, delivery, originalTimeSlot);
    }
}
