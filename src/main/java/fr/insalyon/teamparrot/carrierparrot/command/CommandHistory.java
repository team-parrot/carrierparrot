/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.command;

import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.concurrent.Worker;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import static fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent.NotificationEventType.INFO;

/**
 * CommandHistory is the (singleton) class which handle commands and there history. It's hence possible to
 * execute a command, and to undo / redo.
 *
 * It maintains a command history which is navigable using undo / redo.
 *
 * Only one command (execute, undo or redo) can run at the same time. Hence, if @execute() is called when
 * the command history @isBusy(), the new command is queued.
 */
public final class CommandHistory implements InvalidationListener {

    private static CommandHistory instance = null;

    private static CommandHistory getInstance() {
        if (null == instance) {
            instance = new CommandHistory();
        }
        return instance;
    }

    private final List<Command> history;
    private final List<Command> futureCommands;
    private final Queue<Command> commandQueue;

    private Command currentCommand;
    private ReadOnlyObjectProperty<Worker.State> currentTaskState;
    private CommandType currentCommandType;

    private CommandHistory() {
        this.history = new ArrayList<>();
        this.futureCommands = new ArrayList<>();
        this.commandQueue = new ArrayDeque<>();
    }

    /**
     * @see #execute(Command)
     * @param command : the command to execute.
     */
    private void _execute(Command command) {
        if (isBusy()) {
            commandQueue.offer(command);
            return;
        }
        if (!command.initialize()) {
            return;
        }
        currentCommand = command;
        currentCommandType = CommandType.EXECUTE;
        currentTaskState = command.execute();
        currentTaskState.addListener(this);
    }

    /**
     * Executes the given command, by first calling it's initialize() method, then its execute method.
     * If a command is currently running, the new command in queued.
     *
     * @param command : the command to execute.
     */
    public static void execute(Command command) {
        getInstance()._execute(command);
    }

    /**
     * @see #undo()
     */
    private void _undo() {
        if (isBusy()) {
            EventBus.post(new NotificationEvent(
                    "Can't undo.",
                    "A command is currently running.",
                    INFO
            ));
            return;
        }
        if (history.isEmpty()) {
            EventBus.post(new NotificationEvent(
                    "Can't undo.",
                    "No command to undo.",
                    INFO
            ));
            return;
        }

        Command command = history.get(history.size() - 1);
        currentCommand = command;
        currentCommandType = CommandType.UNDO;
        currentTaskState = command.rollback();
        currentTaskState.addListener(this);
    }

    /**
     * Tries to undo the previous command in the command history. It fails with a notification if {@link CommandHistory}
     * is busy or if the history is empty.
     * The undo task is executed asynchronously, and the {@link CommandHistory} is busy as long as the task hasn't
     * finished.
     */
    public static void undo() {
        getInstance()._undo();
    }

    /**
     * @see #redo()
     */
    private void _redo() {
        if (isBusy()) {
            EventBus.post(new NotificationEvent(
                    "Can't redo.",
                    "A command is currently running.",
                    INFO
            ));
            return;
        }
        if (futureCommands.isEmpty()) {
            EventBus.post(new NotificationEvent(
                    "Can't redo.",
                    "No command to redo.",
                    INFO
            ));
            return;
        }

        Command command = futureCommands.get(futureCommands.size() - 1);
        currentCommand = command;
        currentCommandType = CommandType.REDO;
        currentTaskState = command.execute();
        currentTaskState.addListener(this);
    }

    /**
     * Tries to redo the future command in the command history. It fails with a notification if {@link CommandHistory}
     * is busy or if there are no future commands (ie. commands previously undone).
     * The redo task is executed asynchronously, and the {@link CommandHistory} is busy as long as the task hasn't
     * finished.
     */
    public static void redo() {
        getInstance()._redo();
    }

    /**
     * @return whether there is a command currently running asynchronously.
     */
    private boolean isBusy() {
        return null != currentCommand;
    }

    @Override
    public void invalidated(Observable observable) {
        final Worker.State taskState = currentTaskState.get();

        // If the command failed, clear the command queue
        if (taskState == Worker.State.CANCELLED || taskState == Worker.State.FAILED) {
            currentCommand = null;
            currentTaskState = null;
            clearCommandQueueOnFailure();
            return;
        }

        // If the command succeeded, handle the history and execute the next queued command
        if (taskState == Worker.State.SUCCEEDED) {
            if (currentCommandType == CommandType.EXECUTE) {
                history.add(currentCommand);
                futureCommands.clear();
            } else if (currentCommandType == CommandType.UNDO) {
                futureCommands.add(currentCommand);
                history.remove(history.size() - 1);
            } else if (currentCommandType == CommandType.REDO) {
                futureCommands.remove(futureCommands.size() - 1);
                history.add(currentCommand);
            }
            currentCommand = null;
            currentTaskState = null;
            executeNextQueuedCommand();
        }
    }

    private void executeNextQueuedCommand() {
        if (commandQueue.size() > 0) {
            _execute(commandQueue.poll());
        }
    }

    private void clearCommandQueueOnFailure() {
        if (commandQueue.size() > 0) {
            commandQueue.clear();
            EventBus.post(new NotificationEvent("Command queue cleared.",
                    "The command queue has been cleared because the last command failed.", INFO));
        }
    }
}
