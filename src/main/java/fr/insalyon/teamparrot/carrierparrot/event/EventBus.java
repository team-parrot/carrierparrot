/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.event;

import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import javafx.application.Platform;

/**
 * The EventBus delivers a simple but efficient interface to send events all over the app.
 * It follows the Event/Subscribe design pattern.
 */
public class EventBus {
    private static EventBus instance = new EventBus();

    public static EventBus getInstance() {
        return instance;
    }

    private Subject<Event> busSubject;

    private EventBus() {
        busSubject = PublishSubject.create();
    }

    private void _subscribe(final Class<?> eventClass, Consumer<Event> consumer) {
        busSubject
                .filter(event -> eventClass.isAssignableFrom(event.getClass()))
                .subscribe(consumer);
    }

    private void _post(Event value) {
        Platform.runLater(() -> busSubject.onNext(value));
    }

    public static void subscribe(final Class<?> eventClass, Consumer<Event> consumer) {
        EventBus.getInstance()._subscribe(eventClass, consumer);
    }

    public static void post(Event event) {
        EventBus.getInstance()._post(event);
    }
}
