/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory;

import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.MapLoadedEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Place;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The Place factory generates and store places associated to map nodes. It makes the link between business object and
 * map representation object.
 */
public class PlaceFactory {

    private static PlaceFactory instance = null;

    private static PlaceFactory getInstance() {
        if (null == instance) {
            instance = new PlaceFactory();
        }
        return instance;
    }

    private PlaceFactory() {
        EventBus.subscribe(MapLoadedEvent.class, event -> places.clear());
    }

    private final HashMap<Long, Place> places = new HashMap<>();

    /**
     * Check if the place is stored in cache. If not, generate a new one from map and node ID given in parameters.
     * @param map Map containing the place node.
     * @param nodeId The concerned node id.
     * @return The place linked to this node id.
     */
    private Place _get(Map map, Long nodeId) {
        Place place = places.get(nodeId);

        if (null == place) {
            ArrayList<String> streets = new ArrayList<>();
            for (Section section : map.getSectionsFromOriginId(nodeId)) {
                streets.add(section.getStreetName());
            }
            for (Section section : map.getSectionsFromDestinationId(nodeId)) {
                streets.add(section.getStreetName());
            }
            place = new Place(map.getNodeFromId(nodeId), streets);

            places.put(nodeId, place);
        }

        return place;
    }

    /**
     * Check if the place is stored in cache. If not, generate a new one from map and node ID given in parameters.
     * @param map Map containing the place node.
     * @param nodeId The concerned node id.
     * @return The place linked to this node id.
     */
    public static Place get(Map map, Long nodeId) {
        return getInstance()._get(map, nodeId);
    }
}
