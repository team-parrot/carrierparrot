/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp;

import fr.insalyon.teamparrot.carrierparrot.Config;
import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A time aware implementation of TSP branch-and-bound algorithm.
 *
 * @author Christine Solnon, INSA Lyon
 * @author Team Parrot, INSA Lyon
 */
public abstract class TSPBranchAndBound implements TSP {

    private Integer[] bestSolution;
    private LocalTime[] bestSolutionMinTime;
    private long bestSolutionPrice = 0;
    private Boolean reachedLimitTime;
    private long beginTime;

    @Override
    public boolean getReachedLimitTime() {
        return reachedLimitTime;
    }

    @Override
    public void computeSolution(int nbNodes, long[][] prices, long[] durations, TimeSlot[] timeSlots) {
        reachedLimitTime = false;
        bestSolutionPrice = Long.MAX_VALUE;
        bestSolution = new Integer[nbNodes];
        bestSolutionMinTime = new LocalTime[nbNodes];
        List<Integer> unseen = new LinkedList<>();
        for (int i = 1; i < nbNodes; i++) {
            unseen.add(i);
        }
        List<Integer> seen = new LinkedList<>();
        seen.add(0); // the first visited node is 0
        List<LocalTime> seenMinTime = new LinkedList<>();
        seenMinTime.add(timeSlots[0].getStartTime());
        this.beginTime = System.currentTimeMillis();
        branchAndBound(
                0,
                unseen, seen, seenMinTime,
                0,
                prices, durations, timeSlots
        );
    }

    @Override
    public boolean hasSolution() {
        for (Integer elem : bestSolution) {
            if (null == elem) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Integer getBestSolutionAt(int i) {
        if ((bestSolution == null) || (i < 0) || (i >= bestSolution.length)) {
            return null;
        }
        return bestSolution[i];
    }

    @Override
    public LocalTime getMinArrivalTime(int i) {
        if ((bestSolution == null) || (i < 0) || (i >= bestSolution.length)) {
            return null;
        }
        return bestSolutionMinTime[i];
    }

    @Override
    public long getBestSolutionPrice() {
        return bestSolutionPrice;
    }

    /**
     * @param currentNode : the last visited node.
     * @param unseen      : list of remaining nodes to visit.
     * @param prices      : prices[i][j] = duration to go from i to j, with 0 &lt;= i &lt; nbNodes et 0 &lt;= j &lt; nbNodes.
     * @param durations   : durations[i] = duration to visit the node i, with 0 &lt;= i &lt; nbNodes.
     * @param timeSlots   : timeSlots[i] = time slot of node i, with 0 &lt;= i &lt; nbNodes.
     * @return a lower bound of the price of permutations, beginning with currentNode, containing every unseen nodes
     * exactly one time, and finishing by a 0.
     */
    abstract long bound(int currentNode, List<Integer> unseen,
                                  long[][] prices, long[] durations, TimeSlot[] timeSlots);

    /**
     * @param currentNode : the last visited node.
     * @param unseen      : list of remaining nodes to visit.
     * @param prices      : prices[i][j] = duration to go from i to j, with 0 &lt;= i &lt; nbNodes et 0 &lt;= j &lt; nbNodes.
     * @param durations   : durations[i] = duration to visit the node i, with 0 &lt;= i &lt; nbNodes.
     * @param timeSlots   : timeSlots[i] = time slot of node i, with 0 &lt;= i &lt; nbNodes.
     * @return an {@code Iterator<Long>} enabling to iterate all unseen nodes.
     */
    protected abstract Iterator<Integer> iterator(
            int currentNode,
            List<Integer> unseen,
            long[][] prices,
            long[] durations,
            TimeSlot[] timeSlots);

    /**
     * @param currentTime : the minimal arrival time at the current node.
     * @param unseen      : list of remaining nodes to visit.
     * @param timeSlots   : timeSlots[i] = time slot of node i, with 0 &lt;= i &lt; nbNodes.
     * @return whether an unseen node has its timeslot end earlier than currentTime.
     */
    private boolean verifyUnseenTimeSlotFeasible(LocalTime currentTime, List<Integer> unseen, TimeSlot[] timeSlots) {
        for (Integer node : unseen) {
            if (null != timeSlots[node] && null != timeSlots[node].getEndTime()
                    && currentTime.isAfter(timeSlots[node].getEndTime())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Branch&Bound resolution of TSP (template method).
     *
     * seen's order represent the round order. seenMinTime has the same order than seen.
     *
     * @param currentNode    : the last visited node.
     * @param unseen         : list of remaining nodes to visit.
     * @param seen           : list of seen nodes (including currentNode).
     * @param seenMinTime    : list of minimal possible arrival time at each nodes (including currentNode).
     * @param seenPrice      : sum of prices of path edges passing by every seen nodes + sum of durations of seen nodes.
     * @param prices         : prices[i][j] = duration to go from i to j, with 0 &lt;= i &lt; nbNodes et 0 &lt;= j &lt; nbNodes.
     * @param durations      : durations[i] = duration to visit the node i, with 0 &lt;= i &lt; nbNodes.
     * @param timeSlots      : timeSlots[i] = time slot of node i, with 0 &lt;= i &lt; nbNodes.
     */
    private void branchAndBound(int currentNode,
                                List<Integer> unseen,
                                List<Integer> seen,
                                List<LocalTime> seenMinTime,
                                long seenPrice,
                                long[][] prices,
                                long[] durations,
                                TimeSlot[] timeSlots) {
        // Stop the computation if it takes to much time
        if (System.currentTimeMillis() - beginTime > (long) (Config.TIME_LIMIT_SEC * 1000)) {
            reachedLimitTime = true;
            return;
        }

        // Compute two useful timing values
        final LocalTime warehouseStartTime = timeSlots[0].getStartTime();
        // cnmt = ws + sp - dcn
        LocalTime currentNodeMinTime = warehouseStartTime
                .plus(Duration.ofSeconds(seenPrice))
                .minus(Duration.ofSeconds(durations[currentNode]));

        // Doesn't explore the branch if it's not compatible with the time window
        if (!verifyUnseenTimeSlotFeasible(currentNodeMinTime, unseen, timeSlots)) {
            return;
        }
        if (null != timeSlots[currentNode] && null != timeSlots[currentNode].getEndTime()) {
            final LocalTime currentNodeTimeSlotEnd = timeSlots[currentNode].getEndTime();

            if (currentNodeMinTime.isAfter(currentNodeTimeSlotEnd)) {
                return;
            }
        }

        // Pause the delivery man if he arrives before the beginning of the time slot
        if (null != timeSlots[currentNode] && null != timeSlots[currentNode].getStartTime()) {
            final LocalTime currentNodeTimeSlotStart = timeSlots[currentNode].getStartTime();

            if (currentNodeMinTime.isBefore(currentNodeTimeSlotStart)) {
                currentNodeMinTime = currentNodeTimeSlotStart;

                // TODO: this is invalid if the delivery overlap midnight
                seenPrice = currentNodeMinTime.toSecondOfDay()
                        - warehouseStartTime.toSecondOfDay()
                        + durations[currentNode]
                ;
            }
        }

        //
        if (unseen.size() == 0) { // every nodes have been visited
            seenPrice += prices[currentNode][0];
            if (seenPrice < bestSolutionPrice) { // we found a better solution than bestSolution
                seen.toArray(bestSolution); // copies seen into bestSolution
                bestSolutionPrice = seenPrice;

                final int NB_NODES = bestSolutionMinTime.length;
                for (int i = 0; i < NB_NODES - 1; ++i) {
                    bestSolutionMinTime[i] = seenMinTime.get(i + 1);
                }
                bestSolutionMinTime[NB_NODES - 1] = currentNodeMinTime;

            }
        } else if (seenPrice + bound(currentNode, unseen, prices, durations, timeSlots) < bestSolutionPrice) {
            Iterator<Integer> it = iterator(currentNode, unseen, prices, durations, timeSlots);
            while (it.hasNext()) {
                Integer nextNode = it.next();

                seen.add(nextNode);
                seenMinTime.add(currentNodeMinTime);
                unseen.remove(nextNode);

                branchAndBound(
                        nextNode,
                        unseen, seen, seenMinTime,
                        seenPrice + prices[currentNode][nextNode] + durations[nextNode],
                        prices, durations, timeSlots);

                seen.remove(nextNode);
                seenMinTime.remove(currentNodeMinTime);
                unseen.add(nextNode);
            }
        }
    }
}

