/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp;

import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import java.util.Arrays;
import java.util.Collection;

/**
 * A simple node iterator, which simply iterates over a list of given nodes.
 *
 * @author Christine Solnon, INSA Lyon
 */
class IteratorIncreasing extends IteratorSeq {

    /**
     * Create an iterator over the nodes of unseen.
     *
     * @param currentNode : the last visited node.
     * @param unseen      : list of remaining nodes to visit.
     * @param prices      : prices[i][j] = duration to go from i to j, with 0 <= i < nbNodes et 0 <= j < nbNodes.
     * @param timeSlots   : timeSlots[i] = time slot of node i, with 0 <= i < nbNodes.
     */
    IteratorIncreasing(int currentNode, Collection<Integer> unseen, long[][] prices, TimeSlot[] timeSlots) {
        super(unseen);
        Arrays.sort(this.candidates, (i, j) -> {
            if (null == timeSlots[i] && null != timeSlots[j]) { // 23h59 by default
                return 1;
            } else if (null != timeSlots[i] && null == timeSlots[j]) {
                return -1;
            } else if (null != timeSlots[i] && null != timeSlots[j]) {
                if (!timeSlots[i].getEndTime().equals(timeSlots[j].getEndTime())) {
                    return timeSlots[i].getEndTime().isAfter(timeSlots[j].getEndTime()) ? 1 : -1;
                }
            }
            final long price_i = prices[currentNode][i];
            final long price_j = prices[currentNode][j];
            return (int) (price_j - price_i);
        });
    }
}
