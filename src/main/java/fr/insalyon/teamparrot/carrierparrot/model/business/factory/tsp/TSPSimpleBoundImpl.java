/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp;

import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple bound function using a greedy approach.
 * <p>
 * Bound: simple sum of min lengths, see @bound for details.
 * Iterator: unsorted.
 */
public class TSPSimpleBoundImpl extends TSPNaiveImpl {

    /**
     * Compute a bounding length for navigating through each @unseen nodes.
     *
     * It proceeds by summing length from each source to its nearest destination, source nodes being the unseen
     * ones + @currentNode, and destination nodes being the unseen ones + 0.
     */
    @Override
    public long bound(int currentNode, List<Integer> unseen,
                      long[][] prices, long[] durations, TimeSlot[] timeSlots) {

        List<Integer> sources = new ArrayList<>(unseen);
        sources.add(currentNode);
        List<Integer> destinations = new ArrayList<>(unseen);
        destinations.add(0);

        long bound1 = 0;
        for (Integer source : sources) {
            long minPrice1 = Long.MAX_VALUE;
            long minPrice2 = Long.MAX_VALUE;
            for (Integer destination : destinations) {
                if (prices[source][destination] < minPrice1) {
                    minPrice2 = minPrice1;
                    minPrice1 = prices[source][destination];
                } else if (prices[source][destination] < minPrice2) {
                    minPrice2 = prices[source][destination];
                }
            }
            bound1 += minPrice1 + minPrice2;
        }
        long bound2 = 0;
        for (Integer destination : destinations) {
            long minPrice1 = Long.MAX_VALUE;
            long minPrice2 = Long.MAX_VALUE;
            for (Integer source : sources) {
                if (prices[destination][source] < minPrice1) {
                    minPrice2 = minPrice1;
                    minPrice1 = prices[destination][source];
                } else if (prices[destination][source] < minPrice2) {
                    minPrice2 = prices[destination][source];
                }
            }
            bound2 += minPrice1 + minPrice2;
        }
        long bound = Math.min(bound1 / 2, bound2 / 2);
        for (Integer node : unseen) {
            bound += durations[node];
        }
        return bound;
    }
}
