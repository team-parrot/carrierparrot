/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.parser;

import fr.insalyon.teamparrot.carrierparrot.error.DeliveryRequestFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryWarehouse;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import java.util.ArrayList;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Compute a delivery request from an XML file. Uses SAX to parse the different elements.
 */
public class XMLDeliveryRequestParser {

    /**
     * For a given XML file path and the current map, generate the delivery request.
     * @param filePath The XML file path. If the file does not exist or if the file is invalid, exception is thrown.
     * @param map The current map instance.
     * @return The generated delivery request.
     * @throws DeliveryRequestFailedToLoadException Thrown if the file is not readable, or
     * the delivery request is irrelevant with current map.
     */
    public static DeliveryRequest parse(String filePath, Map map) throws DeliveryRequestFailedToLoadException {

        final DeliveryWarehouse[] warehouseContainer = {null};
        ArrayList<Delivery> deliveries = new ArrayList<>();

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DeliveryRequestSAXHandler handler = new DeliveryRequestSAXHandler(map, deliveries, warehouseContainer);

            saxParser.parse(filePath, handler);

            DeliveryRequest deliveryRequest = new DeliveryRequest(warehouseContainer[0], deliveries);
            if (deliveryRequest.getNumberOfDeliveries() <= 2) {
                throw new DeliveryRequestFailedToLoadException("The delivery request can't be empty.");
            }
            return deliveryRequest;
        } catch (Exception e) {
            throw new DeliveryRequestFailedToLoadException("Invalid delivery request file: " + e.getMessage());
        }
    }
}
