/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.model.business.parser;

import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryWarehouse;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PlaceFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;
import javax.validation.constraints.NotNull;

/**
 * SAX parse XML document by calling a Handler for each element. Unlike DOM, it does not construct a node tree.
 * This Handler parse elements needed to construct DeliveryRequest object.
 */
public class DeliveryRequestSAXHandler extends DefaultHandler {

    private Map map;
    private ArrayList<Delivery> deliveries;
    private DeliveryWarehouse[] warehouse;
    private HashMap<String, Function<Attributes, Boolean>> elementHandlers;

    /**
     * Constructor: needs Map, a delivery container and a warehouse container.
     * @param map The related map.
     * @param deliveries A delivery container.
     * @param warehouseContainer The warehouse container.
     */
    public DeliveryRequestSAXHandler(
        @NotNull Map map,
        @NotNull ArrayList<Delivery> deliveries,
        @NotNull DeliveryWarehouse[] warehouseContainer
    ) {
        this.map = map;
        this.deliveries = deliveries;
        this.warehouse = warehouseContainer;

        this.elementHandlers = new HashMap<>();
        this.elementHandlers.put("entrepot", this::extractWarehouse);
        this.elementHandlers.put("livraison", this::extractDelivery);
    }

    /**
     * Method called when SAX explore a new element.
     * @param uri Element URI
     * @param localName Local name, not used in this method.
     * @param qName The tag name of the element.
     * @param attributes Attributes of the element, stored in a bag.
     * @throws SAXException If the element parse fails, throws a SAXException.
     */
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        Function<Attributes, Boolean> handler = elementHandlers.get(qName);

        if (null != handler && !handler.apply(attributes)) {
            throw new SAXException("Unable to extract \"" + qName + "\" attribute.");
        }
    }

    /**
     * Specific warehouse data extractor.
     * @param attributes XML Element attributes.
     * @return Execution report : True if ok, false if any problem occurred.
     */
    private boolean extractWarehouse(Attributes attributes) {
        long address = Long.parseLong(attributes.getValue("adresse"));
        String[] departureHourComponents = attributes.getValue("heureDepart").split(":");
        for (int i = 0; i < 3; i++) {
            if (departureHourComponents[i].length() < 2) {
                departureHourComponents[i] = '0' + departureHourComponents[i];
            }
        }
        LocalTime departureHour = LocalTime.parse(
            departureHourComponents[0] +
                ':' +
                departureHourComponents[1] +
                ':' +
                departureHourComponents[2]
        );

        Node warehouseNode = map.getNodeFromId(address);
        if (null == warehouseNode) {
            // No corresponding node on map.
            // TODO : Make a more detailled "return bag" ?
            return false;
        }
        warehouse[0] = new DeliveryWarehouse(
            PlaceFactory.get(map, warehouseNode.getId()),
            departureHour
        );

        return true;
    }

    /**
     * Delivery data extractor.
     * @param attributes XML Element attributes.
     * @return Execution report : True if ok, false if any problem occurred.
     */
    private boolean extractDelivery(Attributes attributes) {
        LocalTime timeSlotStart = null;
        LocalTime timeSlotEnd = null;

        long address = Long.parseLong(attributes.getValue("adresse"));
        long duration = Long.parseLong(attributes.getValue("duree"));

        if (attributes.getValue("debutPlage") != null) {
            String[] timeSlotStartComponents = attributes.getValue("debutPlage").split(":");

            timeSlotStart = LocalTime.of(
                Integer.parseInt(timeSlotStartComponents[0]),
                Integer.parseInt(timeSlotStartComponents[1]),
                Integer.parseInt(timeSlotStartComponents[2])
            );
        }

        if (attributes.getValue("finPlage") != null) {
            String[] timeSlotEndComponents = attributes.getValue("finPlage").split(":");
            timeSlotEnd = LocalTime.of(
                Integer.parseInt(timeSlotEndComponents[0]),
                Integer.parseInt(timeSlotEndComponents[1]),
                Integer.parseInt(timeSlotEndComponents[2])
            );
        }

        Node deliveryNode = map.getNodeFromId(address);

        if (null == deliveryNode) {
            // No corresponding node on map.
            // TODO : Make a more detailled "return bag" ?
            return false;
        }

        Delivery delivery = new Delivery(
            PlaceFactory.get(map, deliveryNode.getId()),
            timeSlotStart,
            duration,
            timeSlotEnd
        );
        deliveries.add(delivery);

        return true;
    }

    public void endElement(String uri, String localName,
                           String qName) throws SAXException { }

    public void characters(char ch[], int start, int length) throws SAXException { }
}
