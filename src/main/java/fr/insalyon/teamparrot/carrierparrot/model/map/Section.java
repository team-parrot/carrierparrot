/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map;

/**
 * A section is a straight segment, composed of a length, an origin and a destination node.
 * Note: this class has a natural ordering that is inconsistent with equals.
 */
public class Section {

    private String streetName;
    private double length;

    private Node origin;
    private Node destination;

    public Section(String streetName, double length, Node origin, Node destination) {
        this.streetName = streetName;
        this.length = length;
        this.origin = origin;
        this.destination = destination;
    }

    public String getStreetName() {
        return streetName;
    }

    public double getLength() {
        return length;
    }

    public Node getOrigin() {
        return origin;
    }

    public Node getDestination() {
        return destination;
    }
}
