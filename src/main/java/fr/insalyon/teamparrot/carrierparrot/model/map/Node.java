/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map;

/**
 * Object representation of a map node.
 */
public class Node {
    
    protected long id;
    private long x;
    private long y;

    public Node(long id, long x, long y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public long getId() {
        return id;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    public boolean equals(Node n) {
        return id == n.getId();
    }

    public int hashCode() {
        return Long.hashCode(id);
    }
}
