/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * A delivery request is a simple container, gathering delivery warehouse and planned deliveries.
 * WARNING : This object DOES NOT contain the Round the delivery man will execute. There is not optimum or determined
 * order in the deliveries collection.
 * TODO : Implementing {@code Iterable<Delivery>} interface would be better.
 */
public class DeliveryRequest {

    private DeliveryWarehouse deliveryWarehouse;
    private LinkedList<Delivery> deliveries;

    /**
     * DeliveryRequest constructor. We extract the deliveries given in argument and add warehouse in a LinkedList.
     * @param deliveryWarehouse The delivery warehouse.
     * @param deliveries The deliveries.
     */
    public DeliveryRequest(DeliveryWarehouse deliveryWarehouse, Collection<Delivery> deliveries) {
        this.deliveryWarehouse = deliveryWarehouse;
        this.deliveries = new LinkedList<>(deliveries);

        this.deliveries.addFirst(deliveryWarehouse);
        this.deliveries.addLast(deliveryWarehouse);
    }

    public DeliveryWarehouse getDeliveryWarehouse() {
        return deliveryWarehouse;
    }

    public Iterator<Delivery> deliveriesIterator() {
        return deliveries.iterator();
    }

    public int getNumberOfDeliveries() {
        return deliveries.size();
    }
}
