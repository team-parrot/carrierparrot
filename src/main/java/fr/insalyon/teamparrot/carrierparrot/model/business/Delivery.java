/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business;

import javafx.application.Platform;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Observable;
import javax.validation.constraints.NotNull;

/**
 * A delivery is a simple object, containing the address (Place) to deliver, the time on-site (duration), and the
 * time slot requested by the client.
 */
public class Delivery extends Observable {

    private Place address;
    private long duration;
    private int deliveryNumber;

    private TimeSlot timeSlot;
    private LocalTime minArrivalTime;

    /**
     * Delivery model constructor. No specific actions are made here.
     *
     * @param address       Place to perform the delivery.
     * @param timeSlotStart The minimal time bound to do the delivery.
     * @param duration      The maximum time on-site for the delivery man.
     * @param timeSlotEnd   The maximal time bound to do the delivery.
     */
    public Delivery(Place address, LocalTime timeSlotStart, long duration, LocalTime timeSlotEnd) {
        this.address = address;
        this.duration = duration;
        if (null == timeSlotStart && null == timeSlotEnd) {
            this.timeSlot = null;
        } else {
            this.timeSlot = new TimeSlot(timeSlotStart, timeSlotEnd);
        }
        this.minArrivalTime = null;
    }

    public Place getAddress() {
        return address;
    }

    public long getDuration() {
        return duration;
    }

    public LocalTime getTimeSlotStart() {
        return null == timeSlot ? null : timeSlot.getStartTime();
    }

    /**
     * Set a value to the start attribute of the time slot of a Delivery.
     * @param timeSlotStart The value to set the said attribute with.
     */
    public void setTimeSlotStart(@NotNull LocalTime timeSlotStart) {
        Objects.requireNonNull(timeSlotStart);

        if (null == this.timeSlot) {
            timeSlot = new TimeSlot(null, null);
        }
        this.timeSlot.setStartTime(timeSlotStart);
        triggerMutation();
    }

    public LocalTime getTimeSlotEnd() {
        return null == timeSlot ? null : timeSlot.getEndTime();
    }

    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    /**
     * Set a value to the end attribute of the time slot of a Delivery.
     * @param timeSlotEnd The value to set the said attribute with.
     */
    public void setTimeSlotEnd(@NotNull LocalTime timeSlotEnd) {
        Objects.requireNonNull(timeSlotEnd);

        if (null == this.timeSlot) {
            timeSlot = new TimeSlot(null, null);
        }
        this.timeSlot.setEndTime(timeSlotEnd);
        triggerMutation();
    }

    public int getDeliveryNumber() {
        return deliveryNumber;
    }

    public void setDeliveryNumber(int deliveryNumber) {
        this.deliveryNumber = deliveryNumber;
    }

    public void setMinArrivalTime(LocalTime minArrivalTime) {
        this.minArrivalTime = minArrivalTime;
    }

    public LocalTime getMinArrivalTime() {
        return minArrivalTime;
    }

    public String toString() {
        return "Delivery n°" + deliveryNumber;
    }

    private void triggerMutation() {
        Platform.runLater(() -> {
            setChanged();
            notifyObservers();
        });
    }
}
