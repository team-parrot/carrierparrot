/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business;

import fr.insalyon.teamparrot.carrierparrot.Config;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import java.util.Collection;
import java.util.Iterator;

/**
 * A Path is a way to go from a delivery point to another, using the map sections (streets).
 */
public class Path implements Iterable<Section> {

    private Delivery start;
    private Delivery finish;
    private Collection<Section> sections;

    private double duration;

    public Path(Delivery start, Delivery finish, Collection<Section> sections) {
        this.start = start;
        this.finish = finish;
        this.sections = sections;
        this.duration = computeDuration(sections);
    }

    public Iterator<Section> iterator() {
        return sections.iterator();
    }

    public Delivery getStart() {
        return start;
    }

    public Delivery getFinish() {
        return finish;
    }

    /**
     * @return The minimum duration it gets to go from start to finish, in seconds.
     */
    public double getDuration() {
        return duration;
    }

    private static double computeDuration(Collection<Section> sections) {
        double duration = 0.;
        for (Section section : sections) {
            duration += section.getLength() / Config.DELIVERY_VELOCITY;
        }
        return duration;
    }
}
