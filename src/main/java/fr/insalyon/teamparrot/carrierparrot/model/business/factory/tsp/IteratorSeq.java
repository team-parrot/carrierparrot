/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A simple node iterator, which simply iterates over a list of given nodes.
 *
 * @author Christine Solnon, INSA Lyon
 */
public class IteratorSeq implements Iterator<Integer> {

    protected Integer[] candidates;
    private int nbCandidates;

    IteratorSeq(Collection<Integer> unseen) {
        this.candidates = new Integer[unseen.size()];
        this.nbCandidates = 0;
        for (Integer s : unseen) {
            candidates[nbCandidates++] = s;
        }
    }

    @Override
    public boolean hasNext() {
        return nbCandidates > 0;
    }

    @Override
    public Integer next() {
        if (nbCandidates == 0) {
            throw new NoSuchElementException();
        }
        return candidates[--nbCandidates];
    }
}
