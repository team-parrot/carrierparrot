/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.error.NoPathExistException;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PathFactory;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.RoundFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import javafx.application.Platform;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Spliterator;
import java.util.function.Consumer;


/**
 * A round is a sorted set of paths, linking a delivery to an other.
 */
public class Round extends Observable implements Iterable<Path> {

    protected Map map;

    private DeliveryWarehouse deliveryWarehouse;

    /**
     * 1st Path : Warehouse to 1st Delivery.
     * Last Path : last Delivery to Warehouse.
     */
    private List<Path> paths;

    public Round(Map map, DeliveryWarehouse deliveryWarehouse, List<Path> paths) {
        this.map = map;
        this.deliveryWarehouse = deliveryWarehouse;
        this.paths = paths;

        CarrierParrotApp.LOGGER.debug("Feasible: " + isPathListFeasible(paths));
    }

    public DeliveryWarehouse getDeliveryWarehouse() {
        return deliveryWarehouse;
    }

    public List<Path> getPaths() {
        return paths;
    }

    private void setPaths(List<Path> paths) {
        this.paths = paths;
        updateDeliveryNumbers();
        setChangedAndNotifyObservers();
    }

    public Map getMap() {
        return map;
    }

    @Override
    public Iterator<Path> iterator() {
        return paths.iterator();
    }

    @Override
    public void forEach(Consumer<? super Path> consumer) {
        paths.forEach(consumer);
    }

    @Override
    public Spliterator<Path> spliterator() {
        return paths.spliterator();
    }

    /**
     * Add a new delivery to the round.
     *
     * @param delivery the delivery to add.
     * @throws Exception if it's impossible to create a path to / from the new delivery.
     * @throws Exception if another error happened during the TSP, ie. no change made.
     */
    public void addAutomatically(Delivery delivery) throws Exception {
        // Get the list of deliveries
        List<Delivery> deliveries = new ArrayList<>();
        for (Path path : this) {
            deliveries.add(path.getStart());
        }
        deliveries.add(delivery);

        // Compute a new Round with the new delivery
        Round newRound = RoundFactory.get(map, deliveryWarehouse, deliveries); // throws if fails
        setPaths(newRound.paths);
    }

    /**
     * Add a delivery after a specific delivery of the round and verify if the new round is correct.
     * @param deliveryToAdd The new delivery we want to add.
     * @param position The position of the already existing delivery which will be before the new one.
     * @return whether the adding is feasible.
     * @throws NoPathExistException if out of bounds in paths.
     */
    public boolean addAt(Delivery deliveryToAdd, int position) throws NoPathExistException {
        List<Path> newPaths = new ArrayList<>(paths);

        Path firstPath = PathFactory.get(this.map, newPaths.get(position).getStart(), deliveryToAdd);
        Path secondPath = PathFactory.get(this.map, deliveryToAdd, newPaths.get(position).getFinish());

        newPaths.set(position, firstPath);
        newPaths.add(position + 1, secondPath);

        return changePathsIfFeasible(newPaths);
    }

    /**
     * Add a delivery after a specific delivery of the round and verify if the new round is correct.
     *
     * @param delivery The delivery to delete from the round.
     * @throws NoPathExistException if out of bounds in paths.
     */
    public void removeDelivery(Delivery delivery) throws NoPathExistException {
        final int deliveryPosition = delivery.getDeliveryNumber();

        // Get the deliveries before and after the selected one
        Delivery previousDelivery = paths.get(deliveryPosition - 1).getStart();
        Delivery nextDelivery = paths.get(deliveryPosition).getFinish();

        // Remove the paths between the previous delivery, the selected delivery and the nex delivery
        paths.remove(deliveryPosition - 1);
        paths.remove(deliveryPosition - 1);

        // Find the best path between the previous and the next deliveries
        Path newPath = PathFactory.get(map, previousDelivery, nextDelivery);

        // Add the calculated path to the list of paths of the round
        paths.add(deliveryPosition - 1, newPath);

        updateDeliveryNumbers();
        setChangedAndNotifyObservers();
    }

    /**
     * Update the deliveries' numbers in the round-trip.
     * Heads-up: this method doesn't notify the observers.
     */
    public void updateDeliveryNumbers() {
        int i = 0;
        for (Path path : this) {
            path.getStart().setDeliveryNumber(i);
            i++;
        }
    }

    /**
     * Change the time slot of @delivery to (@startTime, @endTime), if the round is still feasible. It throws an
     * explicit exception if it's not possible.
     *
     * @param delivery  the delivery to modify.
     * @param startTime the beginning of the time slot.
     * @param endTime   the end of the time slot.
     * @throws Exception if it's impossible to change the time slots.
     */
    public void changeDeliveryTimeSlots(Delivery delivery, LocalTime startTime, LocalTime endTime) throws Exception {

        // Save the original time slot
        TimeSlot originalTimeSlot = new TimeSlot(delivery.getTimeSlotStart(), delivery.getTimeSlotEnd());

        // Modify time slots
        if (null != startTime) {
            delivery.setTimeSlotStart(startTime);
        }
        if (null != endTime) {
            delivery.setTimeSlotEnd(endTime);
        }

        // Get the list of deliveries
        List<Delivery> deliveries = new ArrayList<>();
        for (Path path : this) {
            deliveries.add(path.getStart());
        }

        // Compute a new Round with the new delivery
        Round newRound;
        try {
            newRound = RoundFactory.get(map, deliveryWarehouse, deliveries);
        } catch (Exception e) {
            delivery.setTimeSlotStart(originalTimeSlot.getStartTime());
            delivery.setTimeSlotEnd(originalTimeSlot.getEndTime());
            throw e;
        }
        setPaths(newRound.paths);
    }

    /**
     * Verify if the path is feasible or not.
     *
     * @param paths List of Path of the round.
     * @return whether there are slotTime conflicts.
     */
    private static boolean isPathListFeasible(List<Path> paths) {
        LocalTime currentTime = paths.get(0).getStart().getTimeSlotStart();
        for (Path path : paths) {
            if (!(path.getFinish() instanceof DeliveryWarehouse)) {

                currentTime = currentTime.plusSeconds((long) path.getDuration());
                if (currentTime.isBefore(path.getFinish().getTimeSlotStart())) {
                    currentTime = path.getFinish().getTimeSlotStart();
                    currentTime = currentTime.plusSeconds(path.getFinish().getDuration());
                } else if (currentTime.isAfter(path.getFinish().getTimeSlotStart())
                        && currentTime.isBefore(path.getFinish().getTimeSlotEnd())) {
                    currentTime = currentTime.plusSeconds(path.getFinish().getDuration());
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Check the feasability of a new list of paths, and change the round if it is.
     * @param newPaths The new list of paths.
     * @return whether it has been modified.
     */
    private boolean changePathsIfFeasible(List<Path> newPaths) {
        if (isPathListFeasible(newPaths)) {
            setPaths(newPaths);
            return true;
        } else {
            return false;
        }
    }

    private void setChangedAndNotifyObservers() {
        Platform.runLater(() -> {
            setChanged();
            notifyObservers();
        });
    }
}
