/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.model.map.parser;

import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

/**
 * SAX parse XML document by calling a Handler for each element. Unlike DOM, it does not construct a node tree.
 * This Handler parse node and section element when SAX discovers them.
 */
public class MapSAXHandler extends DefaultHandler {

    private HashMap<Long, Node> nodes;
    private ArrayList<Section> sections;
    private HashMap<String, Function<Attributes, Boolean>> elementHandlers;

    /**
     * Constructor : Needs non-null nodes hashmap and sections list. The handler will just populate these collections.
     * @param nodes The nodes hashmap (key: node ID).
     * @param sections The sections list.
     */
    public MapSAXHandler(HashMap<Long, Node> nodes, ArrayList<Section> sections) {
        this.nodes = nodes;
        this.sections = sections;

        this.elementHandlers = new HashMap<>();
        this.elementHandlers.put("noeud", this::extractNode);
        this.elementHandlers.put("troncon", this::extractSection);
    }

    /**
     * Method called when SAX explore a new element.
     * @param uri Element URI
     * @param localName Local name, not used in this method.
     * @param qName The tag name of the element.
     * @param attributes Attributes of the element, stored in a bag.
     * @throws SAXException If the element parse fails, throws a SAXException.
     */
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        Function<Attributes, Boolean> handler = elementHandlers.get(qName);
        if (null != handler && !handler.apply(attributes)) {
            throw new SAXException("Unable to extract " + qName + "attribute.");
        }
    }

    /**
     * Generate a new Node object from a "noeud" element in XML file.
     * Be careful, x and y are swapped here.
     * @param attributes Attributes of the "noeud" element.
     * @return Execution report.
     */
    private boolean extractNode(Attributes attributes) {
        long id = Long.parseLong(attributes.getValue("id"));
        long y = -Long.parseLong(attributes.getValue("x"));
        long x = Long.parseLong(attributes.getValue("y"));

        Node node = new Node(id, x, y);
        nodes.put(node.getId(), node);

        return true;
    }

    /**
     * Generate a new Section object from a "troncon" element in XML file.
     * @param attributes Attributes of the "troncon" element.
     * @return Execution report. False if a node composing the section is not found in the map.
     */
    private boolean extractSection(Attributes attributes) {
        String streetName = attributes.getValue("nomRue");
        double length = Double.parseDouble(attributes.getValue("longueur"));
        long originId = Long.parseLong(attributes.getValue("origine"));
        long destinationId = Long.parseLong(attributes.getValue("destination"));

        Node origin = nodes.get(originId);
        Node destination = nodes.get(destinationId);
        if (null == origin || null == destination) {
            return false;
        }
        Section section = new Section(streetName, length, origin, destination);
        sections.add(section);

        return true;
    }
}
