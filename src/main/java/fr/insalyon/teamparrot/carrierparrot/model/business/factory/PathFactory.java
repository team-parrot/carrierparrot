/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.model.business.factory;

import fr.insalyon.teamparrot.carrierparrot.error.NoPathExistException;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Place;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import javax.annotation.Nullable;


/**
 * Generate Path between two deliveries, using Dijkstra's algorithm.
 */
public class PathFactory {

    private static HashMap<Place, HashMap<Place, Path>> pathCache = new HashMap<>();

    /**
     * Determine the optimal path between two nodes of the graph, using Dijkstra's algorithm.
     * @param map The related map instance.
     * @param a Start delivery point
     * @param b End delivery point
     * @return Minimal path between A and B.
     * @throws NoPathExistException If the path does not exist, throws an exception.
     */
    public static Path get(Map map, Delivery a, Delivery b) throws NoPathExistException {

        // Return cached path, if already computed
        Path cachedPath = getFromCache(a, b);
        if (null != cachedPath) {
            return cachedPath;
        }

        // Compute the path
        HashMap<Node, PredecessorData> predecessors = new HashMap<>();
        HashSet<Node> marked = new HashSet<>();

        HashMap<Node, ScoredNode> scoredNodeStore = new HashMap<>();
        PriorityQueue<ScoredNode> queue = new PriorityQueue<>();

        queue.add(new ScoredNode(a.getAddress().getNode(), 0));

        // Dijkstra things...

        ScoredNode current;
        PredecessorData predecessor;
        ScoredNode nextNode;

        while (!queue.isEmpty() && !marked.contains(b.getAddress().getNode())) {
            current = queue.poll();

            if (!marked.contains(current.getNode())) {

                for (Section s : map.getSectionsFromOriginId(current.getNode().getId())) {

                    nextNode = scoredNodeStore.get(s.getDestination());
                    predecessor = predecessors.get(s.getDestination());
                    double newLength = current.getScore() + s.getLength();

                    // We haven't explored the node before.
                    if (null == predecessor) {
                        predecessor = new PredecessorData(s.getDestination(), current.getNode(), s);
                        nextNode = new ScoredNode(s.getDestination(), newLength);

                        predecessors.put(predecessor.getNode(), predecessor);
                        scoredNodeStore.put(nextNode.getNode(), nextNode);

                        queue.add(nextNode);

                    } else if (newLength < nextNode.getScore()) {
                        predecessor.setOriginNode(current.getNode());
                        predecessor.setOriginSection(s);

                        nextNode.setScore(newLength);

                        queue.remove(nextNode);
                        queue.add(nextNode);
                    }
                }

                marked.add(current.getNode());
            }
        }

        List<Section> sectionList = new ArrayList<>();

        predecessor = predecessors.get(b.getAddress().getNode());

        if (null == predecessor) {
            throw new NoPathExistException("An error occured, please check your map file.");
        }

        while (null != predecessor && !predecessor.getNode().equals(a.getAddress().getNode())) {
            sectionList.add(predecessor.originSection);
            predecessor = predecessors.get(predecessor.getOriginNode());
        }

        Collections.reverse(sectionList);
        Path path = new Path(a, b, sectionList);
        cachePath(a, b, path);
        return path;
    }

    private static Path getFromCache(Delivery a, Delivery b) {
        HashMap<Place, Path> aCache = pathCache.get(a.getAddress());
        return (null != aCache) ? aCache.get(b.getAddress()) : null;
    }

    private static void cachePath(Delivery a, Delivery b, Path path) {
        pathCache.computeIfAbsent(a.getAddress(), ignored -> new HashMap<>());
        pathCache.get(a.getAddress()).put(b.getAddress(), path);
    }

    private static class PredecessorData {

        private Node node;
        private Node originNode;
        private Section originSection;

        PredecessorData(Node node, @Nullable Node originNode, @Nullable Section originSection) {
            this.node = node;
            this.originNode = originNode;
            this.originSection = originSection;
        }

        Node getNode() {
            return node;
        }

        void setOriginNode(Node originNode) {
            this.originNode = originNode;
        }

        void setOriginSection(Section originSection) {
            this.originSection = originSection;
        }

        Node getOriginNode() {
            return originNode;
        }

        Section getOriginSection() {
            return originSection;
        }

    }

    private static class ScoredNode implements Comparable<ScoredNode> {
        private Node node;
        private double score;

        ScoredNode(Node node, double score) {
            this.node = node;
            this.score = score;
        }

        Node getNode() {
            return node;
        }

        void setNode(Node node) {
            this.node = node;
        }

        double getScore() {
            return score;
        }

        void setScore(double score) {
            this.score = score;
        }

        @Override
        public int compareTo(ScoredNode scoredNode) {
            return Double.compare(score, scoredNode.score);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof ScoredNode && ((ScoredNode) obj).compareTo(this) == 0;
        }

        @Override
        public int hashCode() {
            return Double.hashCode(score);
        }
    }

}
