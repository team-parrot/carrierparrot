/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory;

import com.google.common.collect.Lists;
import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.Config;
import fr.insalyon.teamparrot.carrierparrot.error.NoFeasibleRoundException;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.MessageEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryWarehouse;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp.TSP;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp.TSPSimpleBoundIncreasingIteratorImpl;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * The RoundFactory generates for a given delivery request an optimized round, thanks to TSP.
 */
public class RoundFactory {

    /**
     * Compute the optimal round from a delivery request.
     *
     * @param map the map in which the round takes place.
     * @param deliveryRequest the delivery request from which the round is computed.
     * @return the optimal round.
     * @throws Exception if anything bad happen.
     */
    public static Round get(Map map, DeliveryRequest deliveryRequest) throws Exception {
        // Get the list of deliveries
        ArrayList<Delivery> deliveries = Lists.newArrayList(deliveryRequest.deliveriesIterator());
        deliveries.remove(deliveries.size() - 1); // the last is the warehouse

        return get(map, deliveryRequest.getDeliveryWarehouse(), deliveries);
    }

    /**
     * Compute the optimal round from a delivery request.
     *
     * @param map          the map in which the round takes place.
     * @param warehouse    the round's warehouse.
     * @param deliveryList the list of deliveries with which the round is computed.
     * @return the optimal round.
     * @throws Exception if anything bad happen.
     */
    public static Round get(Map map, DeliveryWarehouse warehouse, List<Delivery> deliveryList) throws Exception {
        return new Round(map, warehouse,
                getOptimalRound(map, deliveryList, new TSPSimpleBoundIncreasingIteratorImpl()));
    }

    /**
     * THIS METHOD IS FOR TEST PURPOSES ONLY.
     * Compute the optimal round from a delivery request.
     *
     * @param map             the map in which the round takes place.
     * @param deliveryRequest the delivery request from which the round is computed.
     * @param tsp             the TSP implementation used for the computation.
     * @return the optimal round.
     * @throws Exception if anything bad happen.
     */
    @SuppressWarnings("unused")
    static Round getWithCustomTSP(Map map, DeliveryRequest deliveryRequest, TSP tsp) throws Exception {
        // Get the list of deliveries
        ArrayList<Delivery> deliveries = Lists.newArrayList(deliveryRequest.deliveriesIterator());
        deliveries.remove(deliveries.size() - 1); // the last is the warehouse

        return getWithCustomTSP(map, deliveryRequest.getDeliveryWarehouse(), deliveries, tsp);
    }

    /**
     * THIS METHOD IS FOR TEST PURPOSES ONLY.
     * Compute the optimal round from a delivery request.
     *
     * @param map          the map in which the round takes place.
     * @param warehouse    the round's warehouse.
     * @param deliveryList the list of deliveries with which the round is computed.
     * @param tsp          the TSP implementation used for the computation.
     * @return the optimal round.
     * @throws Exception if anything bad happen.
     */
    @SuppressWarnings("unused")
    static Round getWithCustomTSP(Map map, DeliveryWarehouse warehouse, List<Delivery> deliveryList, TSP tsp)
            throws Exception {
        return new Round(map, warehouse, getOptimalRound(map, deliveryList, tsp));
    }

    /**
     * Compute the price matrix, ie the matrix containing, for 0 <= i,j < nbDeliveries, the price to go from delivery i
     * to delivery j.
     *
     * @param map the map in which the round takes place.
     * @param deliveries the list of every deliveries.
     * @return the price matrix.
     * @throws Exception if anything bad happen.
     */
    private static long[][] getPrices(Map map, List<Delivery> deliveries) throws Exception {
        long[][] prices = new long[deliveries.size()][deliveries.size()];

        // Save the starting time
        long duration = System.currentTimeMillis();

        // Fill prices
        EventBus.post(new MessageEvent("Computing optimal route (computing prices between places)"));
        // TODO: Floyd–Warshall ?
        for (int i = 0; i < deliveries.size(); ++i) {
            for (int j = 0; j < deliveries.size(); ++j) {
                if (i == j) {
                    prices[i][j] = Long.MAX_VALUE;
                } else {
                    prices[i][j] = (long) PathFactory.get(map, deliveries.get(i), deliveries.get(j)).getDuration();
                }
            }
        }

        // Log the time consumed to compute prices
        duration = System.currentTimeMillis() - duration;
        CarrierParrotApp.LOGGER.debug("prices: " + duration / 1000. + " sec.");

        return prices;
    }

    /**
     * Construct the duration array, containing, for 0 <= i < nbDeliveries, the i-th delivery's duration.
     *
     * @param deliveries the list of every deliveries.
     * @return the duration array.
     * @throws Exception if anything bad happen.
     */
    private static long[] getDurations(List<Delivery> deliveries) throws Exception {
        long[] durations = new long[deliveries.size()];

        for (int i = 0; i < deliveries.size(); ++i) {
            durations[i] = deliveries.get(i).getDuration();
        }

        return durations;
    }

    /**
     * Construct the time slot array, containing, for 0 <= i < nbDeliveries, the i-th delivery's time slot.
     *
     * @param deliveries the list of every deliveries.
     * @return the time slot array.
     * @throws Exception if anything bad happen.
     */
    private static TimeSlot[] getTimeSlots(List<Delivery> deliveries) throws Exception {
        TimeSlot[] timeSlots = new TimeSlot[deliveries.size()];

        for (int i = 0; i < deliveries.size(); ++i) {
            timeSlots[i] = deliveries.get(i).getTimeSlot();
        }

        return timeSlots;
    }

    /**
     * Debug logs the computed path contained in a TSP instance.
     *
     * @param tsp the TSP instance.
     * @param numberOfDeliveries the number of deliveries in the round.
     * @param durationInMs the duration of the computing in milliseconds.
     */
    @SuppressWarnings("unused")
    private static void debugPrintPath(TSP tsp, int numberOfDeliveries, long durationInMs) {
        CarrierParrotApp.LOGGER.debug("TSP (" + numberOfDeliveries + "): " + (durationInMs / 1000.) + " sec.");
        if (tsp.hasSolution()) {
            CarrierParrotApp.LOGGER.debug("Length: " + tsp.getBestSolutionPrice());
            StringBuilder pathDebug = new StringBuilder("Path: ");
            for (int i = 0; i < numberOfDeliveries; ++i) {
                pathDebug.append(tsp.getBestSolutionAt(i)).append(" ");
            }
            CarrierParrotApp.LOGGER.debug(pathDebug.toString());
        } else {
            CarrierParrotApp.LOGGER.debug("No solution.");
        }
    }

    /**
     * Return the list of path computed by a TSP instance.
     *
     * @param map the map in which the round takes place.
     * @param tsp the TSP instance.
     * @param deliveries the list of all the deliveries.
     * @return the list of path.
     * @throws Exception if anything bad happen.
     */
    private static List<Path> getPathList(Map map, TSP tsp, List<Delivery> deliveries) throws Exception {
        final int nbNodes = deliveries.size();

        List<Path> paths = new ArrayList<>();
        for (int i = 0; i < nbNodes - 1; ++i) {
            Path path = PathFactory.get(map,
                    deliveries.get(tsp.getBestSolutionAt(i)),
                    deliveries.get(tsp.getBestSolutionAt(i + 1)));
            path.getStart().setDeliveryNumber(i);
            paths.add(path);
        }
        Path path = PathFactory.get(map,
                deliveries.get(tsp.getBestSolutionAt(nbNodes - 1)),
                deliveries.get(tsp.getBestSolutionAt(0)));
        path.getStart().setDeliveryNumber(nbNodes - 1);
        path.getFinish().setDeliveryNumber(0);
        paths.add(path);

        return paths;
    }

    /**
     * Truncate a time to the nearest half hour.
     *
     * @param time the time to truncate.
     * @return the truncated time.
     */
    private static LocalTime truncateToNearestHalfHour(LocalTime time) {
        time = time.truncatedTo(ChronoUnit.MINUTES);
        if (time.getMinute() < 15) {
            time = time.truncatedTo(ChronoUnit.HOURS);
        } else if (time.getMinute() < 30) {
            time = time.plusMinutes(30 - time.getMinute());
        } else if (time.getMinute() < 45) {
            time = time.minusMinutes(time.getMinute() - 30);
        } else {
            time = time.truncatedTo(ChronoUnit.HOURS).plusHours(1);
        }
        return time;
    }

    /**
     * Compute the optimal round for the TSP problem, as a list of paths.
     * The computation takes into account time slots.
     *
     * @param map The related map instance.
     * @param deliveryList the list of deliveries with which the round is computed.
     * @param tsp the TSP implementation used for the computation.
     * @return The ordered list of paths.
     * @throws Exception In case of no path between two deliveries, throw an exception.
     */
    private static List<Path> getOptimalRound(Map map, List<Delivery> deliveryList, TSP tsp)
            throws Exception {

        int nbNodes = deliveryList.size();

        // Compute the round
        long duration = System.currentTimeMillis();
        final long[][] prices = getPrices(map, deliveryList);
        final long[] durations = getDurations(deliveryList);
        final TimeSlot[] timeSlots = getTimeSlots(deliveryList);
        EventBus.post(new MessageEvent("Computing optimal route (branch&bound)"));
        tsp.computeSolution(
                nbNodes,
                prices,
                durations,
                timeSlots
        );
        duration = System.currentTimeMillis() - duration;
        debugPrintPath(tsp, nbNodes, duration);

        // Construct the round
        if (tsp.hasSolution()) {

            // TODO: Do this in the post-process method
            for (int i = 0; i < deliveryList.size(); ++i) {
                deliveryList.get(tsp.getBestSolutionAt(i)).setMinArrivalTime(tsp.getMinArrivalTime(i));
            }

            for (int i = 1; i < deliveryList.size(); ++i) {
                Delivery delivery = deliveryList.get(i);

                if (null == delivery.getTimeSlotStart()) {
                    delivery.setTimeSlotStart(truncateToNearestHalfHour(delivery.getMinArrivalTime()
                            .minusSeconds((long) (Config.AUTO_TIMESLOT_WIDTH / 2.))));
                }
                if (null == delivery.getTimeSlotEnd()) {
                    delivery.setTimeSlotEnd(truncateToNearestHalfHour(delivery.getMinArrivalTime()
                            .plusSeconds((long) (Config.AUTO_TIMESLOT_WIDTH / 2.))));
                }
            }

            EventBus.post(new MessageEvent("Computing optimal route (constructing the path)"));
            return getPathList(map, tsp, deliveryList);
        } else {
            throw new NoFeasibleRoundException("Impossible to create a round which satisfies the delivery request.");
        }
    }
}
