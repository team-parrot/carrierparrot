/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp;

import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import java.util.Iterator;
import java.util.List;

/**
 * Naive iterator and bound function for the TSP.
 *
 * Bound: returns 0.
 * Iterator: unsorted.
 *
 * @author Christine Solnon, INSA Lyon
 */
@SuppressWarnings("unused")
public class TSPNaiveImpl extends TSPBranchAndBound {

    /**
     * Return a simple unsorted iterator.
     */
    @Override
    protected Iterator<Integer> iterator(
            int currentNode,
            List<Integer> unseen,
            long[][] prices,
            long[] durations,
            TimeSlot[] timeSlots) {
        return new IteratorSeq(unseen);
    }

    /**
     * A naive bound which just returns 0.
     */
    @Override
    long bound(int currentNode, List<Integer> unseen,
                         long[][] prices, long[] durations, TimeSlot[] timeSlots) {
        return 0;
    }
}
