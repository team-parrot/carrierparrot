/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map.parser;

import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Extract a Map object, containing nodes and sections, from an XML File.
 */
public class XMLMapParser {

    public XMLMapParser() throws InstantiationException {
        throw new InstantiationException("You can't instantiate this class. Please use parse().");
    }

    /**
     * For a given file path, generate a map.
     * @param filePath The XML file path. If invalid, an exception is thrown.
     * @return The generated map.
     * @throws MapFailedToLoadException Thrown if the file is invalid (empty, corrupted, incoherent, etc.)
     */
    public static Map parse(String filePath) throws MapFailedToLoadException {

        HashMap<Long, Node> nodes = new HashMap<>();
        ArrayList<Section> sections = new ArrayList<>();

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            MapSAXHandler handler = new MapSAXHandler(nodes, sections);

            saxParser.parse(filePath, handler);

            Map map = new Map(nodes.values(), sections);
            if (map.getNumberOfSections() == 0 || map.getNumberOfNodes() == 0) {
                throw new MapFailedToLoadException("The map can't be empty.");
            }
            return map;
        } catch (Exception e) {
            throw new MapFailedToLoadException("Invalid map file: " + e.getMessage());
        }
    }
}
