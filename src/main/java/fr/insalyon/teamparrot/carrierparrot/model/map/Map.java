/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A representation of extracted map file. This class offers iterators for both sections and nodes, which are indexed
 * with the node id. For sections, the index is related to origin or destination node id.
 */
public class Map {

    private Multimap<Long, Section> sectionsFromOrigin;
    private Multimap<Long, Section> sectionsFromDestination;
    private HashMap<Long, Node> nodes;

    /**
     * protected map constructor, used in public ones.
     */
    protected Map() {
        nodes = new HashMap<>();
        sectionsFromOrigin      = MultimapBuilder.hashKeys().arrayListValues().build();
        sectionsFromDestination = MultimapBuilder.hashKeys().arrayListValues().build();
    }

    /**
     *
     * @param rawNodes A node collection.
     * @param rawSections A section collection.
     */
    public Map(Collection<Node> rawNodes, Collection<Section> rawSections) {
        this();

        buildNodes(rawNodes);
        buildSections(rawSections);

    }

    private void buildNodes(Collection<Node> rawNodes) {
        for (Node node : rawNodes) {
            nodes.put(node.getId(), node);
        }
    }

    private void buildSections(Collection<Section> rawSections) {
        for (Section section: rawSections) {
            sectionsFromOrigin.put(section.getOrigin().getId(), section);
            sectionsFromDestination.put(section.getDestination().getId(), section);
        }
    }

    public Node getNodeFromId(Long id) {
        return nodes.get(id);
    }

    public Collection<Section> getSectionsFromOriginId(Long originId) {
        return sectionsFromOrigin.get(originId);
    }

    public Collection<Section> getSectionsFromDestinationId(Long destinationId) {
        return sectionsFromDestination.get(destinationId);
    }

    public Iterator<Section> sectionsIterator() {
        return sectionsFromOrigin.values().iterator();
    }

    public Iterator<Node> nodesIterator() {
        return nodes.values().iterator();
    }

    public int getNumberOfSections() {
        return this.sectionsFromOrigin.size();
    }

    public int getNumberOfNodes() {
        return this.nodes.size();
    }

    /**
     * Find and the return the nearest node from a right click on the map
     * while in adding mode.
     *
     * @param x Abscissa of the click.
     * @param y Ordinate of the click.
     * @return The nearest node of those coordinates within the Map.
     */
    public Node getNearestNode(long x, long y) {
        // TODO: Use a faster data structure

        double xd = (double) x;
        double yd = (double) y;

        Node nearest = null;
        double nearestDist = Double.MAX_VALUE;
        for (Node node : nodes.values()) {
            double nodeX = (double) node.getX();
            double nodeY = (double) node.getY();
            final double dist = Math.sqrt((nodeX - xd) * (nodeX - xd) + (nodeY - yd) * (nodeY - yd));
            if (dist < nearestDist) {
                nearest = node;
                nearestDist = dist;
            }
        }
        return nearest;
    }
}
