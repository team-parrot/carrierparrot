/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp;

import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import java.time.LocalTime;

/**
 * A TSP interface.
 * @author Christine Solnon, INSA Lyon
 */
public interface TSP {

    /**
     * @return true if computeSolution() terminated because it exceeded time limit before finishing to explore the whole
     * research space, ie if it doesn't have a solution or of it's unsure it's optimal.
     */
    boolean getReachedLimitTime();

    /**
     * Search the round with the minimal duration passing by every nodes (included in [0..nbNodes-1]).
     *
     * @param nbNodes   : number of the nodes of the graph.
     * @param prices    : prices[i][j] = duration to go from i to j, with 0 &lt;= i &lt; nbNodes et 0 &lt;= j &lt; nbNodes.
     * @param durations : durations[i] = duration to visit the node i, with 0 &lt;= i &lt; nbNodes.
     * @param timeSlots : timeSlots[i] = time slot of node i, with 0 &lt;= i &lt; nbNodes.
     */
    void computeSolution(int nbNodes, long[][] prices, long[] durations, TimeSlot[] timeSlots);

    /**
     * @return whether there is a solution.
     */
    boolean hasSolution();

    /**
     * @param i the node index.
     * @return the i-th node in the computed round
     */
    Integer getBestSolutionAt(int i);

    LocalTime getMinArrivalTime(int i);

    /**
     * @return the duration of the computed round
     */
    long getBestSolutionPrice();
}
