/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot;

import javafx.scene.paint.Color;
import java.time.LocalTime;

/**
 * Basic configuration constants provider for CarrierParrot.
 */
public class Config {

    /**
     * The delivery men speed, in m/s.
     */
    public static final double DELIVERY_VELOCITY = 15 / 3.6;

    /**
     * The map background color.
     */
    public static final Color MAP_BACKGROUND_COLOR = Color.grayRgb(40);

    /**
     * The conversion factor between coordinates and pixels.
     * 1 px = 1 coordinate unit / MAP_SCALE_FACTOR
     */
    public static final double MAP_SCALE_FACTOR = 100.;

    /**
     * The width (duration) of the auto-assigned time slots, in seconds.
     */
    public static final double AUTO_TIMESLOT_WIDTH = 1. * 3600.;

    /**
     * The TSP time limit, in seconds.
     */
    public static final double TIME_LIMIT_SEC = 60. * 60.;

    public static final LocalTime START_OF_DAY = LocalTime.of(7, 0);
    public static final LocalTime END_OF_DAY = LocalTime.of(21, 0);
}
