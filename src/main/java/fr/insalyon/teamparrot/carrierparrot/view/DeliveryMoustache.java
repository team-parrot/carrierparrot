/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.view;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Cursor;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import static fr.insalyon.teamparrot.carrierparrot.controller.SynopticComponentController.timeToSeconds;
import static fr.insalyon.teamparrot.carrierparrot.controller.SynopticComponentController.timeToWidth;

/**
 * A view that represents a delivery as a box, or in French "une boîte à moustache".
 * The delivery time slot is represented by a white line.
 * The time needed for transportation is an orange rectangle.
 * The time needed at delivery location is colored in green.
 */
public class DeliveryMoustache extends Pane {

    private Line timeline;
    private Line baseline;
    private Rectangle transportationLine;
    private Rectangle deliveryLine;

    /**
     * Component constructor : create sub elements and convert buisiness model attributes to useful data.
     * @param globalWidthProperty The global timeline width
     * @param p The path displayed by the component
     */
    public DeliveryMoustache(DoubleProperty globalWidthProperty, Path p) {
        setCursor(Cursor.HAND);

        double arrivalInSeconds = timeToSeconds(p.getFinish().getMinArrivalTime());
        double startSeconds = arrivalInSeconds - p.getDuration();
        double endSeconds = arrivalInSeconds + p.getFinish().getDuration();

        double timeslotStart = (null == p.getFinish().getTimeSlotStart()) ?
                startSeconds :
                timeToSeconds(p.getFinish().getTimeSlotStart());
        double timeslotEnd = (null == p.getFinish().getTimeSlotEnd()) ?
                endSeconds :
                timeToSeconds(p.getFinish().getTimeSlotEnd());

        double baselineStart = timeToWidth(timeslotStart);
        double baselineEnd = timeToWidth(timeslotEnd);
        double lineTransitionWidth = timeToWidth(arrivalInSeconds);
        double startWidth = timeToWidth(startSeconds);
        double endWidth = timeToWidth(endSeconds);

        timeline = new Line();
        timeline.setStartX(0);
        timeline.setStartY(6);
        timeline.endYProperty().bind(timeline.startYProperty());
        timeline.endXProperty().bind(globalWidthProperty);
        timeline.setStroke(Color.DARKGRAY);
        timeline.setStrokeWidth(1);
        timeline.setStrokeLineCap(StrokeLineCap.ROUND);

        baseline = new Line();
        baseline.setStartX(0);
        baseline.setStartY(6);
        baseline.endYProperty().bind(baseline.startYProperty());
        baseline.setStroke(Color.WHITE);
        baseline.setStrokeWidth(1);
        baseline.setStrokeLineCap(StrokeLineCap.ROUND);
        baseline.startXProperty().bind(globalWidthProperty.multiply(baselineStart));
        baseline.endXProperty().bind(globalWidthProperty.multiply(baselineEnd));

        transportationLine = new Rectangle();
        transportationLine.setHeight(12);
        transportationLine.setFill(Color.ORANGERED);
        transportationLine.xProperty().bind(globalWidthProperty.multiply(startWidth));
        transportationLine.widthProperty().bind(globalWidthProperty.multiply(lineTransitionWidth)
                .subtract(globalWidthProperty.multiply(startWidth)));

        deliveryLine = new Rectangle();
        deliveryLine.setHeight(12);
        deliveryLine.setFill(Color.GREEN);
        deliveryLine.xProperty().bind(globalWidthProperty.multiply(lineTransitionWidth));
        deliveryLine.widthProperty().bind(globalWidthProperty.multiply(endWidth)
                .subtract(globalWidthProperty.multiply(lineTransitionWidth)));

        getChildren().addAll(timeline, baseline, transportationLine, deliveryLine);

        CarrierParrotApp.LOGGER.debug(startSeconds + " - " + arrivalInSeconds + " - " + endSeconds);
        CarrierParrotApp.LOGGER.debug(startWidth + " - " + lineTransitionWidth + " - " + endWidth);
    }

    // TODO : Decorator design pattern ?
    public void selectedColorStyle() {
        baseline.setStroke(Color.ROYALBLUE);
        transportationLine.setFill(Color.ROYALBLUE);
        deliveryLine.setFill(Color.DARKBLUE);
    }

    public void classicColorStyle() {
        baseline.setStroke(Color.WHITE);
        transportationLine.setFill(Color.ORANGERED);
        deliveryLine.setFill(Color.GREEN);
    }

    public void previousColorStyle() {
        baseline.setStroke(Color.LIGHTGREEN);
        transportationLine.setFill(Color.LIGHTGREEN);
        deliveryLine.setFill(Color.DARKGREEN);
    }

    public void nextColorStyle() {
        baseline.setStroke(Color.ORANGE);
        transportationLine.setFill(Color.ORANGERED);
        deliveryLine.setFill(Color.DARKORANGE);
    }
}
