/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.view;

import fr.insalyon.teamparrot.carrierparrot.event.DeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryWarehouse;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * A numbered marker for deliveries on the map.
 */
public class DeliveryMarker extends StackPane implements Observer {

    private Rectangle rectangle;

    private Delivery delivery;

    private Label numberLabel;

    /**
     * Construct the marker, using deliveryMarker.fxml as layout.
     *
     * @param x        the x layout position of the marker.
     * @param y        the y layout position of the marker.
     * @param delivery the delivery represented by the marker.
     */
    public DeliveryMarker(double x, double y, Delivery delivery) {
        this.delivery = delivery;

        try {
            // It's a warehouse
            if (delivery instanceof DeliveryWarehouse) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/warehouseMarker.fxml"));
                fxmlLoader.setRoot(this);
                fxmlLoader.load();

            } else { // It's a simple delivery

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/deliveryMarker.fxml"));
                fxmlLoader.setRoot(this);
                fxmlLoader.load();

                // Change the label text to the delivery number
                numberLabel = (Label) lookup("#numberLabel");
                numberLabel.setText(String.valueOf(delivery.getDeliveryNumber()));

                this.rectangle = (Rectangle) lookup("#rectangle");
            }

            // Handle the click on the marker
            setOnMouseClicked((event) -> EventBus.post(new DeliverySelectedEvent(this.delivery, null)));

            // Set the marker's position
            setLayoutX(x - getBoundsInLocal().getWidth() / 2);
            setLayoutY(y - getBoundsInLocal().getHeight() / 2);

            // Move the marker to foreground on mouse hover
            setOnMouseEntered(ignored -> moveToForeground());

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void moveToForeground() {
        if (getParent() instanceof Group) {
            Group group = (Group) getParent();
            group.getChildren().remove(this);
            group.getChildren().add(this);
        }
    }

    /**
     * Highlight or unhighlight the deliveryMarker (if it's not a warehouse).
     *
     * @param highlighted true to highlight the marker and false to unhighlight it.
     */
    public void setHighlighted(boolean highlighted) {
        if (!(delivery instanceof DeliveryWarehouse)) {
            if (highlighted) {
                this.rectangle.getStyleClass().add("highlighted-delivery");
            } else {
                this.rectangle.getStyleClass().remove("highlighted-delivery");
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (!(delivery instanceof DeliveryWarehouse)) {
            numberLabel.setText(String.valueOf(delivery.getDeliveryNumber()));
        }
    }
}
