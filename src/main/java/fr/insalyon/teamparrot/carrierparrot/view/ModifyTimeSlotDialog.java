/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.view;

import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import fr.insalyon.teamparrot.carrierparrot.utils.TimeParser;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import java.time.LocalTime;

/**
 * The Dialog showed when the user modify the time slot of a delivery.
 */
public class ModifyTimeSlotDialog {

    private TimeSlot timeSlot;

    /**
     * @param delivery The delivery whose time slot is modified.
     */
    public ModifyTimeSlotDialog(Delivery delivery) {
        this.timeSlot = null;

        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Modify Time Slot");
        dialog.setHeaderText("Please enter the new time slot for this Delivery");

        ButtonType applyButtonType = new ButtonType("Apply", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(applyButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));


        TextField start = new TextField();
        start.setPromptText("Start");
        TextField end = new TextField();
        end.setPromptText("End");

        if (delivery.getTimeSlotStart() != null && delivery.getTimeSlotEnd() != null) {
            start.setPromptText(delivery.getTimeSlotStart().toString());
            end.setPromptText(delivery.getTimeSlotEnd().toString());
        } else {
            start.setPromptText("Start");
            end.setPromptText("End");
        }

        grid.add(new Label("Start"), 0, 0);
        grid.add(start, 1, 0);
        grid.add(new Label("End"), 0, 1);
        grid.add(end, 1, 1);

        dialog.getDialogPane().setContent(grid);

        dialog.showAndWait();

        LocalTime startTime = TimeParser.getAsLocalTime(start.getText());
        LocalTime endTime = TimeParser.getAsLocalTime(end.getText());
        if (null != startTime && null != endTime) {
            timeSlot = new TimeSlot(startTime, endTime);
        }
    }

    public TimeSlot getTimeSlot() {
        return timeSlot;
    }
}
