/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.view;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.controller.LeftPaneController;
import fr.insalyon.teamparrot.carrierparrot.controller.RoundComponentController;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.RoundHiddenEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundSelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TitledPane;
import java.io.IOException;

/**
 * A RoundComponent is a accordion pane, made for displaying round data in left pane.
 */
public class RoundComponent extends TitledPane {

    private Round round;

    /**
     * Constructor: Load dedicated FXML file, set component title and subscribe to RoundSelected and RoundHidden events.
     * @param parentController In this case, it's the LeftPaneController.
     * @param round The round this component is linked to.
     */
    public RoundComponent(LeftPaneController parentController, Round round) {
        this.round = round;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/roundComponent.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(new RoundComponentController(parentController, round));

        try {
            fxmlLoader.load();

            this.setText("Round - " + round.getDeliveryWarehouse().getDepartureHour());

            EventBus.subscribe(RoundSelectedEvent.class, event -> {
                RoundSelectedEvent rse = (RoundSelectedEvent) event;

                this.getStyleClass().remove("round-selected");

                if (rse.getRound() == round) {
                    CarrierParrotApp.LOGGER.debug("Set round as selected.");
                    this.getStyleClass().add("round-selected");
                }
            });

            EventBus.subscribe(RoundHiddenEvent.class, event -> {
                this.getStyleClass().remove("round-selected");
            });

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * TODO : Is it ok to get the object from the component ?
     * @return The round linked to this component
     */
    public Round getRound() {
        return round;
    }
}
