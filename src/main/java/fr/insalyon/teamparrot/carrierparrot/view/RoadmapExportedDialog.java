/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.view;

import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

/**
 * The Dialog showed when a roapmap has been exported successfully.
 */
public class RoadmapExportedDialog extends Dialog<ButtonType> {

    private String roadmapLocation;

    private final ButtonType copyLocInClipboardBtn = new ButtonType("Copy location", ButtonBar.ButtonData.LEFT);
    private final ButtonType openFileBtn = new ButtonType("Open file", ButtonBar.ButtonData.LEFT);
    private final ButtonType doBothBtn = new ButtonType("Do both", ButtonBar.ButtonData.LEFT);
    private final ButtonType nothingBtn = new ButtonType("Finish", ButtonBar.ButtonData.RIGHT);

    /**
     * Constructor : Set content text and add button on dialog.
     * @param roadmapLocation The roadmap file path.
     */
    public RoadmapExportedDialog(String roadmapLocation) {
        super();

        this.roadmapLocation = roadmapLocation;

        setTitle("Roadmap exported");
        setContentText("Roadmap PDF successfully generated. Want do you want to do?");

        getDialogPane().getButtonTypes().addAll(copyLocInClipboardBtn, openFileBtn, doBothBtn, nothingBtn);

    }

    public void showAndHandle() {
        showAndWait().ifPresent(this::buttonClickHandler);
    }

    /**
     * Handle click on a dialog button.
     * @param response The triggered button.
     */
    private void buttonClickHandler(ButtonType response) {
        if (response == copyLocInClipboardBtn || response == doBothBtn) {
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final ClipboardContent content = new ClipboardContent();
            content.putString(roadmapLocation);
            clipboard.setContent(content);
        }
        if (response == openFileBtn || response == doBothBtn) {
            if (Desktop.isDesktopSupported()) {
                new Thread(() -> {
                    try {
                        File pdfFile = new File(roadmapLocation);
                        Desktop.getDesktop().open(pdfFile);
                    } catch (IOException e) {
                        EventBus.post(new NotificationEvent(
                            "Impossible to open the PDF on your computer.",
                            e.getMessage(),
                            NotificationEvent.NotificationEventType.WARNING
                        ));
                    }
                }).start();
            } else {
                EventBus.post(new NotificationEvent(
                    "Impossible to open the PDF on your computer.",
                    "You should check if you have a default PDF application.",
                    NotificationEvent.NotificationEventType.WARNING
                ));
            }
        }
    }
}
