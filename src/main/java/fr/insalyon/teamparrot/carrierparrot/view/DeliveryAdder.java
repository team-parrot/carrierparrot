/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.view;

import fr.insalyon.teamparrot.carrierparrot.controller.DeliveryAdderController;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TitledPane;
import java.io.IOException;


/**
 * View for the DeliveryAdder component on the right of the window.
 */
public class DeliveryAdder extends TitledPane {

    /**
     * The constructor of the view. Just link the FXML to tis controller.
     *
     * @param round The round where the Delivery is added.
     * @param node  The Node of the new Delivery we want to add to the round.
     */
    public DeliveryAdder(Round round, Node node) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/deliveryAdder.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(new DeliveryAdderController(round, node));

        try {
            fxmlLoader.load();

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
