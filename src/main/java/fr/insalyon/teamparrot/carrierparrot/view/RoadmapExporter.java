/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.view;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * The roadmap exporter convert a round objet to a human-readable set of instruction, stored in a document file (PDF).
 */
public class RoadmapExporter {

    private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD);
    private static Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static Font defaultFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);
    private static String title = "Road Map";
    private String file;
    private ArrayList<Phrase> content;
    private Round round;

    /**
     * TODO : THIS MUST BE REFACTORED.
     * @param fileName The path to roadmap file.
     * @param round The round to export
     */
    public RoadmapExporter(String fileName, Round round) {
        file = fileName;
        this.round = round;
    }

    /**
     * Create the road map document.
     */
    public void createDocument() {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(file));
            content = new ArrayList<>();
            fillContent(round);
            document.open();
            document.addTitle(title);
            document.addCreator("Carrier Parrot");
            addTitle(document);
            addContent(document);
            document.close();
        } catch (Exception e) {
            CarrierParrotApp.LOGGER.error("Failed to export the roadmap.", e);
        }
    }

    /**
     * Add a title to the document.
     * @param document The current PDF document
     * @throws DocumentException If the document is not opened, throws this exception.
     */
    private static void addTitle(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph(title, titleFont));
        preface.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(preface, 3);
        document.add(preface);
    }

    /**
     * Add content to the document.
     * @param document The current PDF document
     * @throws DocumentException If the document is not opened, throws this exception.
     */
    private void addContent(Document document) throws DocumentException {
        Paragraph pageContent = new Paragraph();
        List instructionsList = new List(false, false, 10);
        for (Phrase instruction : this.getContent()) {
            instructionsList.add(new ListItem(instruction));
        }
        pageContent.add(instructionsList);
        document.add(pageContent);
    }

    /**
     * Add an empty line to the document.
     * @param paragraph A paragraph where you want to add empty lines
     * @param number    The number of empty lines to add
     */
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    /**
     * Format the round data to fill document content.
     * @param round The concerned round.
     */
    private void fillContent(Round round) {
        Section lastSection, nextSection;
        String street, nextInstruction;
        String lastInstruction = "Leave the warehouse";
        long distance = 0;
        int i = 1;
        Iterator<Path> it = round.iterator();
        this.addToContent(lastInstruction, boldFont);
        while (it.hasNext()) {
            Path path = it.next();
            Iterator<Section> sectionIt = path.iterator();
            if (sectionIt.hasNext()) {
                lastSection = sectionIt.next();
                while (lastSection == null) {
                    lastSection = sectionIt.next();
                }
                street = lastSection.getStreetName();
                if (street.isEmpty()) {
                    street = "the first road";
                }
                lastInstruction = "Take " + street;
                while (sectionIt.hasNext()) {
                    nextSection = sectionIt.next();
                    street = nextSection.getStreetName();
                    if (round.getMap().getSectionsFromOriginId(lastSection.getOrigin().getId()).size() > 1) {
                        nextInstruction = this.giveInstruction(lastSection, nextSection, street);
                        if (nextInstruction.contains("Follow") && nextInstruction.equals(lastInstruction)) {
                            distance += lastSection.getLength();
                        } else {
                            if (lastInstruction.contains("Follow")) {
                                distance += lastSection.getLength();
                                lastInstruction += " for " + getRoundedDistance(distance) + " meters";
                                distance = 0;
                            }
                            this.addToContent(lastInstruction);
                            lastInstruction = nextInstruction;
                        }
                    }
                    lastSection = nextSection;
                }
                if (it.hasNext()) {
                    lastInstruction = "Make the delivery " + i;
                    this.addToContent(lastInstruction, boldFont);
                    i++;
                }
            }
        }
        this.addToContent(lastInstruction);
        this.addToContent("Return to the warehouse", boldFont);
    }

    /**
     * Find the instruction to give to go from a section to the next.
     * @param lastSection The last section taken by the round
     * @param nextSection The next section taken by the round
     * @param street      The next street taken by the round
     * @return The next instruction for the road map export
     */
    private String giveInstruction(Section lastSection, Section nextSection, String street) {
        int instruction = calculateInstruction(lastSection, nextSection);
        switch (instruction) {
            case 0:
                if (street.isEmpty()) {
                    return ("Follow the road");
                } else {
                    return ("Follow " + street);
                }
            case 1:
                if (street.isEmpty()) {
                    return ("Take a right");
                } else {
                    return ("Take a right on " + street);
                }
            case 2:
                if (street.isEmpty()) {
                    return ("Take a left");
                } else {
                    return ("Take a left on " + street);
                }
            default:
                return "";
        }
    }

    /**
     * Calculate the type of instruction to write based on the angle between the last and the next sections.
     * @param lastSection The last section taken by the round
     * @param nextSection The next section taken by the round
     * @return 0 for "Follow", 1 for "Take a right", 2 for "Take a left"
     */
    private static int calculateInstruction(Section lastSection, Section nextSection) {
        double originOrientation = calculateOrientation(lastSection);
        double destinationOrientation = calculateOrientation(nextSection);
        double angle = destinationOrientation - originOrientation;
        if ((angle > 20 && angle < 180) ||
                (angle < -180 && angle > -340)) {
            return 1;
        }
        if ((angle > 180 && angle < 340) ||
                (angle < -20 && angle > -180)) {
            return 2;
        }
        return 0;
    }

    /**
     * Calculate the orientation of a section.
     * @param vector The section which you want to know the orientation of
     * @return The vectorial orientation of a section
     */
    private static double calculateOrientation(Section vector) {
        double x = vector.getDestination().getX() - vector.getOrigin().getX();
        double y = vector.getDestination().getY() - vector.getOrigin().getY();
        double angle = Math.toDegrees(Math.atan(y / x));
        if (x < 0) {
            angle += 180;
        } else if (y < 0) {
            angle += 360;
        }

        return angle;
    }

    /**
     * Calculate the rounded distance for better instructions.
     * @param number The number to round
     * @return The rounded number
     */
    private static int getRoundedDistance(double number) {
        double rest = number % 10;
        if (rest < 5) {
            return (int) (number - rest);
        }
        return (int) (number + 10 - rest);
    }

    /**
     * Add an instruction to the content variable.
     * @param line The text of the instruction to add.
     */
    private void addToContent(String line) {
        this.content.add(new Phrase(line, defaultFont));
    }

    /**
     * Add an instruction to the content variable.
     * @param line The text of the instruction to add.
     * @param font The style of the instruction to add.
     */
    private void addToContent(String line, Font font) {
        this.content.add(new Phrase(line, font));
    }

    ArrayList<Phrase> getContent() {
        return content;
    }

}
