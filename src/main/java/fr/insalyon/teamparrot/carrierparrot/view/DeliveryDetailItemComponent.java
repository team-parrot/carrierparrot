/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.view;

import fr.insalyon.teamparrot.carrierparrot.event.DeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.ModifyTimeSlotEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RemoveDeliveryEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import java.util.Observable;
import java.util.Observer;

/**
 * A view containing delivery details.
 */
public class DeliveryDetailItemComponent extends VBox implements Observer {

    private Label timeSlotLabel;
    private Label placeLabel;
    private Label durationLabel;

    /**
     * Constructor : adding buttons and label, customized with delivery data.
     * TODO : Create a FXML file to avoid creating elements programmatically.
     * @param delivery The related delivery.
     */
    public DeliveryDetailItemComponent(Delivery delivery) {
        timeSlotLabel = new Label();
        placeLabel = new Label();
        durationLabel = new Label();

        updatePlace(delivery);
        updateDuration(delivery);
        updateTimeSlot(delivery);

        this.setFillWidth(true);
        this.setSpacing(10);
        this.setPadding(new Insets(10, 20, 10, 20));

        Label minArrivalTime = new Label("Time slots ignored");
        if (delivery.getMinArrivalTime() != null) {
            minArrivalTime = new Label("Min arrival time : " + delivery.getMinArrivalTime() + ".");
        }

        Button closeButton = new Button("Close");
        closeButton.setOnMouseClicked((event) -> EventBus.post(new DeliverySelectedEvent(null, null)));
        closeButton.setMaxWidth(Double.MAX_VALUE);

        Button modifyTimeSlotButton = new Button("Modify Time Slot");
        modifyTimeSlotButton.setOnMouseClicked((event) -> EventBus.post(new ModifyTimeSlotEvent(delivery)));
        modifyTimeSlotButton.setMaxWidth(Double.MAX_VALUE);

        Button deleteButton = new Button("Delete");
        deleteButton.setOnMouseClicked((event) -> EventBus.post(new RemoveDeliveryEvent(delivery)));
        deleteButton.getStyleClass().add("delete-button");
        deleteButton.setMaxWidth(Double.MAX_VALUE);

        getChildren().addAll(placeLabel, durationLabel, timeSlotLabel,
                minArrivalTime, modifyTimeSlotButton, closeButton, deleteButton);

        delivery.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof Delivery) {
            updateTimeSlot((Delivery) observable);
        }
    }

    /**
     * Update the delivery time slot display.
     *
     * @param delivery Delivery whose time slot is displayed
     */
    private void updateTimeSlot(Delivery delivery) {
        if (delivery.getTimeSlotEnd() != null) {
            timeSlotLabel.setText(
                    "Between : " + delivery.getTimeSlotStart() + " and " + delivery.getTimeSlotEnd());
        } else {
            timeSlotLabel.setText("No time slot for this delivery");
        }
    }

    /**
     * Update the delivery place display.
     * The street name is displayed if it exists, the node id otherwise.
     *
     * @param delivery Delivery whose place is displayed.
     */
    private void updatePlace(Delivery delivery) {
        String placeText = "" + delivery.getAddress().getNode().getId();
        if (delivery.getAddress().getStreets() != null) {
            placeText = "Place : " + delivery.getAddress().getStreets().get(0);
        }
        placeLabel.setText(placeText);

    }

    private void updateDuration(Delivery delivery) {
        durationLabel.setText("Duration : " + (int) (delivery.getDuration() / 60) + " minutes");
    }
}
