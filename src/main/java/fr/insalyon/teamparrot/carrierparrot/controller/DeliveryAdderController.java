/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.command.AddDeliveryCommand;
import fr.insalyon.teamparrot.carrierparrot.command.CommandHistory;
import fr.insalyon.teamparrot.carrierparrot.event.CloseAddingModeEvent;
import fr.insalyon.teamparrot.carrierparrot.event.DeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.Event;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PlaceFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import fr.insalyon.teamparrot.carrierparrot.utils.TimeParser;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * The controller for adding a new delivery in the current round.
 *
 * Some methods and fields are annotated with @SuppressWarnings("unused") because they are called by the FXML,
 * and IDEA / CheckStyle aren't aware of this.
 */
public class DeliveryAdderController {
    private StateManager stateManager;

    private Delivery deliveryToAdd = null;

    private Node node = null;
    private Round round = null;

    @FXML
    @SuppressWarnings("unused")
    private TextField duration;

    @FXML
    @SuppressWarnings("unused")
    private TextField slotStart;

    @FXML
    @SuppressWarnings("unused")
    private TextField slotEnd;

    /**
     * @param round the round in which we want to add a delivery.
     * @param node  the map node where we want to place the new delivery.
     */
    public DeliveryAdderController(Round round, Node node) {
        stateManager = StateManager.getInstance();

        this.round = round;
        this.node = node;
    }

    @FXML
    public void initialize() {
        CarrierParrotApp.LOGGER.debug("DeliveryAdderController initialized");
        EventBus.subscribe(DeliverySelectedEvent.class, this::addDeliveryBeforeSpecificDelivery);
    }

    /**
     * A new delivery is added to the round with the information of the interface.
     * Triggered by the "Add" button of the component.
     */
    @SuppressWarnings("unused")
    public void addDelivery() {
        String durationString = duration.getCharacters().toString();
        String slotStartString = slotStart.getCharacters().toString();
        String slotEndString = slotEnd.getCharacters().toString();

        Delivery deliveryToAdd = new Delivery(PlaceFactory.get(round.getMap(), this.node.getId()),
                TimeParser.getAsLocalTime(slotStartString),
                Long.parseLong(durationString),
                TimeParser.getAsLocalTime(slotEndString));

        CommandHistory.execute(new AddDeliveryCommand(this.round, deliveryToAdd, -1));
    }

    /**
     * A new delivery is added to the round with the information of the interface, and placed in the round automatically
     * by calling a TSP.
     * Triggered by the "Add" button of the component.
     */
    @SuppressWarnings("unused")
    public void addDeliveryManually() {
        String durationString = duration.getCharacters().toString();
        String slotStartString = slotStart.getCharacters().toString();
        String slotEndString = slotEnd.getCharacters().toString();

        deliveryToAdd = new Delivery(PlaceFactory.get(round.getMap(), this.node.getId()),
                TimeParser.getAsLocalTime(slotStartString),
                Long.parseLong(durationString),
                TimeParser.getAsLocalTime(slotEndString));

        stateManager.toAddingModeMSTate();
        EventBus.post(new NotificationEvent("Add your delivery manually",
                "Please select the delivery you want to be before your new delivery",
                NotificationEvent.NotificationEventType.INFO));
    }

    /**
     * Add a new delivery to the round.
     * @param event : the event which triggered this method.
     */
    private void addDeliveryBeforeSpecificDelivery(Event event) {
        if (State.ADDING_MODE_M == stateManager.getState()) {
            DeliverySelectedEvent deliverySelectedEvent = (DeliverySelectedEvent) event;
            Delivery deliveryLandmark = deliverySelectedEvent.getDelivery();

            CommandHistory.execute(new AddDeliveryCommand(this.round,
                    deliveryToAdd, deliveryLandmark.getDeliveryNumber()));

            this.deliveryToAdd = null;
        }
    }

    /**
     * Just send a CloseAddingEvent.
     * Triggered by the "Cancel" button of the interface.
     */
    @SuppressWarnings("unused")
    public void cancelAddingDelivery() {
        stateManager.toRoundSelectedState();
        deliveryToAdd = null;

        EventBus.post(new CloseAddingModeEvent());
    }

}
