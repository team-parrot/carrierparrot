/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.Config;
import fr.insalyon.teamparrot.carrierparrot.event.DeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.Event;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.RoundHiddenEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundSelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryWarehouse;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.view.DeliveryMoustache;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 * This controller handles the linear synoptic view.
 */
public class SynopticComponentController implements Observer {

    @FXML
    private ScrollPane rootPane;

    @FXML
    private VBox timelinesTitleContainer;

    @FXML
    private Group textsGroup;

    @FXML
    private Group linesGroup;

    @FXML
    private Rectangle synoptic;

    @FXML
    private VBox timelinesContainer;

    private HashMap<Delivery, DeliveryMoustache> moustaches;
    private Round round;

    /**
     * Initialize data used by synoptic components (timeline keys), register handlers for delivery/round selection.
     */
    public void initialize() {
        moustaches = new HashMap<>();

        int startHour = Config.START_OF_DAY.getHour();
        int endHour = Config.END_OF_DAY.getHour();

        // TODO: Instead of using multiply, use margins to have a proper thing.
        DoubleBinding paddedWidth =
                rootPane.widthProperty().subtract(timelinesTitleContainer.widthProperty()).multiply(0.9);

//        rootBox.minHeightProperty().bind(rootPane.heightProperty());

        // TODO: Fix issue with timeline padding, hovering titles.

        timelinesContainer.setSpacing(6);
        timelinesContainer.maxWidthProperty().bind(paddedWidth);
        timelinesContainer.minWidthProperty().bind(paddedWidth);
        timelinesContainer.prefWidthProperty().bind(paddedWidth);

        synoptic.setX(0);
        synoptic.widthProperty().bind(paddedWidth);

        for (int i = startHour; i <= endHour; i++) {
            Text t = new Text();
            t.setTextAlignment(TextAlignment.CENTER);
            t.setFill(Color.WHITE);
            t.setText(LocalTime.of(i, 0).toString());
            t.xProperty().bind(synoptic.widthProperty().multiply(timeToWidth(i * 3600)));

            textsGroup.getChildren().add(t);
        }

        rootPane.setOnMouseClicked(mouseEvent -> EventBus.post(new DeliverySelectedEvent(null, round)));

        EventBus.subscribe(RoundSelectedEvent.class, this::handleRoundSelection);
        EventBus.subscribe(RoundHiddenEvent.class, this::handleRoundHidden);
        EventBus.subscribe(DeliverySelectedEvent.class, this::handleDeliverySelection);

        // TODO: Mustache title is not aligned, with a lot of deliveries there is a huge gap

    }

    /**
     * When round is hidden, we stop observing the current round and set it to null.
     * In the same time, we hide the synoptic view.
     * @param event RoundHiddenEvent
     */
    private void handleRoundHidden(Event event) {
        round = null;
        rootPane.setManaged(false);
        animatePane(1, actionEvent -> {
            rootPane.setVisible(false);
        }, rootPane);
    }

    /**
     * When a round is selected, we need to display the synoptic view and observe the new round.
     * @param event RoundSelectedEvent
     */
    private void handleRoundSelection(Event event) {
        Round r = ((RoundSelectedEvent) event).getRound();

        CarrierParrotApp.LOGGER.debug(r);

        rootPane.setVvalue(1);
        rootPane.setManaged(true);
        rootPane.setVisible(true);
        animatePane(0.75, actionEvent -> {
            generateTimeline();
        }, rootPane);
        round = r;
        round.addObserver(this);
    }

    /**
     * When a delivery is selected, we update the style of the moustaches (a box displayed for a delivery)
     * @param event The delivery selected event.
     */
    private void handleDeliverySelection(Event event) {
        CarrierParrotApp.LOGGER.debug("Handle delivery selection in synoptic view !");

        Delivery d = ((DeliverySelectedEvent) event).getDelivery();
        boolean deliveryExplored = false;

        if (d == null) {
            for (DeliveryMoustache dm : moustaches.values()) {
                dm.classicColorStyle();
            }
        } else {
            for (Path p : round) {
                if (!(p.getFinish() instanceof DeliveryWarehouse)) {
                    if (p.getFinish() == d) {
                        moustaches.get(d).selectedColorStyle();
                        deliveryExplored = true;
                    } else if (deliveryExplored) {
                        moustaches.get(p.getFinish()).nextColorStyle();
                    } else {
                        moustaches.get(p.getFinish()).previousColorStyle();
                    }
                }
            }
        }
    }

    private void generateTimeline() {
        resetTimeline();
        generateBasicLine();
        generateMoustaches();
    }

    private void resetTimeline() {
        moustaches.clear();
        timelinesTitleContainer.getChildren().clear();
        timelinesContainer.getChildren().clear();
        linesGroup.getChildren().clear();
    }

    /**
     * Using round paths, generates the corresponding moustaches and related components.
     */
    private void generateMoustaches() {
        for (Path p: round) {
            if (!(p.getFinish() instanceof DeliveryWarehouse)) {
                DeliveryMoustache moustache = new DeliveryMoustache(synoptic.widthProperty(), p);
                moustache.setMinHeight(15);
                moustache.setOnMouseClicked((event) -> {
                    Text popoverContent = new Text("Departure: "
                            + p.getFinish().getMinArrivalTime().minusSeconds((long) (p.getDuration()))
                            + "\n"
                            + "Arrival : " + p.getFinish().getMinArrivalTime() + "\n"
                            + "End of delivery: "
                            + p.getFinish().getMinArrivalTime().plusSeconds(p.getFinish().getDuration()) + "\n"
                            + "Time slot announced : " + p.getFinish().getTimeSlotStart()
                            + " - " + p.getFinish().getTimeSlotEnd()
                    );

                    HBox hb = new HBox();
                    hb.setPadding(new Insets(5, 5, 5, 5));
                    hb.getChildren().add(popoverContent);

                    PopOver streetPopOver = new PopOver();
                    streetPopOver.setContentNode(hb);
                    streetPopOver.setArrowLocation(PopOver.ArrowLocation.LEFT_CENTER);
                    streetPopOver.setDetachable(false);
                    // Little trick to use baseline instead of timeline.
                    streetPopOver.show(moustache.getChildren().get(1));

                    EventBus.post(new DeliverySelectedEvent(p.getFinish(), round));
                    event.consume();
                });

                moustaches.put(p.getFinish(), moustache);

                timelinesContainer.getChildren().add(moustache);

                Text text = new Text("Delivery n° " + p.getFinish().getDeliveryNumber());
                text.setFill(Color.WHITE);
                text.setTextAlignment(TextAlignment.RIGHT);

                Pane textPane = new Pane();
                textPane.setMinHeight(15);
                textPane.getChildren().add(text);

                timelinesTitleContainer.getChildren().add(textPane);
            }
        }
    }

    /**
     * As said, generate the global timeline, thanks to global width and different paths in round.
     */
    private void generateBasicLine() {

        Rectangle nl;
        Rectangle nl2;

        for (Path p: round) {
            if (!(p.getFinish() instanceof DeliveryWarehouse)) {
                double arrivalInSeconds = timeToSeconds(p.getFinish().getMinArrivalTime());
                double startSeconds = arrivalInSeconds - p.getDuration();
                double endSeconds = arrivalInSeconds + p.getFinish().getDuration();

                nl = new Rectangle();
                nl.xProperty().bind(synoptic.widthProperty().multiply(timeToWidth(startSeconds)));
                nl.widthProperty().bind(synoptic.widthProperty().multiply(timeToWidth(arrivalInSeconds))
                        .subtract(synoptic.widthProperty().multiply(timeToWidth(startSeconds))));
                nl.setFill(Color.ORANGERED);
                nl.setHeight(10);

                nl2 = new Rectangle();
                nl2.xProperty().bind(synoptic.widthProperty().multiply(timeToWidth(arrivalInSeconds)));
                nl2.widthProperty().bind(synoptic.widthProperty().multiply(timeToWidth(endSeconds))
                        .subtract(synoptic.widthProperty().multiply(timeToWidth(arrivalInSeconds))));
                nl2.setFill(Color.GREEN);
                nl2.setHeight(10);

                linesGroup.getChildren().add(nl);
                linesGroup.getChildren().add(nl2);
            }
        }
    }

    /**
     * Animate the pane.
     * @param targetPosition the targeted position where the pane's divider arrive at the end of the animation.
     * @param finishCallback the callback called when the animation finishes.
     * @param root .
     * TODO: Refactor method with Details Pane Controller.
     */
    private void animatePane(double targetPosition, EventHandler<ActionEvent> finishCallback, javafx.scene.Node root) {
        SplitPane splitPane = (SplitPane) root.getParent().getParent();
        SplitPane.Divider splitPaneDivider = splitPane.getDividers().get(0);
        KeyValue end = new KeyValue(splitPaneDivider.positionProperty(), targetPosition);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(.3), end));
        timeline.setOnFinished(finishCallback);
        timeline.play();
    }

    public static double timeToSeconds(LocalTime time) {
        return time.getHour() * 3600 + time.getMinute() * 60.0 + time.getSecond();
    }

    public static double timeToWidth(double timeInSeconds) {
        return (timeInSeconds - timeToSeconds(Config.START_OF_DAY))
                / (timeToSeconds(Config.END_OF_DAY) - timeToSeconds(Config.START_OF_DAY));
    }

    @Override
    public void update(Observable o, Object arg) {
        generateTimeline();
    }
}
