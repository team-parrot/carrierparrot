/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.command.CommandHistory;
import fr.insalyon.teamparrot.carrierparrot.command.ModifyTimeSlotCommand;
import fr.insalyon.teamparrot.carrierparrot.event.CloseAddingModeEvent;
import fr.insalyon.teamparrot.carrierparrot.event.DeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.Event;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.ModifyTimeSlotEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RemoveDeliveryEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundHiddenEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundSelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.SelectNewDeliveryEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryWarehouse;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.view.DeliveryAdder;
import fr.insalyon.teamparrot.carrierparrot.view.DeliveryDetailItemComponent;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

/**
 * The DetailsPaneController manage views and components shown in the right pane.
 */
public class DetailsPaneController {

    private int numberOfAdders = 0;

    private Round round;

    @FXML
    private VBox root;

    @FXML
    private TitledPane detailsPane;

    @FXML
    private VBox deliveryAdder;

    @FXML
    private VBox adderPane;

    /**
     * Classic initializer of a controller, with the Events it subscribes.
     */
    @FXML
    public void initialize() {
        CarrierParrotApp.LOGGER.info("DetailsPaneController initialized");

        EventBus.subscribe(DeliverySelectedEvent.class, this::handleDeliverySelection);
        EventBus.subscribe(RemoveDeliveryEvent.class, ignored -> hideDetailsPane());
        EventBus.subscribe(RoundHiddenEvent.class, ignored -> hideDetailsPane());
        EventBus.subscribe(ModifyTimeSlotEvent.class, this::handleModifyTimeSlot);
        EventBus.subscribe(SelectNewDeliveryEvent.class, this::handleAddingDelivery);
        EventBus.subscribe(CloseAddingModeEvent.class, ignored -> hideDetailsPane());

        EventBus.subscribe(RoundSelectedEvent.class, this::handleRoundSelection);
    }

    /**
     * Handle RoundSelectedEvent to maintain a up-to-date round.
     * @param event Must be a RoundSelectedEvent.
     */
    private void handleRoundSelection(Event event) {
        RoundSelectedEvent roundSelectedEvent = (RoundSelectedEvent) event;
        this.round = roundSelectedEvent.getRound();
    }

    /**
     * Display delivery details when a delivery is selected in the left pane.
     *
     * @param event Must be a DeliverySelectedEvent.
     */
    private void handleDeliverySelection(Event event) {
        DeliverySelectedEvent deliverySelectedEvent = (DeliverySelectedEvent) event;
        Delivery delivery = deliverySelectedEvent.getDelivery();
        if (delivery != null) {
            openDeliveryDetails(delivery);
        } else {
            hideDetailsPane();
        }
    }

    private void handleModifyTimeSlot(Event event) {
        ModifyTimeSlotEvent modifyTimeSlotEvent = (ModifyTimeSlotEvent) event;
        Delivery delivery = modifyTimeSlotEvent.getDelivery();

        CommandHistory.execute(new ModifyTimeSlotCommand(round, delivery));
    }

    /**
     * Show details pane and set delivery details.
     * @param delivery The selected delivery.
     */
    private void openDeliveryDetails(Delivery delivery) {
        String detailsTitle;
        if (delivery instanceof DeliveryWarehouse) {
            detailsTitle = "Warehouse";
        } else {
            detailsTitle = delivery.toString();
        }
        detailsPane.setText(detailsTitle);
        detailsPane.setTextAlignment(TextAlignment.CENTER);
        detailsPane.setContent(new DeliveryDetailItemComponent(delivery));
        root.setVisible(true);
        animatePane(.75, ignored -> root.setManaged(true));
    }

    /**
     * Open the delivery adding interface to get informations on the delivery to add.
     *
     * @param event A selectNewDeliveryEvent.
     */
    private void handleAddingDelivery(Event event) {
        SelectNewDeliveryEvent selectNewDeliveryEvent = (SelectNewDeliveryEvent) event;

        DeliveryAdder da = new DeliveryAdder(
                selectNewDeliveryEvent.getRound(),
                selectNewDeliveryEvent.getNode());
        root.getChildren().add(da);

        root.setVisible(true);
        animatePane(.75, ignored -> root.setManaged(true));

        this.numberOfAdders++;
    }

    /**
     * Hide the details pane.
     */
    private void hideDetailsPane() {
        root.setVisible(false);
        root.setManaged(false);

        if (this.numberOfAdders > 0) {
            root.getChildren().get(this.numberOfAdders).setVisible(false);
            root.getChildren().get(this.numberOfAdders).setManaged(false);
        }
        animatePane(1., null);
    }

    /**
     * Animate the details pane.
     * @param targetPosition the targeted position where the pane's divider arrive at the end of the animation.
     * @param finishCallback the callback called when the animation finishes.
     */
    private void animatePane(double targetPosition, EventHandler<ActionEvent> finishCallback) {
        SplitPane splitPane = (SplitPane) root.getParent().getParent();
        SplitPane.Divider splitPaneDivider = splitPane.getDividers().get(1);
        KeyValue end = new KeyValue(splitPaneDivider.positionProperty(), targetPosition);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(.3), end));
        timeline.setOnFinished(finishCallback);
        timeline.play();
    }
}
