/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.event.Event;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.MapLoadedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundComputedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundHiddenEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundSelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.task.LoadDeliveryRequestTask;
import fr.insalyon.teamparrot.carrierparrot.task.LoadDeliveryRequestWithFileChooserTask;
import fr.insalyon.teamparrot.carrierparrot.task.TaskExecutor;
import fr.insalyon.teamparrot.carrierparrot.view.RoundComponent;
import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import java.io.File;

/**
 * The LeftPaneController manage very important elements (views): the map loader, the delivery request loader, and the
 * round accordion list.
 */
public class LeftPaneController {

    private StateManager stateManager;

    private Map map = null;

    @FXML
    private Accordion accordion;

    @FXML
    private VBox root;

    @FXML
    private Button plusButton;

    @FXML
    private Label plusButtonLabel;

    public LeftPaneController() {
        stateManager = StateManager.getInstance();
    }

    /**
     * A simple controller initializer.
     */
    @FXML
    public void initialize() {
        CarrierParrotApp.LOGGER.info("LeftPaneController initialized");

        EventBus.subscribe(MapLoadedEvent.class, this::handleMapLoad);
        EventBus.subscribe(RoundComputedEvent.class, this::handleRoundComputation);
        accordion.expandedPaneProperty().addListener(this::handleAccordionExpanded);
        root.setOnDragDropped(this::handleDrop);
        root.setOnDragOver(dragEvent -> {
            if (dragEvent.getGestureSource() != root && dragEvent.getDragboard().hasFiles()) {
                dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            dragEvent.consume();
        });
    }

    /**
     * When the map is loaded, we first clear all previous round that might have been opened, then we store the map
     * and unlock new functionnalities.
     * @param event MapLoadedEvent object.
     */
    private void handleMapLoad(Event event) {
        MapLoadedEvent mapLoadedEvent = (MapLoadedEvent) event;
        clearRoundComponents();

        this.map = mapLoadedEvent.getMap();

        plusButton.setDisable(false);
        plusButtonLabel.setVisible(true);
    }

    /**
     * Add the round to the list.
     * Also bind the plus button with to the root width.
     *
     * @param event the currently handled event.
     */
    private void handleRoundComputation(Event event) {
        RoundComputedEvent roundComputedEvent = (RoundComputedEvent) event;
        addNewRound(roundComputedEvent.getRound());
        plusButtonLabel.setVisible(false);
        plusButtonLabel.setManaged(false);

        plusButton.prefWidthProperty().bind(root.widthProperty());
    }

    /**
     * When the accordion is expanded, check if a round is selected and throw the corresponding event.
     *
     * @param observable The observable value, containing the selected round in the accordion.
     */
    private void handleAccordionExpanded(Observable observable) {
        @SuppressWarnings("unchecked") ObservableValue<RoundComponent> ov =
                (ObservableValue<RoundComponent>) observable;
        RoundComponent rc = ov.getValue();

        CarrierParrotApp.LOGGER.debug("Accordion expanded my friend.");

        if (rc != null) {
            EventBus.post(new RoundSelectedEvent(rc.getRound()));
        } else {
            stateManager.toNoRoundSelectedState();
            EventBus.post(new RoundHiddenEvent());
        }
    }

    /**
     * Handle dropped delivery request files.
     *
     * @param dragEvent the drag event.
     */
    private void handleDrop(DragEvent dragEvent) {
        final Dragboard dragboard = dragEvent.getDragboard();
        if (null != dragboard && dragboard.hasFiles()) {
            for (File file : dragboard.getFiles()) {
                TaskExecutor.execute(new LoadDeliveryRequestTask(map, file));
            }
            dragEvent.setDropCompleted(true);
            dragEvent.consume();
        }
    }

    @FXML
    public void loadDeliveryRequest() {
        if (null != this.map) {
            TaskExecutor.execute(new LoadDeliveryRequestWithFileChooserTask(this.map, root.getScene().getWindow()));
        }
    }

    private void addNewRound(Round round) {
        // TODO : Find a clever way to manage parentController
        RoundComponent rc = new RoundComponent(this, round);
        accordion.getPanes().add(rc);
        accordion.setExpandedPane(rc);
    }

    private void clearRoundComponents() {
        if (accordion.getPanes().size() > 0) {
            accordion.getPanes().forEach((TitledPane rc) -> ((RoundComponent) rc).getRound().deleteObservers());
            accordion.getPanes().clear();
            EventBus.post(new RoundHiddenEvent());
        }
    }

    void removeRoundComponent(RoundComponent roundComponent) {
        accordion.getPanes().remove(roundComponent);

        stateManager.toNoRoundSelectedState();

        EventBus.post(new RoundHiddenEvent());
    }
}
