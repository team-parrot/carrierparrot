/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.Config;
import fr.insalyon.teamparrot.carrierparrot.command.CommandHistory;
import fr.insalyon.teamparrot.carrierparrot.command.RemoveDeliveryCommand;
import fr.insalyon.teamparrot.carrierparrot.event.CenterMapRoundEvent;
import fr.insalyon.teamparrot.carrierparrot.event.CloseAddingModeEvent;
import fr.insalyon.teamparrot.carrierparrot.event.DeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.Event;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.MapLoadedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.NextDeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import fr.insalyon.teamparrot.carrierparrot.event.OpenAddingModeEvent;
import fr.insalyon.teamparrot.carrierparrot.event.PreviousDeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RemoveDeliveryEvent;
import fr.insalyon.teamparrot.carrierparrot.event.ResetMapViewEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundHiddenEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundSelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.SelectNewDeliveryEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import fr.insalyon.teamparrot.carrierparrot.task.LoadMapTask;
import fr.insalyon.teamparrot.carrierparrot.task.LoadMapWithFileChooserTask;
import fr.insalyon.teamparrot.carrierparrot.task.TaskExecutor;
import fr.insalyon.teamparrot.carrierparrot.view.DeliveryMarker;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Text;
import org.controlsfx.control.PopOver;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import static fr.insalyon.teamparrot.carrierparrot.Config.MAP_SCALE_FACTOR;

/**
 * The MapController handles the display of the map and the selected round on a graphical view.
 * TODO : Refactor this class (Class Data Abstraction Coupling of 8, min : 7)
 */
public class MapController implements Observer {

    private StateManager stateManager;

    private Map map = null;
    private Round currentRound = null;
    private Circle selectedNodeCircle = null;

    @FXML
    private StackPane root;

    @FXML
    private ScrollPane scrollPane;
    /**
     * StackPane for centering the content, in case the ScrollPane viewport is larger than zoomTarget.
     */
    @FXML
    private StackPane content;
    @FXML
    private Group group;
    @FXML
    private StackPane zoomTarget;
    @FXML
    private StackPane mapRoot;
    @FXML
    private Group groupsContainer;
    @FXML
    private Group mapGroup;
    @FXML
    private Group roundGroup;

    @FXML
    private VBox loadMapBox;

    private HashMap<Section, Line> lineHashMap;
    private HashMap<Delivery, DeliveryMarker> deliveryHashMap;
    private Delivery currentDelivery;

    /**
     * Constructor of the MapController class.
     */
    public MapController() {
        stateManager = StateManager.getInstance();

        this.lineHashMap = new HashMap<>();
        this.deliveryHashMap = new HashMap<>();
    }

    /**
     * Initialize the controller.
     */
    @FXML
    public void initialize() {
        CarrierParrotApp.LOGGER.info("MapController initialized");
        EventBus.subscribe(MapLoadedEvent.class, this::handleMapLoaded);
        EventBus.subscribe(RoundSelectedEvent.class, this::handleRoundSelected);
        EventBus.subscribe(RoundHiddenEvent.class, ignored -> clearRound());
        EventBus.subscribe(DeliverySelectedEvent.class, this::handleDeliverySelected);
        EventBus.subscribe(RemoveDeliveryEvent.class, this::removeDelivery);
        EventBus.subscribe(CenterMapRoundEvent.class, ignored -> centerRound());
        EventBus.subscribe(ResetMapViewEvent.class, ignored -> resetMapView());
        EventBus.subscribe(NextDeliverySelectedEvent.class, ignored -> handleNextDelivery());
        EventBus.subscribe(PreviousDeliverySelectedEvent.class, ignored -> handlePreviousDelivery());
        root.setOnDragDropped(this::handleDrop);
        root.setOnDragOver(dragEvent -> {
            if (dragEvent.getGestureSource() != root && dragEvent.getDragboard().hasFiles()) {
                dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            dragEvent.consume();
        });

        /*
         * TODO: Create MapClosedEvent, and put this in the handler
         * loadMapBox.setVisible(true);
         * loadMapBox.setManaged(true);
         */

        EventBus.subscribe(OpenAddingModeEvent.class, ignored -> handleOpenAddingMode());
        EventBus.subscribe(CloseAddingModeEvent.class, ignored -> handleClosingAddingMode());

        // Drag and zoom
        mapRoot.setOnScroll(this::scrollMap);
        zoomTarget.setOnDragDetected(event -> {
            Node target = (Node) event.getTarget();
            while (target != zoomTarget && target != null) {
                target = target.getParent();
            }
            if (target != null) {
                target.startFullDrag();
            }
        });
        group.layoutBoundsProperty().addListener((observable, oldBounds, newBounds) -> {
            // keep it at least as large as the content
            content.setMinWidth(newBounds.getWidth());
            content.setMinHeight(newBounds.getHeight());
        });
        scrollPane.viewportBoundsProperty().addListener((observable, oldBounds, newBounds) -> {
            // use viewport size, if not too small for zoomTarget
            content.setPrefSize(newBounds.getWidth(), newBounds.getHeight());
        });
    }

    @FXML
    public void loadMap() {
        TaskExecutor.execute(new LoadMapWithFileChooserTask(mapRoot.getScene().getWindow()));
    }

    /**
     * When the map is loaded, store it in controller, display its view and draw its sections.
     * @param event Must be a MapLoadedEvent.
     */
    private void handleMapLoaded(Event event) {
        loadMapBox.setVisible(false);
        loadMapBox.setManaged(false);
        deliveryHashMap = new HashMap<>();
        MapLoadedEvent mapLoadedEvent = (MapLoadedEvent) event;
        this.map = mapLoadedEvent.getMap();
        drawMap(map);
    }

    /**
     * Display the selected round on the map view.
     * @param event Must be a RoundSelectedEvent.
     */
    private void handleRoundSelected(Event event) {
        RoundSelectedEvent roundSelectedEvent = (RoundSelectedEvent) event;
        currentDelivery = null;
        EventBus.post(new DeliverySelectedEvent(null, this.currentRound));
        clearRound();
        currentRound = roundSelectedEvent.getRound();
        drawRound(currentRound);
        centerRound();
        currentRound.addObserver(this);
    }

    /**
     * Highlight a delivery when it's selected.
     *
     * @param event Must be a DeliverySelectedEvent.
     */
    private void handleDeliverySelected(Event event) {
        DeliverySelectedEvent deliverySelectedEvent = (DeliverySelectedEvent) event;
        Delivery delivery = deliverySelectedEvent.getDelivery();

        // Un-highlight previously highlighted delivery
        if (null != currentDelivery && deliveryHashMap.containsKey(currentDelivery)) {
            deliveryHashMap.get(currentDelivery).setHighlighted(false);
            unHighlightRound();
        }

        // Highlight the selected delivery
        currentDelivery = delivery;
        if (null != currentDelivery && deliveryHashMap.containsKey(currentDelivery)) {
            highlightRound();
            deliveryHashMap.get(currentDelivery).setHighlighted(true);
        }
    }

    /**
     * Handle dropped map file.
     *
     * @param dragEvent the drag event.
     */
    private void handleDrop(DragEvent dragEvent) {
        final Dragboard dragboard = dragEvent.getDragboard();
        if (null != dragboard && dragboard.hasFiles()) {
            if (dragboard.getFiles().size() > 1) {
                EventBus.post(new NotificationEvent("Too many files!",
                        "Please drop exactly one map file.",
                        NotificationEvent.NotificationEventType.WARNING));
                return;
            }
            for (File file : dragboard.getFiles()) {
                TaskExecutor.execute(new LoadMapTask(file));
            }
            dragEvent.setDropCompleted(true);
            dragEvent.consume();
        }
    }

    /**
     * Highlights the round with colors :
     * The path leading to the delivery is blue and wider.
     * The paths before are green.
     * The paths after are orange and dashed.
     */
    private void highlightRound() {
        List<Line> currentPathLines = new ArrayList<>();
        List<DeliveryMarker> currentPathMarkers = new ArrayList<>();

        int deliveryNumber = currentDelivery.getDeliveryNumber();
        int i = 1;
        for (Path path : currentRound) {
            if (i == deliveryNumber) {
                currentPathMarkers.add(deliveryHashMap.get(path.getStart()));
                currentPathMarkers.add(deliveryHashMap.get(path.getFinish()));
            }
            for (Section section : path) {
                Line line = lineHashMap.get(section);
                if (i < deliveryNumber) {
                    line.getStyleClass().add("path-before-delivery");
                } else if (i == deliveryNumber) {
                    currentPathLines.add(line);
                } else {
                    line.getStyleClass().add("path-after-delivery");
                }
            }
            i++;
        }

        // Bring current path lines to foreground
        for (Line line : currentPathLines) {
            line.getStyleClass().remove("path-before-delivery");
            line.getStyleClass().remove("path-after-delivery");
            line.getStyleClass().add("delivery-path");
            mapGroup.getChildren().remove(line);
            mapGroup.getChildren().add(line);
        }

        // Bring overlapped markers to foreground
        if (deliveryHashMap.containsKey(currentRound.getPaths().get(0).getStart())) {
            currentPathMarkers.add(deliveryHashMap.get(currentRound.getPaths().get(0).getStart()));
        }
        for (DeliveryMarker marker : currentPathMarkers) {
            marker.moveToForeground();
        }
    }

    /**
     * Sets the paths to normal color/width/undashed.
     */
    private void unHighlightRound() {
        for (Path path : currentRound) {
            for (Section section : path) {
                Line line = lineHashMap.get(section);
                line.getStyleClass().remove("path-before-delivery");
                line.getStyleClass().remove("path-after-delivery");
                line.getStyleClass().remove("delivery-path");
            }
        }
    }

    /**
     * Send a DeliverySelectedEvent with the next delivery in the round if a delivery is already selected,
     * or the first one if the last one (or no one) was selected .
     * Only trigger if a round is selected.
     */
    private void handleNextDelivery() {
        if (null != this.currentRound) {
            Delivery delivery;
            if (null == currentDelivery) {
                delivery = currentRound.getPaths().get(0).getFinish();
            } else {
                int indexNextDelivery;
                if (currentDelivery.getDeliveryNumber() < currentRound.getPaths().size() - 1) {
                    indexNextDelivery = currentDelivery.getDeliveryNumber();
                } else {
                    indexNextDelivery = 0;
                }
                delivery = currentRound.getPaths().get(indexNextDelivery).getFinish();
            }
            EventBus.post(new DeliverySelectedEvent(delivery, currentRound));
        }
    }

    /**
     * Send a DeliverySelectedEvent with the previous delivery in the round if a delivery is already selected,
     * or the last one if the first one (or no one) was selected.
     * Only trigger if a round is selected.
     */
    private void handlePreviousDelivery() {
        if (null != this.currentRound) {
            Delivery delivery;
            if (null == currentDelivery) {
                delivery = currentRound.getPaths().get(currentRound.getPaths().size() - 2).getFinish();
            } else {
                int indexNextDelivery;
                if (currentDelivery.getDeliveryNumber() > 1) {
                    indexNextDelivery = currentDelivery.getDeliveryNumber() - 2;
                } else {
                    indexNextDelivery = currentRound.getPaths().size() - 2;
                }
                delivery = currentRound.getPaths().get(indexNextDelivery).getFinish();
            }
            EventBus.post(new DeliverySelectedEvent(delivery, currentRound));
        }
    }

    /**
     * Scale the map on mouse scroll.
     * <p>
     * Thanks: https://stackoverflow.com/a/38719541/2209243
     *
     * @param event : contains the information about the scroll.
     */
    private void scrollMap(ScrollEvent event) {
        event.consume();

        final double zoomFactor = event.getDeltaY() > 0 ? 1.2 : 1 / 1.2;

        Bounds groupBounds = group.getLayoutBounds();
        final Bounds viewportBounds = scrollPane.getViewportBounds();

        // calculate pixel offsets from [0, 1] range
        double valX = scrollPane.getHvalue() * (groupBounds.getWidth() - viewportBounds.getWidth());
        double valY = scrollPane.getVvalue() * (groupBounds.getHeight() - viewportBounds.getHeight());

        // convert content coordinates to zoomTarget coordinates
        Point2D posInZoomTarget = zoomTarget.parentToLocal(
                group.parentToLocal(new Point2D(event.getX(), event.getY())));

        // calculate adjustment of scroll position (pixels)
        Point2D adjustment = zoomTarget.getLocalToParentTransform().deltaTransform(
                posInZoomTarget.multiply(zoomFactor - 1));

        // do the resizing
        zoomTarget.setScaleX(zoomFactor * zoomTarget.getScaleX());
        zoomTarget.setScaleY(zoomFactor * zoomTarget.getScaleY());

        // refresh ScrollPane scroll positions & content bounds
        scrollPane.layout();

        // convert back to [0, 1] range
        // (too large/small values are automatically corrected by ScrollPane)
        groupBounds = group.getLayoutBounds();
        scrollPane.setHvalue((valX + adjustment.getX()) / (groupBounds.getWidth() - viewportBounds.getWidth()));
        scrollPane.setVvalue((valY + adjustment.getY()) / (groupBounds.getHeight() - viewportBounds.getHeight()));
    }

    /**
     * For a given map, construct sections and handle UX events (resize, drag, scroll, click on section, etc...).
     * @param map The map to display in the graphical view.
     */
    private void drawMap(Map map) {
        mapGroup.getChildren().clear();
        lineHashMap.clear();

        List<Node> newNodes = new ArrayList<>();
        // Add the ability to select nodes on the map
        mapGroup.setOnMouseClicked(this::clickedToSelectNode);

        Iterator<Section> sectionIterator = map.sectionsIterator();
        while (sectionIterator.hasNext()) {
            Section section = sectionIterator.next();
            Line line = new Line(
                    section.getOrigin().getX() / MAP_SCALE_FACTOR,
                    section.getOrigin().getY() / MAP_SCALE_FACTOR,
                    section.getDestination().getX() / MAP_SCALE_FACTOR,
                    section.getDestination().getY() / MAP_SCALE_FACTOR
            );
            line.setOnMouseEntered((event) -> line.getStyleClass().add("highlighted-section"));
            line.setOnMouseExited((event) -> line.getStyleClass().remove("highlighted-section"));
            line.setOnMouseClicked((event) -> {
                PopOver streetPopOver = new PopOver();
                Text streetName = new Text(section.getStreetName());
                HBox popOverHbox = new HBox(2.0);
                popOverHbox.setAlignment(Pos.CENTER);
                popOverHbox.setPadding(new Insets(0, 5, 0, 5));
                popOverHbox.getChildren().add(streetName);
                streetPopOver.setContentNode(popOverHbox);
                streetPopOver.setArrowLocation(PopOver.ArrowLocation.BOTTOM_CENTER);
                streetPopOver.setDetachable(false);
                streetPopOver.show(line);
            });
            line.setStroke(Color.WHITE);
            line.setStrokeLineCap(StrokeLineCap.ROUND);

            if (lineHashMap.put(section, line) == null) {
                newNodes.add(line);
            }
        }
        mapGroup.getChildren().addAll(newNodes);

        // Add a background to the map, to make the group clickable
        Bounds mapBounds = mapGroup.getBoundsInLocal();
        Rectangle background = new Rectangle(
                mapBounds.getMinX(), mapBounds.getMinY(),
                mapBounds.getWidth(), mapBounds.getHeight()
        );
        background.setFill(Config.MAP_BACKGROUND_COLOR);
        mapGroup.getChildren().add(0, background);
    }

    /**
     * Center the view around the printed round.
     * TODO: DELETE IF NOT DOABLE IN TIME
     */
    private void centerRound() {
//        Bounds roundGroupBounds = roundGroup.getBoundsInLocal();
//        Bounds scrollPaneBounds = scrollPane.getViewportBounds();
//
//        double scale = Math.min(
//                scrollPaneBounds.getWidth() / roundGroupBounds.getWidth() * 0.9,
//                scrollPaneBounds.getHeight() / roundGroupBounds.getHeight() * 0.9
//        );
//        zoomTarget.setScaleX(scale);
//        zoomTarget.setScaleY(scale);
//
//        double vValue = (mapRoot.getBoundsInParent().getMinY() -
//                roundGroup.getBoundsInParent().getMinY()) + roundGroup.getBoundsInParent().getHeight() / 2;
//        vValue /= scrollPaneBounds.getHeight();
//        double hValue = (mapRoot.getBoundsInParent().getMinY() -
//                roundGroup.getBoundsInParent().getMinX()) + roundGroup.getBoundsInParent().getWidth() / 2;
//        hValue /= scrollPaneBounds.getWidth();
//        scrollPane.setVvalue(vValue);
//        scrollPane.setHvalue(hValue);
//
//        CarrierParrotApp.LOGGER.debug("vValue = " + vValue);
//        CarrierParrotApp.LOGGER.debug("hValue = " + hValue);
//
//        Bounds rgBounds = roundGroup.getBoundsInParent();
//        Bounds mrBounds = mapRoot.getBoundsInParent();
//        CarrierParrotApp.LOGGER.debug("RG: " + rgBounds.getWidth() + "x" + rgBounds.getHeight());
//        CarrierParrotApp.LOGGER.debug("MR: " + mrBounds.getWidth() + "x" + mrBounds.getHeight());

//        scrollPane.layout();
    }

    /**
     * Center and change the zoom of the map, to make it fully visible in the window.
     */
    private void resetMapView() {
        Bounds mapBounds = mapRoot.getBoundsInLocal();
        Bounds scrollPaneBounds = scrollPane.getViewportBounds();

        double scale = Math.min(
                scrollPaneBounds.getWidth() / mapBounds.getWidth(),
                scrollPaneBounds.getHeight() / mapBounds.getHeight()
        );
        zoomTarget.setScaleX(scale);
        zoomTarget.setScaleY(scale);

        scrollPane.setHvalue(.5);
        scrollPane.setVvalue(.5);
        scrollPane.layout();
    }

    /**
     * Used when a round is loaded or selected by the user in the accordion. Change the round sections colors and add
     * markers to display nodes (which are Delivery points)
     * @param round The round to display.
     */
    @FXML
    private void drawRound(Round round) {
        clearRound();
        currentRound = round;
        for (Path path : round) {
            Delivery delivery = path.getStart();
            fr.insalyon.teamparrot.carrierparrot.model.map.Node start = delivery.getAddress().getNode();
            for (Section section : path) {
                Line line = lineHashMap.get(section);
                line.getStyleClass().add("path");
                roundGroup.getChildren().remove(line);
                roundGroup.getChildren().add(line);
            }

            DeliveryMarker marker = new DeliveryMarker(
                    start.getX() / MAP_SCALE_FACTOR,
                    start.getY() / MAP_SCALE_FACTOR,
                    delivery
            );
            deliveryHashMap.put(path.getStart(), marker);
            roundGroup.getChildren().add(marker);
        }
        //Little hack to bring the warehouse in the foreground in front of the path
        deliveryHashMap.get(currentRound.getPaths().get(0).getStart()).moveToForeground();
    }

    /**
     * Clean round display on the map and remove stored round.
     */
    @FXML
    private void clearRound() {
        roundGroup.getChildren().clear();
        deliveryHashMap.clear();
        if (null == currentRound) {
            return;
        }

        for (Iterator<Section> it = map.sectionsIterator(); it.hasNext(); ) {
            Section section = it.next();
            Line line = lineHashMap.get(section);
            line.getStyleClass().remove("path");
            line.getStyleClass().remove("path-before-delivery");
            line.getStyleClass().remove("path-after-delivery");
            line.getStyleClass().remove("delivery-path");
        }

        currentRound = null;
    }

    /**
     * Delete delivery from its round.
     * @param event Must be a RemoveDeliveryEvent.
     */
    private void removeDelivery(Event event) {
        RemoveDeliveryEvent removeDeliveryEvent = (RemoveDeliveryEvent) event;
        Delivery delivery = removeDeliveryEvent.getDelivery();
        CarrierParrotApp.LOGGER.debug("removing delivery " + delivery.getDeliveryNumber());
        CommandHistory.execute(new RemoveDeliveryCommand(this.currentRound, delivery));
    }

    /**
     * Open the adding mode if there is a map and the round selected.
     */
    private void handleOpenAddingMode() {
        if (null != this.map && null != this.currentRound) {
            stateManager.toAddingMode1State();
            EventBus.post(new NotificationEvent("Welcome in adding mode",
                    "Please use right click on a new place to deliver",
                    NotificationEvent.NotificationEventType.INFO));
            // TODO: @totorigolo, implement a way to change the notification type of events themselves
        }
    }

    private void handleClosingAddingMode() {
        stateManager.toRoundSelectedState();
        if (null != this.selectedNodeCircle) {
            mapGroup.getChildren().remove(this.selectedNodeCircle);
            this.selectedNodeCircle = null;
        }
    }

    /**
     * Send a SelectNewDeliveryEvent with the current round and the closest node of the right click in the map
     * if in adding mode.
     *
     * @param event A right click on the map.
     */
    private void clickedToSelectNode(MouseEvent event) {

        if (event.getButton() == MouseButton.SECONDARY && State.ADDING_MODE_1 == stateManager.getState()) {
            long x = (long) (event.getX() * MAP_SCALE_FACTOR);
            long y = (long) (event.getY() * MAP_SCALE_FACTOR);

            fr.insalyon.teamparrot.carrierparrot.model.map.Node node = map.getNearestNode(x, y);

            if (null != this.selectedNodeCircle) {
                mapGroup.getChildren().remove(this.selectedNodeCircle);
            }
            Circle circle = new Circle(node.getX() / MAP_SCALE_FACTOR,
                    node.getY() / MAP_SCALE_FACTOR,
                    5);
            circle.getStyleClass().add("selected-node-adding-mode");

            mapGroup.getChildren().add(circle);
            this.selectedNodeCircle = circle;

            stateManager.toAddingMode2STate();

            EventBus.post(new SelectNewDeliveryEvent(node, this.currentRound));
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Round && null != currentRound) {
            drawRound(currentRound);
        }
    }
}
