/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.command.CommandHistory;
import fr.insalyon.teamparrot.carrierparrot.event.CenterMapRoundEvent;
import fr.insalyon.teamparrot.carrierparrot.event.Event;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.MapLoadedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.ResetMapViewEvent;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.task.LoadDeliveryRequestWithFileChooserTask;
import fr.insalyon.teamparrot.carrierparrot.task.LoadMapWithFileChooserTask;
import fr.insalyon.teamparrot.carrierparrot.task.TaskExecutor;
import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * The menu controller handles actions linked to user experience: load a map, a delivery request, undo/redo, etc.
 */
public class MenuController {

    private Map map = null;

    @FXML
    private MenuBar menuBar;

    @FXML
    private MenuItem loadDeliveryRequestButton;

    @FXML
    public void initialize() {
        CarrierParrotApp.LOGGER.info("MenuController initialized");

        EventBus.subscribe(MapLoadedEvent.class, this::handleMapLoaded);
    }

    @FXML
    public void undoMenuAction() {
        CommandHistory.undo();
    }

    @FXML
    public void redoMenuAction() {
        CommandHistory.redo();
    }

    @FXML
    public void loadMap() {
        TaskExecutor.execute(new LoadMapWithFileChooserTask(menuBar.getScene().getWindow()));
    }

    @FXML
    public void loadDeliveryRequest() {
        if (null != this.map) {
            TaskExecutor.execute(new LoadDeliveryRequestWithFileChooserTask(this.map, menuBar.getScene().getWindow()));
        }
    }

    @FXML
    public void centerMap() {
        EventBus.post(new CenterMapRoundEvent());
    }

    @FXML
    public void resetMapView() {
        EventBus.post(new ResetMapViewEvent());
    }

    @FXML
    private void exitMenuAction() {
        System.exit(0);
    }

    private void handleMapLoaded(Event event) {
        MapLoadedEvent mapLoadedEvent = (MapLoadedEvent) event;
        this.map = mapLoadedEvent.getMap();

        loadDeliveryRequestButton.setDisable(false);
    }


}
