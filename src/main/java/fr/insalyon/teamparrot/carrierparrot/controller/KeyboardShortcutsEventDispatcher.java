/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.command.CommandHistory;
import fr.insalyon.teamparrot.carrierparrot.event.CenterMapRoundEvent;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NextDeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.OpenAddingModeEvent;
import fr.insalyon.teamparrot.carrierparrot.event.PreviousDeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.ResetMapViewEvent;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import javafx.event.EventDispatcher;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Check every events in the application and deals with the key ones.
 */
public class KeyboardShortcutsEventDispatcher implements EventDispatcher {

    @Override
    public Event dispatchEvent(Event event, EventDispatchChain tail) {
        /*
         * Heads-up! Do not mix up @javafx.event.Event and @fr.insalyon.teamparrot.carrierparrot.controller.Event.
         */

        // Dispatch the event to the rest of the dispatch chain
        event = tail.dispatchEvent(event);

        // If the event is not null (nor has been nullified), we handle it
        if (null != event) {
            // Pass every keyboard event which didn't happen in a text field
            if (event instanceof KeyEvent && !(event.getTarget() instanceof TextInputControl)) {
                KeyEvent keyEvent = (KeyEvent) event;
                if (keyEvent.getEventType() == KeyEvent.KEY_RELEASED) {
                    if (shortcutsHandler(keyEvent)) {
                        // Consume the event if it's been handled
                        event.consume();
                    }
                }
            }
        }

        return event;
    }

    /**
     * Check the key pushed and post a specified event if correspond to a shortcut.
     *
     * @param keyEvent The key event.
     * @return True if the keycode is one of the shortcuts
     */
    private boolean shortcutsHandler(KeyEvent keyEvent) {

        final KeyCode keyCode = keyEvent.getCode();

        // Shortcuts with Ctrl and Shift modifier key
        return (keyEvent.isControlDown() && keyEvent.isShiftDown() && ctrlShiftShortcutsHandler(keyCode))
                || (keyEvent.isControlDown() && ctrlShortcutsHandler(keyCode))
                || noModifierShortcutsHandler(keyCode);
    }

    /**
     * Handle shortcuts with no modifier key.
     *
     * @param keyCode The code of the key pushed on the keyboard.
     * @return whether the @keyCode is one of the shortcuts.
     */
    private boolean noModifierShortcutsHandler(KeyCode keyCode) {
        switch (keyCode) {
            case A:
                EventBus.post(new OpenAddingModeEvent());
                return true;

            case C:
                EventBus.post(new CenterMapRoundEvent());
                return true;

            case N:
                EventBus.post(new NextDeliverySelectedEvent());
                return true;

            case P:
                EventBus.post(new PreviousDeliverySelectedEvent());
                return true;

            case R:
                EventBus.post(new ResetMapViewEvent());
                return true;
        }
        return false;
    }

    /**
     * Handle shortcuts with no modifier key.
     *
     * @param keyCode The code of the key pushed on the keyboard.
     * @return whether the @keyCode is one of the shortcuts.
     */
    private boolean ctrlShortcutsHandler(KeyCode keyCode) {
        switch (keyCode) {
            case Z:
                CommandHistory.undo();
                return true;
        }
        return false;
    }

    /**
     * Handle shortcuts with no modifier key.
     *
     * @param keyCode The code of the key pushed on the keyboard.
     * @return whether the @keyCode is one of the shortcuts.
     */
    private boolean ctrlShiftShortcutsHandler(KeyCode keyCode) {
        switch (keyCode) {
            case Z:
                CommandHistory.redo();
                return true;
        }
        return false;
    }
}
