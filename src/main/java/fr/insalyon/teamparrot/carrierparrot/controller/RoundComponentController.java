/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.event.DeliverySelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.OpenAddingModeEvent;
import fr.insalyon.teamparrot.carrierparrot.event.RoundSelectedEvent;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.task.GenerateRoadmapTask;
import fr.insalyon.teamparrot.carrierparrot.task.TaskExecutor;
import fr.insalyon.teamparrot.carrierparrot.view.DeliveryMarker;
import fr.insalyon.teamparrot.carrierparrot.view.RoundComponent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

/**
 * Handles the behavior of RoundComponent in window.
 */
public class RoundComponentController implements Observer {

    private Round round;

    private LeftPaneController parentController;
    private HashMap<Delivery, DeliveryMarker> deliveryMarkerHashMap;

    @FXML
    private TitledPane roundContainer;

    @FXML
    private VBox roundList;

    public RoundComponentController(LeftPaneController parentController, Round round) {
        deliveryMarkerHashMap = new HashMap<>();
        this.parentController = parentController;
        this.round = round;
    }

    /**
     * initializes the RoundComponentController.
     */
    @FXML
    @SuppressWarnings("unused")
    public void initialize() {
        CarrierParrotApp.LOGGER.info("RoundComponentController initialized");
        EventBus.subscribe(RoundSelectedEvent.class, event -> {
            RoundSelectedEvent roundSelectedEvent = (RoundSelectedEvent) event;
            Round round = roundSelectedEvent.getRound();
            this.round = round;
            this.fillRoundPanel();
        });
        EventBus.subscribe(DeliverySelectedEvent.class, event -> {
            DeliverySelectedEvent deliverySelectedEvent = (DeliverySelectedEvent) event;
            Delivery delivery = deliverySelectedEvent.getDelivery();
            for (DeliveryMarker marker : deliveryMarkerHashMap.values()) {
                marker.setHighlighted(false);
            }
            if (null != delivery) {
                deliveryMarkerHashMap.get(delivery).setHighlighted(true);
                // TODO: NullPointerException here
            }
        });
        fillRoundPanel();
        round.addObserver(this);
    }

    /**
     * TODO : Refactor : isolate view controllers (required by javafx) and application controllers.
     */
    private void fillRoundPanel() {
        roundList.getChildren().clear();
        HBox hBoxWarehouseDeparture = new HBox(5);
        hBoxWarehouseDeparture.setAlignment(Pos.CENTER_LEFT);
        hBoxWarehouseDeparture.setPadding(new Insets(10, 0, 10, 5));
        DeliveryMarker deliveryMarker = new DeliveryMarker(0, 0, round.getDeliveryWarehouse());
        deliveryMarkerHashMap.put(round.getDeliveryWarehouse(), deliveryMarker);
        hBoxWarehouseDeparture.getChildren().add(deliveryMarker);
        hBoxWarehouseDeparture.getChildren().add(new Label("Warehouse"));

        roundList.getChildren().clear();
        roundList.getChildren().add(hBoxWarehouseDeparture);

        Iterator<Path> pathIterator = round.iterator();
        int i = 0;
        while (pathIterator.hasNext()) {
            i++;
            Path path = pathIterator.next();
            HBox hBoxPath = new HBox(2);
            hBoxPath.setPadding(new Insets(0, 0, 0, 20));
            hBoxPath.getChildren().add(
                    new Label("Path n°" + i + " : " + formatDuration(path.getDuration()))
            );
            roundList.getChildren().add(hBoxPath);

            HBox hBoxDelivery = new HBox(5);
            hBoxDelivery.setPadding(new Insets(10, 0, 10, 5));
            hBoxDelivery.setAlignment(Pos.CENTER_LEFT);
            String deliveryOrWarehouse;
            if (pathIterator.hasNext()) {
                deliveryOrWarehouse = "Delivery n°" + i;
                Button deliveryButton = new Button(deliveryOrWarehouse);
                deliveryButton.setOnMouseClicked((event) -> {
                    EventBus.post(new DeliverySelectedEvent(path.getFinish(), this.round));
                });
                deliveryButton.setMaxWidth(Double.MAX_VALUE);
                HBox.setHgrow(deliveryButton, Priority.ALWAYS);
                deliveryMarker = new DeliveryMarker(0, 0, path.getFinish());
                deliveryMarkerHashMap.put(path.getFinish(), deliveryMarker);
                hBoxDelivery.getChildren().add(deliveryMarker);
                hBoxDelivery.getChildren().add(deliveryButton);
            } else {
                deliveryOrWarehouse = "Warehouse";
                Label warehouseLabel = new Label(deliveryOrWarehouse);
                deliveryMarker = new DeliveryMarker(0, 0, path.getFinish());
                deliveryMarkerHashMap.put(path.getFinish(), deliveryMarker);
                hBoxDelivery.getChildren().add(deliveryMarker);
                hBoxDelivery.getChildren().add(warehouseLabel);
            }
            roundList.getChildren().add(hBoxDelivery);
        }
    }

    /**
     * Format duration in seconds to a human-readable value.
     * @param duration The duration in seconds
     * @return The duration in a human-readable value
     */
    private String formatDuration(double duration) {
        String formattedDuration = "";

        if (duration >= 3600) {
            formattedDuration += String.valueOf((int) (duration / 3600)) + "heure(s) ";
            duration = duration % 3600;
        }

        if (duration > 60) {
            formattedDuration += String.valueOf((int) (duration / 60)) + " minute(s)";
        }

        return (formattedDuration);
    }


    @FXML
    @SuppressWarnings("unused")
    public void closeRound() {
        round.deleteObservers();
        parentController.removeRoundComponent((RoundComponent) roundContainer);
    }

    @FXML
    @SuppressWarnings("unused")
    public void generateRoadmap() {
        TaskExecutor.execute(new GenerateRoadmapTask(this.round, roundContainer.getScene().getWindow()));
    }

    /**
     * Post an OpenAddingModeEvent when "add" button is clicked, and print a notification for the user.
     */
    @FXML
    @SuppressWarnings("unused")
    public void openAddingMode() {
        EventBus.post(new OpenAddingModeEvent());

    }

    @Override
    public void update(Observable o, Object arg) {
        fillRoundPanel();
    }
}
