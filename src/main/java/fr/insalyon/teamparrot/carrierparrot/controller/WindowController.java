/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

import fr.insalyon.teamparrot.carrierparrot.CarrierParrotApp;
import fr.insalyon.teamparrot.carrierparrot.event.Event;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.event.NotificationEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import org.controlsfx.control.Notifications;

/**
 * Handle actions directly linked with application window.
 */
public class WindowController {

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Label progressLabel;

    /**
     * Register controller to Notification events AND every event. The first one is made to display
     * important information, while the other one will just get every event as a String thanks to getNotificationAsText
     */

    public WindowController() {
    }

    /**
     * Simple initializer of the window, with subscriptions to events for explanation texts for the user.
     */
    @FXML
    public void initialize() {
        CarrierParrotApp.LOGGER.info("WindowController initialized");

        progressBar.setVisible(false);
        progressBar.setManaged(false);

        EventBus.subscribe(Event.class, event -> progressLabel.setText(event.getNotificationAsText()));
        EventBus.subscribe(NotificationEvent.class, this::displayNotification);
    }

    /**
     * Display notification event to the user. Different notification types are supported: INFO, WARNING, ERROR
     * @param event The concerned event.
     */
    private void displayNotification(Event event) {
        NotificationEvent notificationEvent = (NotificationEvent) event;
        Notifications notif = Notifications.create()
                .title(notificationEvent.getTitle())
                .text(notificationEvent.getMessage());
        switch (notificationEvent.getType()) {
            case INFO:
                notif.showInformation();
                break;
            case WARNING:
                notif.showWarning();
                break;
            case ERROR:
                notif.showError();
                break;
        }
    }
}
