/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.controller;

/**
 * Keeps track of the state of the application. It is use mainly for the adding mode.
 */
public class StateManager {

    private static StateManager instance = null;

    public static StateManager getInstance() {
        if (null == instance) {
            instance = new StateManager();
        }
        return instance;
    }

    private State state;

    private StateManager() {
        this.state = State.JUST_STARTED;
    }

    /**
     * Change state to NO_ROUND_SELECTED state.
     *
     * @return Boolean which tell if the state change happened.
     */
    public boolean toNoRoundSelectedState() {
        if (State.NO_ROUND_SELECTED != state) {
            state = State.NO_ROUND_SELECTED;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Change state to ROUND_SELECTED state.
     *
     * @return Boolean which tell if the state change happened.
     */
    public boolean toRoundSelectedState() {
        if (!(State.ROUND_SELECTED == state || State.JUST_STARTED == state || State.ADDING_MODE_1 == state)) {
            state = State.ROUND_SELECTED;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Change state to ADDING_MODE_1 state.
     *
     * @return Boolean which tell if the state change happened.
     */
    public boolean toAddingMode1State() {
        if (State.ROUND_SELECTED == state) {
            state = State.ADDING_MODE_1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Change state to ADDING_MODE_2 state.
     *
     * @return Boolean which tell if the state change happened.
     */
    public boolean toAddingMode2STate() {
        if (State.ADDING_MODE_1 == state) {
            state = State.ADDING_MODE_2;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Change state to ADDING_MODE_M state.
     *
     * @return Boolean which tell if the state change happened.
     */
    public boolean toAddingModeMSTate() {
        if (State.ADDING_MODE_2 == state) {
            state = State.ADDING_MODE_M;
            return true;
        } else {
            return false;
        }
    }

    public State getState() {
        return state;
    }
}
