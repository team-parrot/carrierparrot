/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot;

import fr.insalyon.teamparrot.carrierparrot.controller.KeyboardShortcutsEventDispatcher;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * CarrierParrot is a round-trip computing tool.
 *
 * @author Team Parrot, H4304, INSA Lyon
 */
public class CarrierParrotApp extends Application {

    /**
     * This LOg4j logger is used in the whole app. To change scope, change config in resources/log4j2.xml
     */
    public static final Logger LOGGER = LogManager.getLogger("CarrierParrot");

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/window.fxml"));
        primaryStage.setTitle("CarrierParrot");
        primaryStage.setScene(new Scene(root, 1000, 600));
        primaryStage.setMinWidth(600);
        primaryStage.setMinHeight(400);
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images/parrot.png")));
        primaryStage.getScene().setEventDispatcher(new KeyboardShortcutsEventDispatcher());

        primaryStage.show();
    }

    /**
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
