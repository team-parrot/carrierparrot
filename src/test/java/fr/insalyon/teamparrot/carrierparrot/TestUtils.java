/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot;

import fr.insalyon.teamparrot.carrierparrot.command.CommandHistory;
import fr.insalyon.teamparrot.carrierparrot.controller.StateManager;
import fr.insalyon.teamparrot.carrierparrot.error.DeliveryRequestFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.event.EventBus;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PlaceFactory;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.parser.XMLMapParser;
import fr.insalyon.teamparrot.carrierparrot.task.TaskExecutor;
import javafx.embed.swing.JFXPanel;
import java.lang.reflect.Field;

public class TestUtils {

    public static String r(String fileName) {
        return TestUtils.class.getResource(fileName).getPath();
    }

    public static void initializeJavaFxApplication() {
        new JFXPanel();
    }

    private static void resetSingleton(Class<?> singletonClass) throws Exception {
        Field instance = singletonClass.getDeclaredField("instance");
        instance.setAccessible(true);
        instance.set(null, null);
    }

    public static void resetSingletons() throws Exception {
        resetSingleton(CommandHistory.class);
//        resetSingleton(EventBus.class);
        resetSingleton(PlaceFactory.class);
        resetSingleton(StateManager.class);
        resetSingleton(TaskExecutor.class);
    }

    public static Map map(String fileName) throws MapFailedToLoadException {
        return XMLMapParser.parse(r(fileName));
    }

    public static DeliveryRequest deliveryRequest(Map map, String deliveryRequestFileName)
            throws DeliveryRequestFailedToLoadException, MapFailedToLoadException {
        return XMLDeliveryRequestParser.parse(r(deliveryRequestFileName), map);
    }
}
