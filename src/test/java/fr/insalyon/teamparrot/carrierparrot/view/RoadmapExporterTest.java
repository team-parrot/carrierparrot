package fr.insalyon.teamparrot.carrierparrot.view;

import com.itextpdf.text.Phrase;
import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.RoundFactory;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;
import static org.junit.Assert.assertEquals;

public class RoadmapExporterTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void instructions() throws Exception {
        final Map map = map("/planLyonGrand.xml");
        try {
            Round round = RoundFactory.get(map, XMLDeliveryRequestParser.parse(r("/DLpetit3.xml"), map));
            RoadmapExporter rme = new RoadmapExporter("/tmp/roadmap.pdf", round);
            rme.createDocument();
            ArrayList<Phrase> phrases = rme.getContent();
            ArrayList<String> instructions = new ArrayList<>();
            for (Phrase p : phrases) {
                instructions.add(p.getContent());
            }
            ArrayList<String> validInstructions = new ArrayList<>();
            validInstructions.add("Leave the warehouse");
            validInstructions.add("Take Rue Trarieux");
            validInstructions.add("Take a left on Rue du Professeur Florence");
            validInstructions.add("Follow Cours Albert Thomas for 560 meters");
            validInstructions.add("Make the delivery 1");
            validInstructions.add("Take Avenue des Frères Lumière");
            validInstructions.add("Follow Avenue des Frères Lumière for 570 meters");
            validInstructions.add("Follow Avenue Rockefeller for 420 meters");
            validInstructions.add("Take a left on Rue Viala");
            validInstructions.add("Make the delivery 2");
            validInstructions.add("Take Rue Trarieux");
            validInstructions.add("Follow Rue Trarieux");
            validInstructions.add("Return to the warehouse");

            for (int i = 0; i < validInstructions.size(); ++i) {
                assertEquals(validInstructions.get(i), instructions.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}