/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory;

import fr.insalyon.teamparrot.carrierparrot.Config;
import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp.TSP;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp.TSPNaiveImpl;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp.TSPSimpleBoundImpl;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.tsp.TSPSimpleBoundIncreasingIteratorImpl;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.parser.XMLMapParser;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;

public class TSPTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    private Round computeRound(final String mapPath, final String deliveryRequestPath, TSP tsp) throws Exception {
        Map smallMap = XMLMapParser.parse(mapPath);
        DeliveryRequest deliveryRequest = XMLDeliveryRequestParser.parse(deliveryRequestPath, smallMap);
        return RoundFactory.getWithCustomTSP(smallMap, deliveryRequest, tsp);
    }

    private void testTSP(TSP tsp, String mapPath, String deliveryRequestPath, long solutionPrice)
            throws Exception {
        System.out.println("Testing: \"" + deliveryRequestPath + "\"");
        computeRound(r(mapPath), r(deliveryRequestPath), tsp);
        assertEquals(solutionPrice, tsp.getBestSolutionPrice());
        assertFalse(tsp.getReachedLimitTime());
    }

    private void validityTest(Class<?> tsp) throws Exception {
        testTSP((TSP) tsp.newInstance(), "/planLyonPetit.xml", "/DLpetit3.xml",
                2258
        );
        testTSP((TSP) tsp.newInstance(), "/planLyonMoyen.xml", "/DLmoyen5.xml",
                4737
        );
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand11.xml",
                18444
        );
    }

    private void validityTWTest(Class<?> tsp) throws Exception {
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand10TW2.xml",
                19281
        );
        testTSP((TSP) tsp.newInstance(), "/planLyonMoyen.xml", "/DLmoyen5TW4.xml",
                12719
        );
    }

    @Test
    public void naiveImplValidityTest() throws Exception {
        validityTest(TSPNaiveImpl.class);
    }

    @Test
    public void naiveImplValidityTWTest() throws Exception {
        validityTWTest(TSPNaiveImpl.class);
    }

    @Test
    public void simpleBoundImplValidityTest() throws Exception {
        validityTest(TSPSimpleBoundImpl.class);
    }

    @Test
    public void simpleBoundImplValidityTWTest() throws Exception {
        validityTWTest(TSPSimpleBoundImpl.class);
    }

    @Test
    public void simpleBoundIncreasingIteratorImplValidityTest() throws Exception {
        validityTest(TSPSimpleBoundIncreasingIteratorImpl.class);
    }

    @Test
    public void simpleBoundIncreasingIteratorImplValidityTWTest() throws Exception {
        validityTWTest(TSPSimpleBoundIncreasingIteratorImpl.class);
    }

//    @Test
    @SuppressWarnings("unused")
    public void bestImplTimeTest() throws Exception {
        Class<?> tsp = TSPSimpleBoundIncreasingIteratorImpl.class;
        testTSP((TSP) tsp.newInstance(), "/planLyonPetit.xml", "/DLpetit3.xml",
                2258
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonMoyen.xml", "/DLmoyen5.xml",
                4737
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonMoyen.xml", "/DLmoyen5TW4.xml",
                12719
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand10TW2.xml",
                19281
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand14.xml",
                21572
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand16.xml",
                22658
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand18.xml",
                25077
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand20.xml",
                26298
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand20TW20.xml",
                27901
        );

        init();
        testTSP((TSP) tsp.newInstance(), "/planLyonGrand.xml", "/DLgrand20TW.xml",
                24025
        );
    }
}
