package fr.insalyon.teamparrot.carrierparrot.model.business;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PlaceFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.parser.XMLMapParser;
import fr.insalyon.teamparrot.carrierparrot.utils.TimeParser;
import java.time.LocalTime;
import java.util.Iterator;
import java.util.LinkedList;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;
import static org.junit.Assert.*;

public class DeliveryRequestTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    private Map createMap() throws Exception {
        return XMLMapParser.parse(r("/planLyonGrand.xml"));
    }

    @Test
    public void delivery() throws Exception{
        Map map = createMap();
        Long nodeId = map.nodesIterator().next().getId();
        Long duration = 600L;
        LocalTime time = TimeParser.getAsLocalTime("8:0:0");
        DeliveryWarehouse deliveryWarehouse = new DeliveryWarehouse(PlaceFactory.get(map, nodeId), time);
        Delivery delivery1 = new Delivery(PlaceFactory.get(map,nodeId),null, duration, null);
        Delivery delivery2 = new Delivery(PlaceFactory.get(map,nodeId),null, duration, null);
        LinkedList<Delivery> deliveries = new LinkedList<>();
        deliveries.add(delivery1);
        deliveries.add(delivery2);

        DeliveryRequest deliveryRequest = new DeliveryRequest(deliveryWarehouse,deliveries);

        assertEquals(deliveryRequest.getNumberOfDeliveries(),4);

        assertEquals(deliveryRequest.getDeliveryWarehouse().getAddress().getNode().getX(),deliveryWarehouse.getAddress().getNode().getX());
        assertEquals(deliveryRequest.getDeliveryWarehouse().getAddress().getNode().getY(),deliveryWarehouse.getAddress().getNode().getY());
        assertTrue(deliveryRequest.getDeliveryWarehouse().getDepartureHour().equals(deliveryWarehouse.getDepartureHour()));

        Iterator<Delivery> it = deliveryRequest.deliveriesIterator();
        assertEquals(it.next(),deliveryRequest.getDeliveryWarehouse());

        Delivery del1 = it.next();
        assertEquals(del1.getAddress().getNode().getX(), delivery1.getAddress().getNode().getX());
        assertEquals(del1.getAddress().getNode().getY(), delivery1.getAddress().getNode().getY());
        assertEquals(del1.getDuration(), delivery1.getDuration());
        assertNull(del1.getTimeSlotStart());
        assertNull(del1.getTimeSlotEnd());

        Delivery del2 = it.next();
        assertEquals(del2.getAddress().getNode().getX(), delivery2.getAddress().getNode().getX());
        assertEquals(del2.getAddress().getNode().getY(), delivery2.getAddress().getNode().getY());
        assertEquals(del2.getDuration(), delivery2.getDuration());
        assertNull(del2.getTimeSlotStart());
        assertNull(del2.getTimeSlotEnd());

    }
}