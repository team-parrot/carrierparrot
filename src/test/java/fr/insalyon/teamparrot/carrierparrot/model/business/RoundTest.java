/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.model.business;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.error.NoFeasibleRoundException;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.RoundFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import org.junit.Before;
import org.junit.Test;
import java.time.LocalTime;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.deliveryRequest;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static org.junit.Assert.*;

public class RoundTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void roundFullyLoaded() throws Exception {
        final Map map = map("/planLyonGrand.xml");
        final DeliveryRequest deliveryRequest = deliveryRequest(map, "/DLgrand11.xml");
        Round r = RoundFactory.get(map, deliveryRequest);

        assertEquals(map, r.getMap());
        assertEquals(11, r.getPaths().size());
        for (int i = 0; i < r.getPaths().size() - 1; ++i) {
            assertEquals(i + 1, r.getPaths().get(i).getFinish().getDeliveryNumber());
        }
        assertEquals(deliveryRequest.getDeliveryWarehouse(), r.getDeliveryWarehouse());
    }

    @Test
    public void changeDeliveryTime() throws Exception {
        Map map = map("/planLyonGrand.xml");
        Round r = RoundFactory.get(map, deliveryRequest(map, "/DLpetit3.xml"));

        Delivery d = r.getPaths().get(2).getStart();

        r.changeDeliveryTimeSlots(d, LocalTime.of(8, 0), LocalTime.of(8, 10));

        assertSame(d, r.getPaths().get(0).getFinish()); // because the TSP changed the order of the deliveries
    }

    @Test(expected = NoFeasibleRoundException.class)
    public void changeDeliveryTimeException() throws Exception {
        Map map = map("/planLyonGrand.xml");
        Round r = RoundFactory.get(map, deliveryRequest(map, "/DLpetit3.xml"));

        Delivery d = r.getPaths().get(2).getStart();

        r.changeDeliveryTimeSlots(d, LocalTime.of(9, 0), LocalTime.of(4, 0));
    }

    @Test
    public void removeDelivery() throws Exception {
        Map map = map("/planLyonGrand.xml");
        Round r = RoundFactory.get(map, deliveryRequest(map, "/DLpetit3.xml"));

        Delivery d = r.getPaths().get(2).getStart();

        r.removeDelivery(d);

        // There is just one delivery point left.
        assertNotSame(d, r.getPaths().get(1).getStart());
    }

    @Test
    public void autoAddDelivery() throws Exception {
        Map map = map("/planLyonGrand.xml");
        Round r = RoundFactory.get(map, deliveryRequest(map, "/DLgrand11.xml"));

        Delivery d = r.getPaths().get(2).getStart();

        r.removeDelivery(d);

        r.addAutomatically(d);

        assertSame(d, r.getPaths().get(2).getStart());
    }

    @Test
    public void addDelivery() throws Exception {
        Map map = map("/planLyonGrand.xml");
        Round r = RoundFactory.get(map, deliveryRequest(map, "/DLpetit3.xml"));

        Delivery d = r.getPaths().get(2).getStart();

        r.removeDelivery(d);

        r.addAt(d, 0);

        assertSame(d, r.getPaths().get(1).getStart());
    }
}
