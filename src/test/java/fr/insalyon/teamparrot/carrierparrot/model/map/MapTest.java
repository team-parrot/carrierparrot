/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static org.junit.Assert.*;

public class MapTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void mapGetExactNode() throws MapFailedToLoadException {
        final Map map = map("/planLyonGrand.xml");

        Node node = map.getNearestNode(172411, -190164);
        assertEquals(3302622616L, node.getId());
        assertEquals(172411, node.getX());
        assertEquals(-190164, node.getY());
    }

    @Test
    public void mapGetNearestNode() throws MapFailedToLoadException {
        final Map map = map("/planLyonGrand.xml");

        Node node = map.getNearestNode(172415, -190170);
        assertEquals(3302622616L, node.getId());
        assertEquals(172411, node.getX());
        assertEquals(-190164, node.getY());
    }
}