/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.model.business;

import java.time.LocalTime;
import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class DeliveryTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void deliveryWellSet() {
        Place p = null;

        Delivery d1 = new Delivery(p, LocalTime.of(12, 0), 600, LocalTime.of(15, 0));

        assertNotNull(d1.getTimeSlotStart());
        assertNotNull(d1.getTimeSlotEnd());
        assertTrue(600 == d1.getDuration());
        assertNull(d1.getMinArrivalTime());
    }

    @Test
    public void deliveryTimeSlot() {
        Place p = null;

        Delivery d1 = new Delivery(p, null, 600, null);

        assertNull(d1.getTimeSlot());
        assertNull(d1.getTimeSlotEnd());
        assertNull(d1.getTimeSlotStart());
    }

    @Test(expected = NullPointerException.class)
    public void deliverySetStartTime() {
        Place p = null;

        Delivery d1 = new Delivery(p, null, 600, null);

        assertNull(d1.getTimeSlot());

        d1.setTimeSlotStart(LocalTime.of(8, 0));
        assertNotNull(d1.getTimeSlot());
        assertNull(d1.getTimeSlotEnd());

        d1.setTimeSlotStart(null);
    }

    @Test(expected = NullPointerException.class)
    public void deliverySetEndTime() {

        Place p = null;

        Delivery d1 = new Delivery(p, null, 600, null);

        d1.setTimeSlotEnd(LocalTime.of(21, 0));
        assertNotNull(d1.getTimeSlot());
        assertNull(d1.getTimeSlotStart());

        d1.setTimeSlotEnd(null);
    }

    @Test
    public void deliverySetMinArrivalTime() {
        Place p = null;

        Delivery d1 = new Delivery(p, null, 600, null);
        d1.setMinArrivalTime(LocalTime.of(12, 0));

        assertEquals(d1.getMinArrivalTime(), LocalTime.of(12, 0));
    }

    @Test
    public void setDeliveryNumber() {
        Place p = null;

        Delivery d1 = new Delivery(p, null, 600, null);
        d1.setDeliveryNumber(5);
        assertEquals(d1.getDeliveryNumber(), 5);

        assertEquals(d1.toString(), "Delivery n°5");
    }

    @Test
    public void deliveries() {

        Place p = null; // TODO : Get it from PlaceFactory

        Delivery d1 = new Delivery(p, LocalTime.of(12, 0), 600, LocalTime.of(15, 0));
        Delivery d2 = new Delivery(p, LocalTime.of(12, 0), 600, LocalTime.of(15, 0));

        assertNotEquals(d1, d2);
        assertEquals(d1.getDeliveryNumber(), d2.getDeliveryNumber());
        assertEquals(d1.getTimeSlotStart(), d2.getTimeSlotStart());
        assertEquals(d1.getTimeSlotEnd(), d2.getTimeSlotEnd());
        assertEquals(d1.getDuration(), d2.getDuration());
        assertEquals(d1.getAddress(), d2.getAddress());

    }

}
