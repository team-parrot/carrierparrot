package fr.insalyon.teamparrot.carrierparrot.model.business.parser;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.DeliveryRequestFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryWarehouse;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PlaceFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.parser.XMLMapParser;
import fr.insalyon.teamparrot.carrierparrot.utils.TimeParser;
import java.util.Iterator;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;
import static org.junit.Assert.*;

public class XMLDeliveryRequestParserTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    private Map createSmallMap() throws Exception {
        return XMLMapParser.parse(r("/planLyonPetit.xml"));
    }

    @Test
    public void parse() throws Exception {
        Map map = map("/planLyonGrand.xml");

        DeliveryWarehouse deliveryWarehouse = new DeliveryWarehouse(PlaceFactory.get(map, 25303798L), TimeParser.getAsLocalTime("8:0:0"));
        Delivery delivery1 = new Delivery(PlaceFactory.get(map, 195279L), null, 900L, null);
        Delivery delivery2 = new Delivery(PlaceFactory.get(map, 26464319L), null, 600L, null);
        DeliveryRequest deliveryRequest = XMLDeliveryRequestParser.parse(r("/DLpetit3.xml"), map);
        Iterator<Delivery> it = deliveryRequest.deliveriesIterator();
        it.next();

        assertEquals(deliveryRequest.getDeliveryWarehouse().getAddress().getNode().getX(), deliveryWarehouse.getAddress().getNode().getX());
        assertEquals(deliveryRequest.getDeliveryWarehouse().getAddress().getNode().getY(), deliveryWarehouse.getAddress().getNode().getY());
        assertTrue(deliveryRequest.getDeliveryWarehouse().getDepartureHour().equals(deliveryWarehouse.getDepartureHour()));

        Delivery del1 = it.next();
        assertEquals(del1.getAddress().getNode().getX(), delivery1.getAddress().getNode().getX());
        assertEquals(del1.getAddress().getNode().getY(), delivery1.getAddress().getNode().getY());
        assertEquals(del1.getDuration(), delivery1.getDuration());
        assertNull(del1.getTimeSlotStart());
        assertNull(del1.getTimeSlotEnd());

        Delivery del2 = it.next();
        assertEquals(del2.getAddress().getNode().getX(), delivery2.getAddress().getNode().getX());
        assertEquals(del2.getAddress().getNode().getY(), delivery2.getAddress().getNode().getY());
        assertEquals(del2.getDuration(), delivery2.getDuration());
        assertNull(del2.getTimeSlotStart());
        assertNull(del2.getTimeSlotEnd());

    }

    @Test(expected = DeliveryRequestFailedToLoadException.class)
    public void throwsExceptionBadDeliveryRequestFile() throws Exception {
        Map map = map("/planLyonGrand.xml");
        Map smallMap = createSmallMap();

        XMLDeliveryRequestParser.parse(r("/planLyonGrand.xml"), map);
    }

    @Test(expected = DeliveryRequestFailedToLoadException.class)
    public void throwsExceptionIncompatibleMap() throws Exception {
        Map map = map("/planLyonGrand.xml");
        Map smallMap = createSmallMap();

        XMLDeliveryRequestParser.parse(r("/DLgrand20TW.xml"), smallMap);
    }

    @Test
    public void noTimeout() throws Exception {
        XMLDeliveryRequestParser.parse(r("/DLgrand20TW.xml"), map("/planLyonGrand.xml"));
    }
}