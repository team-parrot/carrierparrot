package fr.insalyon.teamparrot.carrierparrot.model.business.factory;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.model.business.Place;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static org.junit.Assert.*;

public class PlaceFactoryTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void get() throws MapFailedToLoadException {
        final Map map = map("/planLyonGrand.xml");
        long nodeId = map.nodesIterator().next().getId();
        Place place = PlaceFactory.get(map, nodeId);
        assertEquals(place.getNode().getId(), nodeId);
    }
}