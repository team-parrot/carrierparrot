/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class SectionTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void sections() {
        Node n1 = new Node(1, -4, 3);
        Node n2 = new Node(1, 11, 17);
        final String sectionName = "Section 1";
        final double sectionLength = 107.3;
        Section s1 = new Section(sectionName, sectionLength, n1, n2);

        assertEquals(sectionName, s1.getStreetName());
        assertEquals(n1, s1.getOrigin());
        assertEquals(n2, s1.getDestination());
        assertEquals(sectionLength, s1.getLength(), 1.);
    }
}