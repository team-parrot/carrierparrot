/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.model.business;

import fr.insalyon.teamparrot.carrierparrot.Config;
import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.error.NoPathExistException;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PathFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static org.junit.Assert.*;

/**
 * Test extracted from the following section defined in big map:
 * <troncon destination="25321447" longueur="113.25773" nomRue="Avenue Lacassagne" origine="251047560"/>
 */
public class PathTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    private static final long START_NODE_ID = 251047560;
    private static final long END_NODE_ID = 25321447;
    private static final double DISTANCE = 113.25773;
    private static final String STREET_NAME = "Avenue Lacassagne";

    @Test
    public void oneSectionPath() throws MapFailedToLoadException {
        final Map map = map("/planLyonGrand.xml");

        Node n1 = map.getNodeFromId(START_NODE_ID);
        Node n2 = map.getNodeFromId(END_NODE_ID);

        Delivery d1 = new Delivery(new Place(n1, new ArrayList<>()), null, 300, null);
        Delivery d2 = new Delivery(new Place(n2, new ArrayList<>()), null, 400, null);

        try {
            Path p = PathFactory.get(map, d1, d2);
            assertEquals(d1, p.getStart());
            assertEquals(d2, p.getFinish());

            assertEquals(DISTANCE, p.getDuration()* Config.DELIVERY_VELOCITY, 2.);

            assertTrue(p.iterator().hasNext() && p.iterator().next().getStreetName().equals(STREET_NAME));

        } catch (NoPathExistException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void iterable() throws MapFailedToLoadException {
        final Map map = map("/planLyonGrand.xml");

        Node n1 = map.getNodeFromId(START_NODE_ID);
        Node n2 = map.getNodeFromId(END_NODE_ID);

        Delivery d1 = new Delivery(new Place(n1, new ArrayList<>()), null, 300, null);
        Delivery d2 = new Delivery(new Place(n2, new ArrayList<>()), null, 400, null);

        try {
            Path p = PathFactory.get(map, d1, d2);

            assertTrue(p.iterator().hasNext());

        } catch (NoPathExistException e) {
            e.printStackTrace();
        }
    }

}
