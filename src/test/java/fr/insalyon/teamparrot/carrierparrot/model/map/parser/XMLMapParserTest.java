/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map.parser;

import com.google.common.collect.Lists;
import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import fr.insalyon.teamparrot.carrierparrot.model.map.Section;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;
import static org.junit.Assert.*;

public class XMLMapParserTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void parse() throws MapFailedToLoadException {

        Map map = XMLMapParser.parse(r("/testMapMini.xml"));

        Node n1 = map.getNodeFromId((long) 1029591870);
        Node n2 = map.getNodeFromId((long) 103755060);
        Node n3 = map.getNodeFromId((long) 1039285693);

        assertNotNull(n1);
        assertNotNull(n2);
        assertNotNull(n3);
        assertEquals(n1.getY(), -15427);
        assertEquals(n1.getX(), 27866);
        assertEquals(n2.getY(), -15602);
        assertEquals(n2.getX(), 27911);
        assertEquals(n3.getY(), -16034);
        assertEquals(n3.getX(), 27050);

        List<Section> sections = Lists.newArrayList(map.sectionsIterator());
        Section s1 = sections.get(0);
        Section s2 = sections.get(1);
        Section s3 = sections.get(2);

        assertNotNull(s1);
        assertNotNull(s2);
        assertNotNull(s3);

        assertEquals(s1.getStreetName(), "Place d'Arsonval");
        assertEquals(s1.getOrigin(), n1);
        assertEquals(s1.getDestination(), n3);
        assertEquals(s1.getLength(), 26.469484, 0.001);

        assertEquals(s2.getStreetName(), "Avenue des Frères Lumière");
        assertEquals(s2.getOrigin(), n2);
        assertEquals(s2.getDestination(), n1);
        assertEquals(s2.getLength(), 16.637556, 0.001);

        assertEquals(s3.getStreetName(), "Boulevard Jean XXIII");
        assertEquals(s3.getOrigin(), n3);
        assertEquals(s3.getDestination(), n2);
        assertEquals(s3.getLength(), 82.35745, 0.001);

    }

    @Test(expected = InstantiationException.class)
    public void failInstantiation() throws InstantiationException {
        new XMLMapParser();
    }

    @Test(expected = MapFailedToLoadException.class)
    public void failParseEmptyMap() throws MapFailedToLoadException {
        XMLMapParser.parse(r("/testEmptyMap.xml"));
    }

    @Test(expected = MapFailedToLoadException.class)
    public void failParseInvalidMap() throws MapFailedToLoadException {
        XMLMapParser.parse(r("/testInvalidMap.xml"));
    }

    @Test
    public void parseLittleMap() throws MapFailedToLoadException {
        Map map = XMLMapParser.parse(r("/planLyonPetit.xml"));

        assertEquals(map.getNumberOfNodes(), 217);
        assertEquals(map.getNumberOfSections(), 370);
    }

    @Test
    public void parseBigMap() throws MapFailedToLoadException {
        Map map = XMLMapParser.parse(r("/planLyonGrand.xml"));
        assertEquals(map.getNumberOfNodes(), 12165);
        assertEquals(map.getNumberOfSections(), 27395);
    }
}