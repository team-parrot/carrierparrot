/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business.factory;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.error.NoPathExistException;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.Path;
import fr.insalyon.teamparrot.carrierparrot.model.business.Place;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.parser.XMLMapParser;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;
import static org.junit.Assert.*;

public class PathFactoryTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void get() throws MapFailedToLoadException, NoPathExistException {
        Map map = XMLMapParser.parse(r("/planLyonGrand.xml"));

        Place p1 = PlaceFactory.get(map, 195735157L);
        Place p2 = PlaceFactory.get(map, 60756056L);
        Delivery d1 = new Delivery(p1, LocalTime.of(10, 0), 13, LocalTime.of(11, 0));
        Delivery d2 = new Delivery(p2, LocalTime.of(12, 31), 37, LocalTime.of(15, 9));

        Path path1 = PathFactory.get(map, d1, d2);
        Path path2 = PathFactory.get(map, d1, d2);
        assertEquals(path1.getDuration(), path2.getDuration(), 1.);
    }
}
