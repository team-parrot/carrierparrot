/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.business;

import java.time.LocalTime;
import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TimeSlotTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void timeSlot() {

        LocalTime start1 = LocalTime.parse("10:32:25");
        LocalTime end1 = LocalTime.parse("11:33:26");
        LocalTime start3 = LocalTime.parse("12:34:27");
        LocalTime end3 = LocalTime.parse("13:35:28");
        TimeSlot t1 = new TimeSlot(start1, end1);
        TimeSlot t2 = new TimeSlot(t1);
        TimeSlot t3 = new TimeSlot(start1, end1);

        t3.setStartTime(start3);
        t3.setEndTime(end3);

        assertEquals(t1.getStartTime(), start1);
        assertEquals(t1.getEndTime(), end1);
        assertEquals(t2.getStartTime(), start1);
        assertEquals(t2.getEndTime(), end1);
        assertEquals(t3.getStartTime(), start3);
        assertEquals(t3.getEndTime(), end3);
    }
}
