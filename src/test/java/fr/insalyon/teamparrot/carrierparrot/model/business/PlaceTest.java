/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.insalyon.teamparrot.carrierparrot.model.business;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.map.Node;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PlaceTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void placeTest() {
        ArrayList<String> s1 = new ArrayList<>();
        s1.add("Rue Jean Jaurès");
        s1.add("Square Lénine");
        Node n1 = new Node(1, -4, 3);
        Place p1 = new Place(n1, s1);

        assertEquals(p1.getNode(), n1);
        assertEquals(p1.getStreets(), s1);
    }
}
