package fr.insalyon.teamparrot.carrierparrot.model.business;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PlaceFactory;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.utils.TimeParser;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static org.junit.Assert.*;

public class DeliveryWarehouseTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void getDepartureHour() throws MapFailedToLoadException {
        final Map map = map("/planLyonGrand.xml");
        Place place = PlaceFactory.get(map,500L);
        LocalTime time = TimeParser.getAsLocalTime("8:0:0");
        DeliveryWarehouse deliveryWarehouse = new DeliveryWarehouse(place, time);

        assertEquals(deliveryWarehouse.getDepartureHour(), time);
    }
}