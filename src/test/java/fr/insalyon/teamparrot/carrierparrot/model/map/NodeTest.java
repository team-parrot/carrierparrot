/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.model.map;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class NodeTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void nodes() {
        long id1 = 13168;
        final int n1x = 651;
        final int n1y = 984512;

        Node n1 = new Node(id1, n1x, n1y);
        Node n2 = new Node(id1, 984653, 32165);

        assertEquals(n1x, n1.getX());
        assertEquals(n1y, n1.getY());
        assertEquals(id1, n1.getId());
        assertTrue(n1.equals(n2));
    }
}