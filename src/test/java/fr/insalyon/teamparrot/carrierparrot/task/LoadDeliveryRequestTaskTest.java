/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.error.MapFailedToLoadException;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import java.io.File;
import java.util.concurrent.ExecutionException;
import javafx.application.Platform;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;

public class LoadDeliveryRequestTaskTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void callSuccess() throws ExecutionException, InterruptedException, MapFailedToLoadException {
        Map map = map("/planLyonGrand.xml");
        File deliveryRequestFile = new File(r("/DLpetit3.xml"));

        LoadDeliveryRequestTask task = new LoadDeliveryRequestTask(map, deliveryRequestFile);
        Platform.runLater(() -> TaskExecutor.execute(task));
        task.get();
        Thread.sleep(500);
    }

    @Test
    public void callFail() throws ExecutionException, InterruptedException, MapFailedToLoadException {
        Map map = map("/planLyonGrand.xml");
        File deliveryRequestFile = new File(r("/DLvide.xml"));

        LoadDeliveryRequestTask task = new LoadDeliveryRequestTask(map, deliveryRequestFile);
        Platform.runLater(() -> TaskExecutor.execute(task));
        Thread.sleep(500);
    }
}