package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.RoundFactory;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import javafx.application.Platform;
import org.junit.Before;
import org.junit.Test;

import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;

public class RemoveDeliveryTaskTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void callSuccess() throws Exception {
        Map map = map("/planLyonGrand.xml");
        DeliveryRequest deliveryRequest = XMLDeliveryRequestParser.parse(r("/DLpetit3.xml"), map);
        Round round = RoundFactory.get(map, deliveryRequest);
        Delivery delivery1 = round.getPaths().get(1).getFinish();
        RemoveDeliveryTask task1 = new RemoveDeliveryTask(round, delivery1);
        Platform.runLater(() -> TaskExecutor.execute(task1));
        task1.get();
    }

    @Test
    public void callError() throws Exception {
        Map map = map("/planLyonGrand.xml");
        DeliveryRequest deliveryRequest = XMLDeliveryRequestParser.parse(r("/DLpetit3.xml"), map);
        Round round = RoundFactory.get(map, deliveryRequest);
        Delivery delivery2 = round.getPaths().get(2).getFinish();
        RemoveDeliveryTask task2 = new RemoveDeliveryTask(round, delivery2);
        Platform.runLater(() -> TaskExecutor.execute(task2));
    }

}