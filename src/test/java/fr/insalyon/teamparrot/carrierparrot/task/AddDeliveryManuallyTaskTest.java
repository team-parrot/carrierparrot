package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.business.*;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.PlaceFactory;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.RoundFactory;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.utils.TimeParser;
import javafx.application.Platform;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static fr.insalyon.teamparrot.carrierparrot.TestUtils.deliveryRequest;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;
import static org.junit.Assert.*;

public class AddDeliveryManuallyTaskTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void addFeasibleDelivery() throws Exception {
        Map map = map("/planLyonPetit.xml");
        Round round = RoundFactory.get(map, deliveryRequest(map, "/DLpetit3.xml"));
        Place place = PlaceFactory.get(map, 21702435L);
        LocalTime startTime = TimeParser.getAsLocalTime("8");
        LocalTime endTime = TimeParser.getAsLocalTime("12:");
        Delivery delivery = new Delivery(place, startTime, 900L, endTime);
        AddDeliveryManuallyTask task = new AddDeliveryManuallyTask(round, delivery, 1);
        Platform.runLater(() -> TaskExecutor.execute(task));
        task.get();
        Thread.sleep(500);
        boolean roundContainsDelivery = false;
        for (Path path : round) {
            if (path.getStart().equals(delivery)) roundContainsDelivery = true;
        }
        assertTrue(roundContainsDelivery);
    }

    @Test(expected = Exception.class)
    public void addUnfeasibleDelivery() throws Exception {
        Map map = map("/planLyonGrand.xml");
        DeliveryRequest deliveryRequest = XMLDeliveryRequestParser.parse(r("/DLpetit3.xml"), map);
        Round round = RoundFactory.get(map, deliveryRequest);
        Place place = PlaceFactory.get(map, map.nodesIterator().next().getId());
        LocalTime startTime = TimeParser.getAsLocalTime("8:0:0");
        LocalTime endTime = TimeParser.getAsLocalTime("8:0:1");
        Delivery delivery = new Delivery(place, startTime, 900L, endTime);
        AddDeliveryManuallyTask task = new AddDeliveryManuallyTask(round, delivery, 1);

        TaskExecutor.execute(task);
        task.get();
    }
}