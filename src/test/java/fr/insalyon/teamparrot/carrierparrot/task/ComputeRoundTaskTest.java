package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import fr.insalyon.teamparrot.carrierparrot.model.map.parser.XMLMapParser;
import javafx.application.Platform;
import org.junit.Before;
import org.junit.Test;

import static fr.insalyon.teamparrot.carrierparrot.TestUtils.deliveryRequest;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;

public class ComputeRoundTaskTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void callSuccess() throws Exception {
        Map map = map("/planLyonGrand.xml");
        DeliveryRequest deliveryRequest1 = deliveryRequest(map, "/DLpetit3.xml");
        ComputeRoundTask task = new ComputeRoundTask(map, deliveryRequest1);
        Platform.runLater(() -> TaskExecutor.execute(task));
        task.get();
        Thread.sleep(500);
    }

    @Test
    public void callError() throws Exception {
        Map map = map("/planImpossible.xml");
        DeliveryRequest deliveryRequest = deliveryRequest(map, "/DLpetit3.xml");
        ComputeRoundTask task = new ComputeRoundTask(map, deliveryRequest);
        Platform.runLater(() -> TaskExecutor.execute(task));
        Thread.sleep(500);
    }
}
