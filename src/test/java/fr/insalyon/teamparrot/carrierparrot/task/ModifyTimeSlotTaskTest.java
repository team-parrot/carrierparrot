/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.insalyon.teamparrot.carrierparrot.task;

import fr.insalyon.teamparrot.carrierparrot.TestUtils;
import fr.insalyon.teamparrot.carrierparrot.model.business.Delivery;
import fr.insalyon.teamparrot.carrierparrot.model.business.DeliveryRequest;
import fr.insalyon.teamparrot.carrierparrot.model.business.Round;
import fr.insalyon.teamparrot.carrierparrot.model.business.TimeSlot;
import fr.insalyon.teamparrot.carrierparrot.model.business.factory.RoundFactory;
import fr.insalyon.teamparrot.carrierparrot.model.business.parser.XMLDeliveryRequestParser;
import fr.insalyon.teamparrot.carrierparrot.model.map.Map;
import java.time.LocalTime;
import javafx.application.Platform;
import org.junit.Before;
import org.junit.Test;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.map;
import static fr.insalyon.teamparrot.carrierparrot.TestUtils.r;

public class ModifyTimeSlotTaskTest {
    @Before
    public void init() throws Exception {
        TestUtils.initializeJavaFxApplication();
        TestUtils.resetSingletons();
    }

    @Test
    public void call() throws Exception {
        Map map = map("/planLyonGrand.xml");
        DeliveryRequest deliveryRequest = XMLDeliveryRequestParser.parse(r("/DLgrand11.xml"), map);
        Round round = RoundFactory.get(map, deliveryRequest);
        final Delivery delivery = round.getPaths().get(1).getStart();
        TimeSlot timeSlot = new TimeSlot(LocalTime.parse("10:25:36"), LocalTime.parse("18:24:19"));
        TimeSlot timeSlot2 = new TimeSlot(LocalTime.parse("07:00:00"), LocalTime.parse("08:00:00"));

        ModifyTimeSlotTask task = new ModifyTimeSlotTask(round, delivery, timeSlot);
        Platform.runLater(() -> TaskExecutor.execute(task));
        task.get();

        ModifyTimeSlotTask task2 = new ModifyTimeSlotTask(round, delivery, timeSlot2);
        Platform.runLater(() -> TaskExecutor.execute(task2));
        task.get();
    }
}