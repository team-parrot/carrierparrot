# Coding style guide for CarrierParrot

Like every team project, CarrierParrot needs to be set on common good practices, followed by any team member. Please note this coding style **will be automatically checked** when submitting a Pull Request.

This coding style guide has been created by Kévin Dumanoir for Team Parrot, H4304, INSA Lyon 2017.

**The main guidelines are mainly based on Intellij IDEA code style settings.**

## I. File and global specifications

### 1. Header

The following header **MUST** be included in the first lines of **every** Java file:

```java
/*
 * This file is part of CarrierParrot, produced during the 4th year
 * at the IT department of INSA de Lyon. It was developed by
 * Team Parrot, 2017.
 *
 * Copyright (C) 2017 Team Parrot - INSA Lyon <contact@teamparrot.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
```

### 2. Tabs, EOF and File size

The only recommended character to perform tabs is spaces. **4 spaces = 1 tab**. 

Java files **MUST** end with a empty line (LF) and **MUST NOT** have more than **1000** lines.

### 3. Imports

Java imports **MUST** be gathered in one group, sorted by ascending package name.
Star imports and redundant imports are **FORBIDDEN*. JUnit framework **MUST NOT** be imported in the project.

### 4. Javadoc

**Every class MUST have a Javadoc comment.** The only exceptions are : Events and Exceptions.

The same rule applies for inner methods. 
**However, methods containing less than 4 lines (included) CAN be let uncommented.**

### 4. Class, attribute, method conventions, if/while/for braces

```java
public class MySimpleClass {
    // Attributes
    (private|public) static final int MY_INTEGER = 1337;
    public Object myAttribute;
    (private|protected) boolean myPrivateAttribute;
    
    // Methods
    private void myMethod();
    public void myOtherMethod();
    private static void myStaticMethod();
    
    // In a function:
    private void randomMethod() {
        byte test = 1;
        final int functionFinal = 42;
        
        while (true) {
            somethingToDo();
        }
        if (false) {
            neverGoHere();
        }
        for (Path p: round.getPaths()) {
            doAnAngryDijsktra(p);
        }
    };
}
```