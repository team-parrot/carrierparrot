# Comment contribuer à un projet TeamParrot JAVA ?

Afin d'isoler de manière sûre les strates de développement, la contribution au projet ne peut pas relever de l'amateurisme.

C'est pour cela que __la stratégie suivante doit TOUJOURS être appliquée, pour les projets nécessitant une maintenance sur le long terme et un développement en équipe.__

## 0. Lexique

* __Tâche__ : Résultat d'une activité liée au développement d'un système. Une tâche est atomique. _Ex: ajout d'une nouvelle vue synoptique._
* __Pull request__/__Merge request (PR)__ : Demande de fusion entre une branche personnelle (voir 2.) et une branche commune. Permet de revoir le code et offre ainsi une autre passerelle de validation.

## 1. Récupérer le projet, configurer les remotes

Pour contribuer à un projet, il faut tout d'abord le récupérer. Cependant, dans le cas présent, il est hors de question de procéder à un simple `git clone`.

Afin de démarrer votre contribution sur de bonnes bases, commencez par __faire un fork__ du projet commun. Pour cela, allez sur la page d'accueil du projet, puis cliquez tout simplement sur `Fork`.

__Chaque développeur possède son fork du projet__. Cela permet d'isoler les erreurs de manipulation de branche (on les a tous connues), et éventuellement d'aller visionner une tâche en cours de développement assez simplement.

Jusqu'à la validation de la **PR** par les autres membres de l'équipe, le code en cours de développement est sur le fork du développeur !

L'idée va être d'avoir deux repository en parallèle pour chaque développeur:
* Le repository principal (c'est à dire le repo de la Team Parrot): `PARROT`, commun à tous les développeurs.
* Le fork de chaque développeur, qui aura pour nom `origin`.

### 1.1. Configurer les remotes

Maintenant qu'on a fait un fork, il suffit de le cloner sur son poste. Cependant, on aura parfois besoin de se mettre à jour par rapport au repository principal (ex: ajout d'une fonctionnalité par un autre dev, une PR validée, etc.).

Du coup, il faut rajouter le repository principal en remote. De préférence, donnez lui un nom bien différent de __origin__. Par convention, on peut utiliser __PARROT__

```
git remote add PARROT http://[repository.git]
git pull PARROT dev
[...]
```

## 2. Gestion de branche

Au delà de toute stratégie de contribution, nous convenons de nommer nos branches de la manière suivante:

### 2.1. Branches communes

En raison des forks, chaque repository n'a pas forcément les mêmes branches. Cependant, les deux branches suivantes assurent le lien entre tous les fork et le repository principal.

* __master__ : Branche de production, c'est la branche qui subit le moins de commit "à chaud", à l'exception des hotfix.
* __dev__ : Branche de développement, elle permet d'agréger les branches liées à chaque tâche. Elle peut contenir des commits 

__MASTER NE SERT JAMAIS DE RÉFÉRENCE POUR DÉBUTER UNE TÂCHE.__

Il est possible d'avoir plus de deux branches communes, en fonction des contraintes de projet. Dans ce cas, se réferrer au guide de contribution de votre projet.

### 2.2. Nomenclature de branches individuelles

Une fois le fork créé et les branches communes identifiées, vous allez vouloir commencer à travailler (normal).

Selon l'organisation du projet, vous allez avoir un certain nombre de tâches à réaliser. À chaque tâche, on associe une branche, __individuelle car elle ne sera présente que sur votre fork !__
1. Chaque tâche peut avoir une thématique différente : implémentation d'une nouvelle fonctionnalité, correction de bug, amélioration, correctif de sécurité...
2. Les tâches sont numérotées chronologiquement.
3. À Chaque tâche on peut associer un petit identificateur.
4. Enfin, vous allez faire beaucoup de commits pendant votre développement (oubli de code, etc.).

   Il convient donc d'avoir une branche de développement et une branche "propre", qui sera envoyé en __pull request__. La branche propre sera issue de la branche __-dev__ via un `git rebase -i`.

De ces quatre règles, on arrive à la nomenclature de développement suivante:

```
[thematique]-[numerotation]-[identificateur]-[dev?]
```

__Thématiques de base :__  feature, refacto, security, fix, init, del, intern.

Voici un exemple concret: Je travaille sur l'ajout d'une authentification Ajax. C'est la 5ème tâche du projet.

```
Branche de developpement : feature-5-AJAXAUTH-dev
Branch à envoyer en PR   : feature-5-AJAXAUTH
```

## 3. Préparation d'une pull request

Pour éviter les merge conflicts, le nommage doit reposer sur des actions à faire __avant__ et __après__ le développement d'une tâche.

### 3.1. Avant : Penser Git PULL !

Avant de débuter une tâche, __toujours se remettre sur la branche dev, la mettre à jour avec le repo principal__ et ensuite faire un checkout.

Avec l'exemple du début, ça donne :

```bash
git checkout dev
git pull PARROT dev
git checkout -b feature-5-AJAXAUTH-dev # -b pour créer une nouvelle branche
```

### 3.2. Après : Toujours penser Git PULL (mais avec ses copains maintenant...)

Pour préparer votre pull request, il faut d'abord __reconnecter vos modifications avec le repo principal__. En effet, des collègues ont pu faire des modifications entre temps. Aie !

Pour cela, on va checkout la branche `dev`, puis __pull depuis le repo principal__. On va ensuite créer la branche _propre_, et effectuer un rebase depuis la branche individuelle de développement.

Vous avez mal à la tête ? C'est normal ! En gros, le rebase va permettre de __reconnecter vos modifications à la branche dev__.

Mais en plus de cela, on effectue un rebase en **mode interactif**, ce qui va nous permettre de modifier, fusionner, déplacer des commits ! En somme, nettoyer tout votre travail avant l'envoi.

Toujours avec le petit exemple:
```bash
git checkout dev
git pull PARROT dev
git checkout -b feature-5-AJAXAUTH # sans le -dev, c'est la branche "propre"

git checkout feature-5-AJAXAUTH-dev
git rebase -i feature-5-AJAXAUTH # Rebrancher branche individuelle sur dev, petit nettoyage en mm temps.
git checkout feature-5-AJAXAUTH # La branche propre ne contient pas encore les commits nettoyés.
git merge feature-5-AJAXAUTH-dev # On résoud ça avec un merge.
```

Enfin, une fois que tout est bien propre en local, on envoie la branche sur son repo personnel:

```bash
git push origin feature-5-AJAXAUTH
```

Il faut ensuite aller sur gitlab pour envoyer la merge request, __qui a pour destination la branch dev du repo principal.__