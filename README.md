# CarrierParrot

CarrierParrot est un logiciel de calcul de tournée de livraisons.

Ce projet est réalisé dans le cadre du Projet Longue Durée (PLD) AGILE de
la 4ème année du département Informatique de l'INSA Lyon. Il est publié
sous licence GNU GPLv3.

Projet réalisé par l'hexanôme H4304 ("Team Parrot") :
Tom Bourret, Thomas Lacroix, Jean Debard, Vincent Guillon, Kévin Dumanoir.

* **Chef de projet:** Tom Bourret
* **Responsable qualité:** Thomas Lacroix
* **Product owner:** Jean Debard

## Prérequis

Ce logiciel repose sur le langage **Java 8** et du gestionnaire de paquet
**maven**. Afin de pouvoir le compiler et le lancer dans des conditions 
optimales, merci d'installer les ressources suivantes :
* Java JDK 8 (OpenJDK 8 compatible)
* Maven (version utilisée : 3.5.2)

Le logiciel est lançable sur les OS usuels: Linux, Mac OS, Windows.

**Les sources de données sont incluses dans le projet, dans le dossier `data`**

## Contribuion et Guide de style

Les fichiers `CONTRIBUTING.md` et `CODINGSTYLE.md` définissent respectivement les points à suivre pour la contribution au Git
et le style de développement unifié au sein du projet. 

**Merci de les consulter avant toute merge request.**

## Procédures usuelles (tests, compilation)

Afin d'obtenir les mêmes résultats, veuillez utiliser un terminal et le
placer dans le dossier racine du projet.

**Les binaires sont localisables dans le dossier `target`**

L'intégralité des procédures est automatisée à l'aide de maven. Voici donc
les commandes à effectuer pour chaque procédure:

* Exécution des tests: `mvn test`
* Compilation : `mvn clean compile assembly:single`, puis lancer le jar 
généré dans `target` avec le template de commande :
`java -jar target/carrierparrot-0.2-release-jar-with-dependencies.jar`

## Couverture et tests

Deux types de tests ont été effectués : les tests unitaires et fonctionnels. Ces derniers sont fournis, soit dans le
code source du projet soit dans le dossier final.

**Les résultats de couverture sont disponibles dans le dossier `test_results`.**

## Génération de tournée optimale (TSP Branch & Bound) : benchmarks

Configuration utilisée pour les tests : 
* Intel i7-4720HQ (8) @ 3.600GHz
* Kernel: 4.14.4-1-ARCH
* Memory: 7418MiB / 15967MiB
* **Windows Manager: _i3_**

| Nombre de livraisons | Plan utilisé | Avec plages horaires ? | Temps (sec.) |
| -------------------- |:------------:|:----------------------:| ------------:|
| 10                   | grand        | oui (TW2)              | 0.348        |
| 14                   | grand        | non                    | 0.949        |
| 16                   | grand        | non                    | 1.122        |
| 18                   | grand        | non                    | 3.578        |
| 20                   | grand        | non                    | 11.313       |
| 20                   | grand        | oui (TW20)             | 13.744       |
| 20                   | grand        | oui (TW)               | 85.029       |

## Technologies utilisées:

* Langage Java, version 8.
* Maven, pour la gestion des dépendances.
* Bibliothèque JavaFx, pour l'interface graphique.
* ControlsFx, pour certains de ses composants.
* Guava, pour ses structures de données.
* ReactiveX, pour le bus d'événements.
* iTextPDF, pour l'export PDF.
* Apache Log4j, pour gérer la journalisation.
* JUnit, pour les tests unitaires.
* IntelliJ IDEA, pour les mesure de la couverture de code.
* Les plugins CheckStyle et FindBugs, pour la qualité du code.